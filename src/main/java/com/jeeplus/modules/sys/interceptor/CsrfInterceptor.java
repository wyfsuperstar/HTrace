package com.jeeplus.modules.sys.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeeplus.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 拦截csrf攻击
 */
public class CsrfInterceptor extends HandlerInterceptorAdapter {
    @Value("#{APP_PROP['server.port']}")
    private String port = "";
    @Value("#{APP_PROP['server.ip']}")
    private String ip = "";

    private static final String URL_PROTO_HTTP="http://";
    private static final String URL_PROTO_HTTPS="https://";

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        String referer = request.getHeader("Referer");
        String port = "-1";
        if(StringUtils.isNotBlank(referer)){
            if(referer.startsWith(URL_PROTO_HTTP)){
                referer = referer.replace(URL_PROTO_HTTP, "");
            }else if(referer.startsWith(URL_PROTO_HTTPS)){
                referer = referer.replace(URL_PROTO_HTTPS, "");
            }
            int i = referer.indexOf("/");
            if(i > 0){
                referer = referer.substring(0,i);
                if(referer.indexOf(":") > 0){ //referer 10.20.147.80:7888 -> 10.20.147.80只验证ip
                    port = referer.split(":")[1];
                    referer= referer.substring(0, referer.indexOf(":"));
                }
            }

            if(!ip.contains(referer)){ //不同域请求 视为非法

                    throw new Exception("非法请求，请求源不正确");
                
            }
        }
        return true;
    }
}