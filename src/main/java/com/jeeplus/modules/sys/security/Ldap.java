package com.jeeplus.modules.sys.security;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.Hashtable;
import java.util.Vector;


public class Ldap {

    /*public static void main(String [] args){
        Ldap ld=new Ldap();
        String root = "ou=people,dc=tjciq,dc=com";//查询所有用户
        String root1 = "ou=orgs,dc=tjciq,dc=com";//查询所有组织机构
        ld.searchInformation(root, "", "(objectclass=*)");


    }*/

   /* public void init(){

        Hashtable env = new Hashtable();
        String LDAP_URL = "ldap://10.12.8.133:389";// LDAP访问地址
        String domain = "cn=Manager,dc=tjciq,dc=com";// 注意用户名的写法：domain\User或

        String adminPassword = "p@ssw0rd"; // 密码
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, domain);
        env.put(Context.SECURITY_CREDENTIALS, adminPassword);

        try {
            DirContext dc = new InitialDirContext(env);// 初始化上下文
            //System.out.println("认证成功");// 这里可以改成异常抛出。
        } catch (javax.naming.AuthenticationException e) {
            System.out.println("ldap connect Error:login error");
        } catch (Exception e) {
            System.out.println("ldap connect Error:" + e);
        }


    }
*/
    public void searchInformation(String base, String scope, String filter) {
        DirContext dc=null;
        Hashtable env = new Hashtable();
        String LDAP_URL = "ldap://10.12.8.133:389";// LDAP访问地址
        String adminName = "cn=Manager,dc=tjciq,dc=com";// 注意用户名的写法：domain\User或

        String adminPassword = "p@ssw0rd"; // 密码
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, adminName);
        env.put(Context.SECURITY_CREDENTIALS, adminPassword);
        try {
            dc = new InitialDirContext(env);// 初始化上下文
        } catch (NamingException e) {
            e.printStackTrace();
        }
        SearchControls sc = new SearchControls();
        if (scope.equals("base")) {
            sc.setSearchScope(SearchControls.OBJECT_SCOPE);
        } else if (scope.equals("one")) {
            sc.setSearchScope(SearchControls.ONELEVEL_SCOPE);
        } else {
            sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        }
        NamingEnumeration ne = null;

        try {
            ne = dc.search(base, filter, sc);
            // Use the NamingEnumeration object to cycle through
            // the result set.
            while (ne.hasMore()) {
                System.out.println();
                SearchResult sr = (SearchResult) ne.next();
                String name = sr.getName();
                /*if (base != null && !base.equals("")) {
                    System.out.println("entry: " + name + "," + base);
                } else {
                    System.out.println("entry: " + name);
                }*/

                Attributes at = sr.getAttributes();
                NamingEnumeration ane = at.getAll();
                while (ane.hasMore()) {
                    Attribute attr = (Attribute) ane.next();//导包有可能错误，若是不能使用，检查导包
                    String attrType = attr.getID();
                    NamingEnumeration values = attr.getAll();
                    Vector vals = new Vector();
                    // Another NamingEnumeration object, this time
                    // to iterate through attribute values.
                    while (values.hasMore()) {
                        Object oneVal = values.nextElement();
                        if(attrType.equals("displayName")||attrType.equals("uid")||attrType.equals("ciqpositionaltitles")||attrType.equals("orgname")||attrType.equals("ciqbusiness")||attrType.equals("description")||attrType.equals("mobile")||attrType.equals("email")){

                            if (oneVal instanceof String) {
                                System.out.print(
                                        (String) oneVal+',');
                            } else {
                                System.out.print(
                                        new String((byte[]) oneVal)+',');
                            }
                        }


                    }

                }
                System.out.println("=======无敌分割线==========");
            }
        } catch (Exception nex) {
            System.err.println("Error: " + nex.getMessage());
            nex.printStackTrace();
        }

        System.out.println("******************************************");
        System.out.println("******************************************");
        System.out.println("******************无敌分割线************************");
        System.out.println("******************************************");
        System.out.println("******************************************");
    }


    public void getInfo(){
        Ldap ld=new Ldap();
        String root = "ou=people,dc=tjciq,dc=com";//查询所有用户
        String root1 = "ou=orgs,dc=tjciq,dc=com";//查询所有组织机构
        ld.searchInformation(root, "", "(objectclass=*)");
        ld.searchInformation(root1, "", "(objectclass=*)");
    }
}
