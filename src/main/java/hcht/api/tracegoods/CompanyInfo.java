/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api.tracegoods;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class CompanyInfo {

	private String cbe_name;		// 企业名称
//	private String register_address;		// 错误码

	public String getCbe_name() {
		return cbe_name;
	}

	public void setCbe_name(String cbe_name) {
		this.cbe_name = cbe_name;
	}

}