/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api.tracegoods;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class ReturnData  {

	private GoodsInfo goods_info;		// 请求是否成功，true：成功，false：失败
	private CompanyInfo company_info;		// 错误码
//	private EntryInfo entry_info;		// 错误码
	private RetrospectInfo retrospect_info;		// 错误码

	public GoodsInfo getGoods_info() {
		return goods_info;
	}

	public void setGoods_info(GoodsInfo goods_info) {
		this.goods_info = goods_info;
	}

	public CompanyInfo getCompany_info() {
		return company_info;
	}

	public void setCompany_info(CompanyInfo company_info) {
		this.company_info = company_info;
	}

	public RetrospectInfo getRetrospect_info() {
		return retrospect_info;
	}

	public void setRetrospect_info(RetrospectInfo retrospect_info) {
		this.retrospect_info = retrospect_info;
	}
}