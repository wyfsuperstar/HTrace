/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api.tracegoods;

import hcht.api.Api;

import java.util.Date;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class ApiLog {
	private String id;
	private String attribute;		// 请求是否成功，true：成功，false：失败
	private String ip;		// 错误码
	private String ssid;		// 错误码
	private String request;
	private Date readDate;
	private String apiType;
	private String success;		// 请求是否成功，true：成功，false：失败
	private String errorCode;		// 错误码
	private String errorMsg;		// 错误说明及详细信息

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public void setApi(Api api){
		this.success = api.getSuccess();
		this.errorCode = api.getErrorCode();
		this.errorMsg = api.getErrorMsg();
	}
}