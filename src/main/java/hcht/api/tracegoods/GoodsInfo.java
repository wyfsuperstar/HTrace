/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api.tracegoods;

import com.jeeplus.common.persistence.DataEntity;

import java.util.HashMap;
import java.util.List;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class GoodsInfo  {

	private static final long serialVersionUID = 1L;
	private String goods_name;		// 商品名称
//	private String master_base;		// 错误码
	private String spec;		// 规则型号
//	private String origin_country;		// 错误码
//	private String pro_ent;		// 错误码
	private String brand;		// 品牌
//	private String use_way;		// 错误码
//	private String hscode;		// 错误码


	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
}