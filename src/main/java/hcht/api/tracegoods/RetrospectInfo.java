/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api.tracegoods;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class RetrospectInfo {
	private String origin_country;		// 原产国
	private String desp_country;		// 发货国家
//	private String desp_port;		// 错误码
//	private String entry_port_name;		// 错误码
//	private String cbe_name;		// 错误码
	private String url;		// 网址
	private String site_name;		// 网站名
	private String importtype;		// 运递进境方式

	public String getOrigin_country() {
		return origin_country;
	}

	public void setOrigin_country(String origin_country) {
		this.origin_country = origin_country;
	}

	public String getDesp_country() {
		return desp_country;
	}

	public void setDesp_country(String desp_country) {
		this.desp_country = desp_country;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getImporttype() {
		return importtype;
	}

	public void setImporttype(String importtype) {
		this.importtype = importtype;
	}
}