/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.api.tracegoods.ApiLog;
import hcht.api.tracegoods.RequestData;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * 分类信息DAO接口
 * @author mather
 * @version 2017-04-20
 */
@MyBatisDao
public interface ApiDao extends CrudDao<Api> {
    public String findCountBySsid(@Param("attribute") String attribute, @Param("ssid") String ssid);
    public List<HashMap> findscjc01(@Param("beginDate") String beginDate, @Param("endDate") String endDate);
    public List<HashMap> findTraceGoods(@Param("attribute") String attribute, @Param("order_no") String order_no);
    public void insertAPILog(ApiLog apiLog);
}