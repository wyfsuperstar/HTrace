/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.annotation.ExcelField;
import com.jeeplus.modules.sys.utils.DictUtils;
import hcht.api.tracegoods.GoodsInfo;
import org.hibernate.validator.constraints.Length;

import java.util.HashMap;
import java.util.List;

/**
 * 导出接口
 * @author mather
 * @version 2017-04-19
 */
public class Api {

	private static final long serialVersionUID = 1L;
	private String success;		// 请求是否成功，true：成功，false：失败
	private String returnData;		// 返回内容（参照2.1.2.1）
	private String errorCode;		// 错误码
	private String errorMsg;		// 错误说明及详细信息

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}