/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api;

import com.alibaba.druid.support.json.JSONParser;
import com.alibaba.druid.support.json.JSONUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.util.concurrent.RateLimiter;
import com.google.gson.JsonObject;
import com.jeeplus.common.utils.Encodes;
import hcht.api.ApiService;
import com.jeeplus.common.web.BaseController;
import hcht.api.tracegoods.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * 分类信息Controller
 * @author mather
 * @version 2017-04-20
 */
@Controller
@RequestMapping(value = "${adminPath}/api/")
public class ApiController extends BaseController {

	@Autowired
	private ApiService apiService;
	@Resource
	LoadingCacheService loadingCacheService;

	/**
	 * 商品信息
	 */
	@RequestMapping("traceGoods")
	@ResponseBody
	public Api traceGoods(@RequestBody RequestData requestData,HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException {
		String attribute = requestData.getAttribute();
		String order_no = requestData.getOrder_no();
		String ssid = requestData.getSsid();
//	public Api traceGoods(String attribute,String order_no,String ssid,HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException {

		Api api = new Api();
		ApiLog apiLog = new ApiLog();

		//log
		ObjectMapper mapper1 = new ObjectMapper();
		String requestdata = mapper1.writeValueAsString(requestData);

		apiLog.setApiType("01");
		apiLog.setAttribute(attribute);
		apiLog.setSsid(ssid);
		apiLog.setRequest(requestdata);
		apiLog.setIp(getIpAddress(request));
		if (attribute == null || attribute.equals("")) {
			api.setSuccess("false");
			api.setErrorCode("901");
			api.setErrorMsg("attribute参数错误");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}
		if (order_no == null || order_no.equals("")) {
			api.setSuccess("false");
			api.setErrorCode("901");
			api.setErrorMsg("order_no参数错误");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}
		if (ssid == null || ssid.equals("")) {
			api.setSuccess("false");
			api.setErrorCode("901");
			api.setErrorMsg("ssid参数错误");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}
		String ipAddr = "localhost";
		try {
			RateLimiter limiter = loadingCacheService.getRateLimiter(ipAddr);

			if(!limiter.tryAcquire()){
				api.setSuccess("false");
				api.setErrorCode("998");
				api.setErrorMsg("超出访问限制");
				apiLog.setApi(api);
//				apiService.insertAPILog(apiLog);
				return api;
			}else{
//				loadingCacheService.invalidateAll();
			}

		} catch (ExecutionException e) {
			e.printStackTrace();
		}



		String count = apiService.findCountBySsid(attribute, ssid);
		if (count.equals(0)) {
			api.setSuccess("false");
			api.setErrorCode("902");
			api.setErrorMsg("企业备案号和ssid参数不存在或不匹配");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}


		List<HashMap> list = apiService.findTraceGoods(attribute, order_no);
		if(list.size() == 0){
			api.setSuccess("false");
			api.setErrorCode("903");
			api.setErrorMsg("没有符合条件的结果");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;

		}

//		api.setSuccess("true");
//		api.setReturnData(list);
		//System.out.println(JSONUtils.toJSONString(api));
//		return api;

//		List<ReturnData> listGoods = null;
		ArrayList listGoods = new ArrayList();
		try {
			for (HashMap l : list) {
				GoodsInfo gi = new GoodsInfo();
//				EntryInfo ei = new EntryInfo();
				CompanyInfo ci = new CompanyInfo();
				RetrospectInfo ri = new RetrospectInfo();
				ReturnData returnData = new ReturnData();
				gi.setGoods_name((String) l.get("goods_name"));
				gi.setSpec((String) l.get("spec"));
				gi.setBrand((String) l.get("brand"));
				returnData.setGoods_info(gi);
				ci.setCbe_name((String) l.get("cbe_name"));
				returnData.setCompany_info(ci);
				ri.setOrigin_country((String) l.get("origin_country"));
				ri.setDesp_country((String) l.get("desp_country"));
				ri.setImporttype((String) l.get("importtypename"));
				ri.setUrl((String) l.get("url"));
				ri.setSite_name((String) l.get("site_name"));
				returnData.setRetrospect_info(ri);
				listGoods.add(returnData);
			}
			api.setSuccess("true");
			ObjectMapper mapper = new ObjectMapper();
			String mapJakcson = mapper.writeValueAsString(listGoods);

			BASE64Encoder encoder = new  BASE64Encoder();// 加密
			api.setReturnData(encoder.encode(mapJakcson.getBytes("UTF-8")).replaceAll("\\r\\n", ""));
//			System.out.print(mapJakcson);
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}catch (Exception e){
			api.setSuccess("false");
			api.setErrorCode("999");
			api.setErrorMsg("意外错误请联系管理员");
			apiLog.setApi(api);
			apiService.insertAPILog(apiLog);
			return api;
		}
	}
	@RequestMapping("test")
	@ResponseBody
	public List test(String attribute,String order_no,String ssid) {


		List<HashMap> list = apiService.findTraceGoods(attribute, order_no);
		return list;
	}

//	@RequestMapping("testapi")
//	@ResponseBody
//	public String testapi(String attribute,String order_no,String ssid) throws IOException {
//		String url = "http://localhost:8888/a/api/traceGoods";
//		String body = "";
//
//		//创建httpclient对象
//		CloseableHttpClient client = HttpClients.createDefault();
//		//创建post方式请求对象
//		HttpPost httpPost = new HttpPost(url);
//
//		//装填参数
//		StringEntity s = new StringEntity("{\"attribute\": \"Q120000201607000036\",\"order_no\": \"71606040754\",\"ssid\": \"bbcc8072945e48ee89f9b9812020d760\"}", "utf-8");
//		s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//				"application/json"));
//		//设置参数到请求对象中
//		httpPost.setEntity(s);
//		System.out.println("请求地址：" + url);
////        System.out.println("请求参数："+nvps.toString());
//
//		//设置header信息
//		//指定报文头【Content-type】、【User-Agent】
////        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
//		httpPost.setHeader("Content-type", "application/json");
//		httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
//
//		//执行请求操作，并拿到结果（同步阻塞）
//		CloseableHttpResponse response = client.execute(httpPost);
//		//获取结果实体
//		HttpEntity entity = response.getEntity();
//		if (entity != null) {
//			//按指定编码转换结果实体为String类型
//			body = EntityUtils.toString(entity, "utf-8	");
//		}
//		EntityUtils.consume(entity);
//		//释放链接
//		response.close();
//		return body;
//	}


	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

}