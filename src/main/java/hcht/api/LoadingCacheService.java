package hcht.api;

import com.google.common.util.concurrent.RateLimiter;

import java.util.concurrent.ExecutionException;

/**
 * Created by 睿 on 2019/3/27.
 */
public interface LoadingCacheService {
    public RateLimiter getRateLimiter(String key) throws ExecutionException;
    void invalidateAll();
}
