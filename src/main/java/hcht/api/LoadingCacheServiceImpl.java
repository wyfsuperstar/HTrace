package hcht.api;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.RateLimiter;
import com.jeeplus.common.utils.CacheUtils;
import com.jeeplus.modules.sys.utils.DictUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


@Service
public class LoadingCacheServiceImpl implements LoadingCacheService {

    DictUtils du = new DictUtils();
//    System.out.println("============================================================================================================="+DictUtils.getDictValue("按企业","statisticsType","01"));
    LoadingCache<String, RateLimiter> ipRequestCaches = CacheBuilder.newBuilder()
            .maximumSize(200)// 设置缓存个数
            .expireAfterWrite(1, TimeUnit.DAYS)
            .build(new CacheLoader<String, RateLimiter>() {
                @Override
                public RateLimiter load(String s) throws Exception {
//                    DictUtils du = new DictUtils();
////                    du = loadingCacheService.getRateLimiter(DictUtils.CACHE_DICT_MAP);
//                    du = (DictUtils) CacheUtils.get(DictUtils.CACHE_DICT_MAP);
                    System.out.println("============================================================================================================="+DictUtils.getDictValue("rate", "sys_api_rate", "0.1"));
                    return RateLimiter.create(Double.parseDouble(DictUtils.getDictValue("rate", "sys_api_rate", "0.1")));// 新的IP初始化 (限流每秒0.1个令牌响应,即10s一个令牌)
                }
            });
    public RateLimiter getRateLimiter(String key) throws ExecutionException {
        return ipRequestCaches.get(key);
    }
    public void invalidateAll(){
        ipRequestCaches.invalidateAll();
    }
}