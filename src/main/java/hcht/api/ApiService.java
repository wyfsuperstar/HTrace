/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.api;

import com.jeeplus.common.service.BaseService;
import hcht.api.ApiDao;
import com.jeeplus.common.service.CrudService;
import hcht.api.tracegoods.ApiLog;
import hcht.api.tracegoods.RequestData;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * 分类信息Service
 * @author mather
 * @version 2017-04-20
 */
@Service
public class ApiService extends BaseService {
	@Autowired
	protected ApiDao dao;

//	@Transactional(readOnly = false)
	public String findCountBySsid(@Param("attribute") String attribute, @Param("ssid") String ssid){
		String count = dao.findCountBySsid(attribute, ssid);
		return count;
	}
//	@Transactional(readOnly = false)
	public List<HashMap> findscjc01(String beginDate,String endDate){
		List<HashMap> list = dao.findscjc01(beginDate, endDate);
		return list;
	}
//	@Transactional(readOnly = false)
	public List<HashMap> findTraceGoods(@Param("attribute") String attribute, @Param("order_no") String order_no){
		List<HashMap> list = dao.findTraceGoods(attribute, order_no);
		return list;
	}

	@Transactional(readOnly = false)
	public void insertAPILog(ApiLog apiLog){
		dao.insertAPILog(apiLog);
	}

}
