/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillbody;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.mhmanifestbillbody.MhManifestbillBody;

/**
 * 清单表体DAO接口
 * @author BMW
 * @version 2018-11-16
 */
@MyBatisDao
public interface MhManifestbillBodyDao extends CrudDao<MhManifestbillBody> {

	
}