/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillbody;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 清单表体Controller
 * @author BMW
 * @version 2018-11-16
 */
@Controller
@RequestMapping(value = "${adminPath}/mhmanifestbillbody/mhManifestbillBody")
public class MhManifestbillBodyController extends BaseController {

	@Autowired
	private MhManifestbillBodyService mhManifestbillBodyService;
	
	@ModelAttribute
	public MhManifestbillBody get(@RequestParam(required=false) String id) {
		MhManifestbillBody entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhManifestbillBodyService.get(id);
		}
		if (entity == null){
			entity = new MhManifestbillBody();
		}
		return entity;
	}
	
	/**
	 * 清单表体列表页面
	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhManifestbillBody mhManifestbillBody, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhManifestbillBody> page = mhManifestbillBodyService.findPage(new Page<MhManifestbillBody>(request, response), mhManifestbillBody); 
		model.addAttribute("page", page);
		return "system/mhmanifestbillbody/mhManifestbillBodyList";
	}

	/**
	 * 查看，增加，编辑清单表体表单页面
	 */
	@RequiresPermissions(value={"mhmanifestbillbody:mhManifestbillBody:view","mhmanifestbillbody:mhManifestbillBody:add","mhmanifestbillbody:mhManifestbillBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhManifestbillBody mhManifestbillBody, Model model) {
		model.addAttribute("mhManifestbillBody", mhManifestbillBody);
		return "system/mhmanifestbillbody/mhManifestbillBodyForm";
	}

	/**
	 * 保存清单表体
	 */
	@RequiresPermissions(value={"mhmanifestbillbody:mhManifestbillBody:add","mhmanifestbillbody:mhManifestbillBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhManifestbillBody mhManifestbillBody, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhManifestbillBody)){
			return form(mhManifestbillBody, model);
		}
		if(!mhManifestbillBody.getIsNewRecord()){//编辑表单保存
			MhManifestbillBody t = mhManifestbillBodyService.get(mhManifestbillBody.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhManifestbillBody, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhManifestbillBodyService.save(t);//保存
		}else{//新增表单保存
			mhManifestbillBodyService.save(mhManifestbillBody);//保存
		}
		addMessage(redirectAttributes, "保存清单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
	}
	
	/**
	 * 删除清单表体
	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:del")
	@RequestMapping(value = "delete")
	public String delete(MhManifestbillBody mhManifestbillBody, RedirectAttributes redirectAttributes) {
		mhManifestbillBodyService.delete(mhManifestbillBody);
		addMessage(redirectAttributes, "删除清单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
	}
	
	/**
	 * 批量删除清单表体
	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhManifestbillBodyService.delete(mhManifestbillBodyService.get(id));
		}
		addMessage(redirectAttributes, "删除清单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhManifestbillBody mhManifestbillBody, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "清单表体"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhManifestbillBody> page = mhManifestbillBodyService.findPage(new Page<MhManifestbillBody>(request, response, -1), mhManifestbillBody);
    		new ExportExcel("清单表体", MhManifestbillBody.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出清单表体记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhManifestbillBody> list = ei.getDataList(MhManifestbillBody.class);
			for (MhManifestbillBody mhManifestbillBody : list){
				try{
					mhManifestbillBodyService.save(mhManifestbillBody);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条清单表体记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条清单表体记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入清单表体失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
    }
	
	/**
	 * 下载导入清单表体数据模板
	 */
	@RequiresPermissions("mhmanifestbillbody:mhManifestbillBody:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "清单表体数据导入模板.xlsx";
    		List<MhManifestbillBody> list = Lists.newArrayList(); 
    		new ExportExcel("清单表体数据", MhManifestbillBody.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillbody/mhManifestbillBody/?repage";
    }
	
	
	

}