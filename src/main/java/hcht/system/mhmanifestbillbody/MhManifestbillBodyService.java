/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillbody;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 清单表体Service
 * @author BMW
 * @version 2018-11-16
 */
@Service
@Transactional(readOnly = true)
public class MhManifestbillBodyService extends CrudService<MhManifestbillBodyDao, MhManifestbillBody> {

	public MhManifestbillBody get(String id) {
		return super.get(id);
	}
	
	public List<MhManifestbillBody> findList(MhManifestbillBody mhManifestbillBody) {
		return super.findList(mhManifestbillBody);
	}
	
	public Page<MhManifestbillBody> findPage(Page<MhManifestbillBody> page, MhManifestbillBody mhManifestbillBody) {
		return super.findPage(page, mhManifestbillBody);
	}
	
	@Transactional(readOnly = false)
	public void save(MhManifestbillBody mhManifestbillBody) {
		super.save(mhManifestbillBody);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhManifestbillBody mhManifestbillBody) {
		super.delete(mhManifestbillBody);
	}
	
	
	
	
}