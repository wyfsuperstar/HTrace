/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillbody;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 清单表体Entity
 * @author BMW
 * @version 2018-11-16
 */
public class MhManifestbillBody extends DataEntity<MhManifestbillBody> {
	
	private static final long serialVersionUID = 1L;
	private String ioGoodsSerialNo;		// 出区进口商品流水号
	private Long seqNo;		// 序号
	private String entCode;		// 企业备案号
	private String goodsRegNo;		// 商品备案号
	private String declIiNo;		// 进口入区申报号
	private String logisticsno;		// 运单号
	private String orderNo;		// 订单号
	private String paymentNo;		// 支付交易号
	private String hsCode;		// HS编码
	private String entCname;		// 企业中文名
	private String goodsNo;		// 商品货号
	private String goodsName;		// 商品名称
	private String consigneeCname;		// 收货人
	private String consigneeIdtype;		// 收货人证件类型
	private String originCountryCode;		// 原产国代码
	private String qtyUnitCode;		// 包装单位代码
	private String consigneeNo;		// 收货人编号
	private Double qty;		// 数量
	private Double goodsUnitPrice;		// 单价
	private Double goodsTotalValues;		// 总价
	private String currUnit;		// 货币单位代码
	private String remark;		// 备注
	private String goodsStatus;		// 商品状态: 状态代码，0未查验 1待查验 2合格 3不合格
	private String headid;		// 表头主键
	private Date storageTime;		// 报文入库时间
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhManifestbillBody() {
		super();
	}

	public MhManifestbillBody(String id){
		super(id);
	}

	@ExcelField(title="出区进口商品流水号", align=2, sort=0)
	public String getIoGoodsSerialNo() {
		return ioGoodsSerialNo;
	}

	public void setIoGoodsSerialNo(String ioGoodsSerialNo) {
		this.ioGoodsSerialNo = ioGoodsSerialNo;
	}
	
	@ExcelField(title="序号", align=2, sort=1)
	public Long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	
	@ExcelField(title="企业备案号", align=2, sort=2)
	public String getEntCode() {
		return entCode;
	}

	public void setEntCode(String entCode) {
		this.entCode = entCode;
	}
	
	@ExcelField(title="商品备案号", align=2, sort=3)
	public String getGoodsRegNo() {
		return goodsRegNo;
	}

	public void setGoodsRegNo(String goodsRegNo) {
		this.goodsRegNo = goodsRegNo;
	}
	
	@ExcelField(title="进口入区申报号", align=2, sort=4)
	public String getDeclIiNo() {
		return declIiNo;
	}

	public void setDeclIiNo(String declIiNo) {
		this.declIiNo = declIiNo;
	}
	
	@ExcelField(title="运单号", align=2, sort=5)
	public String getLogisticsno() {
		return logisticsno;
	}

	public void setLogisticsno(String logisticsno) {
		this.logisticsno = logisticsno;
	}
	
	@ExcelField(title="订单号", align=2, sort=6)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="支付交易号", align=2, sort=7)
	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	
	@ExcelField(title="HS编码", align=2, sort=8)
	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
	
	@ExcelField(title="企业中文名", align=2, sort=9)
	public String getEntCname() {
		return entCname;
	}

	public void setEntCname(String entCname) {
		this.entCname = entCname;
	}
	
	@ExcelField(title="商品货号", align=2, sort=10)
	public String getGoodsNo() {
		return goodsNo;
	}

	public void setGoodsNo(String goodsNo) {
		this.goodsNo = goodsNo;
	}
	
	@ExcelField(title="商品名称", align=2, sort=11)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	@ExcelField(title="收货人", align=2, sort=12)
	public String getConsigneeCname() {
		return consigneeCname;
	}

	public void setConsigneeCname(String consigneeCname) {
		this.consigneeCname = consigneeCname;
	}
	
	@ExcelField(title="收货人证件类型", align=2, sort=13)
	public String getConsigneeIdtype() {
		return consigneeIdtype;
	}

	public void setConsigneeIdtype(String consigneeIdtype) {
		this.consigneeIdtype = consigneeIdtype;
	}
	
	@ExcelField(title="原产国代码", align=2, sort=14)
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
	
	@ExcelField(title="包装单位代码", align=2, sort=15)
	public String getQtyUnitCode() {
		return qtyUnitCode;
	}

	public void setQtyUnitCode(String qtyUnitCode) {
		this.qtyUnitCode = qtyUnitCode;
	}
	
	@ExcelField(title="收货人编号", align=2, sort=16)
	public String getConsigneeNo() {
		return consigneeNo;
	}

	public void setConsigneeNo(String consigneeNo) {
		this.consigneeNo = consigneeNo;
	}
	
	@ExcelField(title="数量", align=2, sort=17)
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}
	
	@ExcelField(title="单价", align=2, sort=18)
	public Double getGoodsUnitPrice() {
		return goodsUnitPrice;
	}

	public void setGoodsUnitPrice(Double goodsUnitPrice) {
		this.goodsUnitPrice = goodsUnitPrice;
	}
	
	@ExcelField(title="总价", align=2, sort=19)
	public Double getGoodsTotalValues() {
		return goodsTotalValues;
	}

	public void setGoodsTotalValues(Double goodsTotalValues) {
		this.goodsTotalValues = goodsTotalValues;
	}
	
	@ExcelField(title="货币单位代码", align=2, sort=20)
	public String getCurrUnit() {
		return currUnit;
	}

	public void setCurrUnit(String currUnit) {
		this.currUnit = currUnit;
	}
	
	@ExcelField(title="备注", align=2, sort=21)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="商品状态: 状态代码，0未查验 1待查验 2合格 3不合格", align=2, sort=22)
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	
	@ExcelField(title="表头主键", align=2, sort=23)
	public String getHeadid() {
		return headid;
	}

	public void setHeadid(String headid) {
		this.headid = headid;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=24)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}