/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.apilog;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * api日志Service
 * @author BMW
 * @version 2019-03-20
 */
@Service
@Transactional(readOnly = true)
public class HtApilogService extends CrudService<HtApilogDao, HtApilog> {

	public HtApilog get(String id) {
		return super.get(id);
	}
	
	public List<HtApilog> findList(HtApilog htApilog) {
		return super.findList(htApilog);
	}
	
	public Page<HtApilog> findPage(Page<HtApilog> page, HtApilog htApilog) {
		return super.findPage(page, htApilog);
	}
	
	@Transactional(readOnly = false)
	public void save(HtApilog htApilog) {
		super.save(htApilog);
	}
	
	@Transactional(readOnly = false)
	public void delete(HtApilog htApilog) {
		super.delete(htApilog);
	}
	
	
	
	
}