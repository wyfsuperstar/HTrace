/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.apilog;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * api日志DAO接口
 * @author BMW
 * @version 2019-03-20
 */
@MyBatisDao
public interface HtApilogDao extends CrudDao<HtApilog> {

	
}