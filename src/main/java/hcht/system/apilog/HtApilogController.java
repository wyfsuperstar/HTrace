/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.apilog;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * api日志Controller
 * @author BMW
 * @version 2019-03-20
 */
@Controller
@RequestMapping(value = "${adminPath}/apilog/htApilog")
public class HtApilogController extends BaseController {

	@Autowired
	private HtApilogService htApilogService;
	
	@ModelAttribute
	public HtApilog get(@RequestParam(required=false) String id) {
		HtApilog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = htApilogService.get(id);
		}
		if (entity == null){
			entity = new HtApilog();
		}
		return entity;
	}
	
	/**
	 * api日志列表页面
	 */
	@RequestMapping(value = {"list", ""})
	public String list(HtApilog htApilog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HtApilog> page = htApilogService.findPage(new Page<HtApilog>(request, response), htApilog); 
		model.addAttribute("page", page);
		return "system/apilog/htApilogList";
	}

	/**
	 * 查看，增加，编辑api日志表单页面
	 */
	@RequestMapping(value = "form")
	public String form(HtApilog htApilog, Model model) {
		model.addAttribute("htApilog", htApilog);
		return "system/apilog/htApilogForm";
	}

	/**
	 * 保存api日志
	 */
	@RequestMapping(value = "save")
	public String save(HtApilog htApilog, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, htApilog)){
			return form(htApilog, model);
		}
		if(!htApilog.getIsNewRecord()){//编辑表单保存
			HtApilog t = htApilogService.get(htApilog.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(htApilog, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			htApilogService.save(t);//保存
		}else{//新增表单保存
			htApilogService.save(htApilog);//保存
		}
		addMessage(redirectAttributes, "保存api日志成功");
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
	}
	
	/**
	 * 删除api日志
	 */
	@RequestMapping(value = "delete")
	public String delete(HtApilog htApilog, RedirectAttributes redirectAttributes) {
		htApilogService.delete(htApilog);
		addMessage(redirectAttributes, "删除api日志成功");
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
	}
	
	/**
	 * 批量删除api日志
	 */
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			htApilogService.delete(htApilogService.get(id));
		}
		addMessage(redirectAttributes, "删除api日志成功");
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(HtApilog htApilog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "api日志"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<HtApilog> page = htApilogService.findPage(new Page<HtApilog>(request, response, -1), htApilog);
    		new ExportExcel("api日志", HtApilog.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出api日志记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
    }

	/**
	 * 导入Excel数据

	 */
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<HtApilog> list = ei.getDataList(HtApilog.class);
			for (HtApilog htApilog : list){
				try{
					htApilogService.save(htApilog);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条api日志记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条api日志记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入api日志失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
    }
	
	/**
	 * 下载导入api日志数据模板
	 */
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "api日志数据导入模板.xlsx";
    		List<HtApilog> list = Lists.newArrayList(); 
    		new ExportExcel("api日志数据", HtApilog.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/apilog/htApilog/?repage";
    }
	
	
	

}