/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.apilog;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * api日志Entity
 * @author BMW
 * @version 2019-03-20
 */
public class HtApilog extends DataEntity<HtApilog> {
	
	private static final long serialVersionUID = 1L;
	private String apiType;		// 接口类型
	private String attribute;		// 企业备案号
	private String ssid;		// SSID
	private Date readDate;		// 访问时间
	private String ip;		// IP
	private String request;		// 参数
	private String success;		// 成功标志
	private String errorCode;		// 错误代码
	private String errorMsg;		// 错误信息
	private Date beginReadDate;		// 开始 访问时间
	private Date endReadDate;		// 结束 访问时间
	
	public HtApilog() {
		super();
	}

	public HtApilog(String id){
		super(id);
	}

	@ExcelField(title="接口类型", align=2, sort=1)
	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}
	
	@ExcelField(title="企业备案号", align=2, sort=2)
	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	@ExcelField(title="SSID", align=2, sort=3)
	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="访问时间", align=2, sort=4)
	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	
	@ExcelField(title="IP", align=2, sort=5)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	@ExcelField(title="参数", align=2, sort=6)
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}
	
	@ExcelField(title="成功标志", align=2, sort=7)
	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}
	
	@ExcelField(title="错误代码", align=2, sort=8)
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	@ExcelField(title="错误信息", align=2, sort=9)
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public Date getBeginReadDate() {
		return beginReadDate;
	}

	public void setBeginReadDate(Date beginReadDate) {
		this.beginReadDate = beginReadDate;
	}
	
	public Date getEndReadDate() {
		return endReadDate;
	}

	public void setEndReadDate(Date endReadDate) {
		this.endReadDate = endReadDate;
	}
		
}