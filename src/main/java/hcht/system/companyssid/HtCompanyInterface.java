/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.companyssid;

import org.hibernate.validator.constraints.Length;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 企业API ssidEntity
 * @author BMW
 * @version 2019-03-20
 */
public class HtCompanyInterface extends DataEntity<HtCompanyInterface> {
	
	private static final long serialVersionUID = 1L;
	private String ssid;		// 加密码
	
	public HtCompanyInterface() {
		super();
	}

	public HtCompanyInterface(String id){
		super(id);
	}

	@Length(min=1, max=32, message="加密码长度必须介于 1 和 32 之间")
	@ExcelField(title="加密码", align=2, sort=1)
	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	private String attribute;

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	private String entCname;

	public String getEntCname() {
		return entCname;
	}

	public void setEntCname(String entCname) {
		this.entCname = entCname;
	}
	private String entId;

	public String getEntId() {
		return entId;
	}

	public void setEntId(String entId) {
		this.entId = entId;
	}
}