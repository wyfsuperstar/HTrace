/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.companyssid;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.companyssid.HtCompanyInterface;

/**
 * 企业API ssidDAO接口
 * @author BMW
 * @version 2019-03-20
 */
@MyBatisDao
public interface HtCompanyInterfaceDao extends CrudDao<HtCompanyInterface> {

	
}