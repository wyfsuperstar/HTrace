/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.companyssid;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 企业API ssidService
 * @author BMW
 * @version 2019-03-20
 */
@Service
@Transactional(readOnly = true)
public class HtCompanyInterfaceService extends CrudService<HtCompanyInterfaceDao, HtCompanyInterface> {

	public HtCompanyInterface get(String id) {
		return super.get(id);
	}
	
	public List<HtCompanyInterface> findList(HtCompanyInterface htCompanyInterface) {
		return super.findList(htCompanyInterface);
	}
	
	public Page<HtCompanyInterface> findPage(Page<HtCompanyInterface> page, HtCompanyInterface htCompanyInterface) {
		return super.findPage(page, htCompanyInterface);
	}
	
	@Transactional(readOnly = false)
	public void save(HtCompanyInterface htCompanyInterface) {
		if(htCompanyInterface.getEntId()!=null&&!htCompanyInterface.getEntId().equals("")){
			dao.update(htCompanyInterface);
		}else {
			dao.insert(htCompanyInterface);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(HtCompanyInterface htCompanyInterface) {
		super.delete(htCompanyInterface);
	}
	
	
	
	
}