/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.companyssid;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 企业API ssidController
 * @author BMW
 * @version 2019-03-20
 */
@Controller
@RequestMapping(value = "${adminPath}/companyssid/htCompanyInterface")
public class HtCompanyInterfaceController extends BaseController {

	@Autowired
	private HtCompanyInterfaceService htCompanyInterfaceService;
	
	@ModelAttribute
	public HtCompanyInterface get(@RequestParam(required=false) String id) {
		HtCompanyInterface entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = htCompanyInterfaceService.get(id);
		}
		if (entity == null){
			entity = new HtCompanyInterface();
		}
		return entity;
	}
	
	/**
	 * 企业API ssid列表页面
	 */
	@RequestMapping(value = {"list", ""})
	public String list(HtCompanyInterface htCompanyInterface, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HtCompanyInterface> page = htCompanyInterfaceService.findPage(new Page<HtCompanyInterface>(request, response), htCompanyInterface); 
		model.addAttribute("page", page);
		return "system/companyssid/htCompanyInterfaceList";
	}

	/**
	 * 查看，增加，编辑企业API ssid表单页面
	 */
	@RequestMapping(value = "form")
	public String form(HtCompanyInterface htCompanyInterface, Model model) {
		if(htCompanyInterface.getSsid()==null||htCompanyInterface.getSsid().equals("")){
			htCompanyInterface.setSsid(htCompanyInterface.getId());
		}
		model.addAttribute("htCompanyInterface", htCompanyInterface);
		return "system/companyssid/htCompanyInterfaceForm";
	}

	/**
	 * 保存企业API ssid
	 */
	@RequestMapping(value = "save")
	public String save(HtCompanyInterface htCompanyInterface, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, htCompanyInterface)){
			return form(htCompanyInterface, model);
		}
		if(htCompanyInterface.getEntId()!=null&&!htCompanyInterface.getEntId().equals("")){//编辑表单保存
			HtCompanyInterface t = htCompanyInterfaceService.get(htCompanyInterface.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(htCompanyInterface, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			htCompanyInterfaceService.save(t);//保存
		}else{//新增表单保存
			htCompanyInterfaceService.save(htCompanyInterface);//保存
		}
		addMessage(redirectAttributes, "保存企业API ssid成功");
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
	}
	
	/**
	 * 删除企业API ssid
	 */
	@RequestMapping(value = "delete")
	public String delete(HtCompanyInterface htCompanyInterface, RedirectAttributes redirectAttributes) {
		htCompanyInterfaceService.delete(htCompanyInterface);
		addMessage(redirectAttributes, "删除企业API ssid成功");
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
	}
	
	/**
	 * 批量删除企业API ssid
	 */
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			htCompanyInterfaceService.delete(htCompanyInterfaceService.get(id));
		}
		addMessage(redirectAttributes, "删除企业API ssid成功");
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(HtCompanyInterface htCompanyInterface, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "企业API ssid"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<HtCompanyInterface> page = htCompanyInterfaceService.findPage(new Page<HtCompanyInterface>(request, response, -1), htCompanyInterface);
    		new ExportExcel("企业API ssid", HtCompanyInterface.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出企业API ssid记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
    }

	/**
	 * 导入Excel数据

	 */
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<HtCompanyInterface> list = ei.getDataList(HtCompanyInterface.class);
			for (HtCompanyInterface htCompanyInterface : list){
				try{
					htCompanyInterfaceService.save(htCompanyInterface);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条企业API ssid记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条企业API ssid记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入企业API ssid失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
    }
	
	/**
	 * 下载导入企业API ssid数据模板
	 */
    @RequestMapping(value = "import/templat	e")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "企业API ssid数据导入模板.xlsx";
    		List<HtCompanyInterface> list = Lists.newArrayList(); 
    		new ExportExcel("企业API ssid数据", HtCompanyInterface.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/companyssid/htCompanyInterface/?repage";
    }
	
	
	

}