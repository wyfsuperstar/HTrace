/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhbill;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 运单Entity
 * @author BMW
 * @version 2018-11-16
 */
public class MhBill extends DataEntity<MhBill> {
	
	private static final long serialVersionUID = 1L;
	private String billSerialNo;		// 运单流水号      
	private String orderNo;		// 订单编号        
	private String logisticsNo;		// 物流运单号      
	private String mainWbNo;		// 总运单号        
	private String logisticsCode;		// 物流企业编号    
	private String ecpCode;		// 电商平台编号    
	private String getWaybillNo;		// 提运单号        
	private String voyageNo;		// 航班航次号      
	private String transCode;		// 运输工具编号 
	private Double freight;		// 运费 
	private Double supportValue;		// 保价金额 
	private String currencyCode;		// 币制编号 
	private String ieType;		// 进/出口编号 
	private String goodsName;		// 商品名称 
	private Double countNum;		// 件数 
	private Double weight;		// 毛重 
	private Double netWeight;		// 净重 
	private String consignorName;		// 发货人名称 
	private String consignorTel;		// 发货人电话 
	private String consignorAddress;		// 发货人地址 
	private String consignorCountryCode;		// 发货人所在国编号
	private String consigneeName;		// 收货人名称 
	private String consigneeTel;		// 收货人电话 
	private String consigneeAddress;		// 收货人地址 
	private String consigneeCountryCode;		// 收货人所在国编号
	private String remark;		// 备注
	private String receiptType;		// 回执类型
	private String receiptNo;		// 回执单号
	private String msgCode;		// 状态代码
	private String msgName;		// 状态名称
	private String msgDesc;		// 操作信息
	private Date msgTime;		// 回执时间
	private String type;		// 区分直邮（Z）和保税(B)
	private Date createTime;		// create_time
	private String logisticsName;		// 物流企业名称
	private String flag;		// 是否单独报检   1 单独报检
	private Date storageTime;		// 报文入库时间
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhBill() {
		super();
	}

	public MhBill(String id){
		super(id);
	}

	@ExcelField(title="运单流水号      ", align=2, sort=0)
	public String getBillSerialNo() {
		return billSerialNo;
	}

	public void setBillSerialNo(String billSerialNo) {
		this.billSerialNo = billSerialNo;
	}
	
	@ExcelField(title="订单编号        ", align=2, sort=1)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="物流运单号      ", align=2, sort=2)
	public String getLogisticsNo() {
		return logisticsNo;
	}

	public void setLogisticsNo(String logisticsNo) {
		this.logisticsNo = logisticsNo;
	}
	
	@ExcelField(title="总运单号        ", align=2, sort=3)
	public String getMainWbNo() {
		return mainWbNo;
	}

	public void setMainWbNo(String mainWbNo) {
		this.mainWbNo = mainWbNo;
	}
	
	@ExcelField(title="物流企业编号    ", align=2, sort=4)
	public String getLogisticsCode() {
		return logisticsCode;
	}

	public void setLogisticsCode(String logisticsCode) {
		this.logisticsCode = logisticsCode;
	}
	
	@ExcelField(title="电商平台编号    ", align=2, sort=5)
	public String getEcpCode() {
		return ecpCode;
	}

	public void setEcpCode(String ecpCode) {
		this.ecpCode = ecpCode;
	}
	
	@ExcelField(title="提运单号        ", align=2, sort=6)
	public String getGetWaybillNo() {
		return getWaybillNo;
	}

	public void setGetWaybillNo(String getWaybillNo) {
		this.getWaybillNo = getWaybillNo;
	}
	
	@ExcelField(title="航班航次号      ", align=2, sort=7)
	public String getVoyageNo() {
		return voyageNo;
	}

	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	
	@ExcelField(title="运输工具编号 ", align=2, sort=8)
	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	
	@ExcelField(title="运费 ", align=2, sort=9)
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}
	
	@ExcelField(title="保价金额 ", align=2, sort=10)
	public Double getSupportValue() {
		return supportValue;
	}

	public void setSupportValue(Double supportValue) {
		this.supportValue = supportValue;
	}
	
	@ExcelField(title="币制编号 ", align=2, sort=11)
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	@ExcelField(title="进/出口编号 ", align=2, sort=12)
	public String getIeType() {
		return ieType;
	}

	public void setIeType(String ieType) {
		this.ieType = ieType;
	}
	
	@ExcelField(title="商品名称 ", align=2, sort=13)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	@ExcelField(title="件数 ", align=2, sort=14)
	public Double getCountNum() {
		return countNum;
	}

	public void setCountNum(Double countNum) {
		this.countNum = countNum;
	}
	
	@ExcelField(title="毛重 ", align=2, sort=15)
	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
	@ExcelField(title="净重 ", align=2, sort=16)
	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}
	
	@ExcelField(title="发货人名称 ", align=2, sort=17)
	public String getConsignorName() {
		return consignorName;
	}

	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}
	
	@ExcelField(title="发货人电话 ", align=2, sort=18)
	public String getConsignorTel() {
		return consignorTel;
	}

	public void setConsignorTel(String consignorTel) {
		this.consignorTel = consignorTel;
	}
	
	@ExcelField(title="发货人地址 ", align=2, sort=19)
	public String getConsignorAddress() {
		return consignorAddress;
	}

	public void setConsignorAddress(String consignorAddress) {
		this.consignorAddress = consignorAddress;
	}
	
	@ExcelField(title="发货人所在国编号", align=2, sort=20)
	public String getConsignorCountryCode() {
		return consignorCountryCode;
	}

	public void setConsignorCountryCode(String consignorCountryCode) {
		this.consignorCountryCode = consignorCountryCode;
	}
	
	@ExcelField(title="收货人名称 ", align=2, sort=21)
	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	
	@ExcelField(title="收货人电话 ", align=2, sort=22)
	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}
	
	@ExcelField(title="收货人地址 ", align=2, sort=23)
	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	
	@ExcelField(title="收货人所在国编号", align=2, sort=24)
	public String getConsigneeCountryCode() {
		return consigneeCountryCode;
	}

	public void setConsigneeCountryCode(String consigneeCountryCode) {
		this.consigneeCountryCode = consigneeCountryCode;
	}
	
	@ExcelField(title="备注", align=2, sort=25)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="回执类型", align=2, sort=26)
	public String getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}
	
	@ExcelField(title="回执单号", align=2, sort=27)
	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	
	@ExcelField(title="状态代码", align=2, sort=28)
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	
	@ExcelField(title="状态名称", align=2, sort=29)
	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}
	
	@ExcelField(title="操作信息", align=2, sort=30)
	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="回执时间", align=2, sort=31)
	public Date getMsgTime() {
		return msgTime;
	}

	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	
	@ExcelField(title="区分直邮（Z）和保税(B)", align=2, sort=33)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="create_time", align=2, sort=34)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@ExcelField(title="物流企业名称", align=2, sort=35)
	public String getLogisticsName() {
		return logisticsName;
	}

	public void setLogisticsName(String logisticsName) {
		this.logisticsName = logisticsName;
	}
	
	@ExcelField(title="是否单独报检   1 单独报检", align=2, sort=36)
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=37)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}