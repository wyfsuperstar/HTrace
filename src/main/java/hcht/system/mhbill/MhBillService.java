/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhbill;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 运单Service
 * @author BMW
 * @version 2018-11-16
 */
@Service
@Transactional(readOnly = true)
public class MhBillService extends CrudService<MhBillDao, MhBill> {

	public MhBill get(String id) {
		return super.get(id);
	}
	
	public List<MhBill> findList(MhBill mhBill) {
		return super.findList(mhBill);
	}
	
	public Page<MhBill> findPage(Page<MhBill> page, MhBill mhBill) {
		return super.findPage(page, mhBill);
	}
	
	@Transactional(readOnly = false)
	public void save(MhBill mhBill) {
		super.save(mhBill);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhBill mhBill) {
		super.delete(mhBill);
	}
	
	
	
	
}