/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhbill;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.mhbill.MhBill;

/**
 * 运单DAO接口
 * @author BMW
 * @version 2018-11-16
 */
@MyBatisDao
public interface MhBillDao extends CrudDao<MhBill> {

	
}