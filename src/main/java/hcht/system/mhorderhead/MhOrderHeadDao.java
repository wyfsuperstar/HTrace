/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderhead;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 订单表头DAO接口
 * @author BMW
 * @version 2018-11-19
 */
@MyBatisDao
public interface MhOrderHeadDao extends CrudDao<MhOrderHead> {

	
}