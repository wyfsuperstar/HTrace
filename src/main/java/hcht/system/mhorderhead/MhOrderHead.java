/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderhead;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 订单表头Entity
 * @author BMW
 * @version 2018-11-19
 */
public class MhOrderHead extends DataEntity<MhOrderHead> {
	
	private static final long serialVersionUID = 1L;
	private String orderSerialNo;		// 订单流水号 
	private String orderNo;		// 订单编号 
	private String batchNo;		// 订单批次号 
	private String ecpCode;		// 电商平台编号    
	private String ieType;		// 进/出口编号     I进口 E出口
	private String cbeCode;		// 电商企业编号    
	private String bizType;		// 业务类型 1保税备货 2保税集货  3邮件 4快件
	private String agentCode;		// 代理申报企业编号
	private String consigneeName;		// 收货人名称 
	private String consigneeTel;		// 收货人电话 
	private String consigneeAddress;		// 收货人地址 
	private String consigneeCountryCode;		// 收货人所在国编号
	private String consignorCname;		// 发货人 
	private String consignorAddress;		// 发货人地址 
	private String consignorTel;		// 发货人电话 
	private String consignorCountryCode;		// 发货人所在国编号
	private String idType;		// 证件类型编号 1身份证 2军官证 3护照 4其他
	private String idCard;		// 证件号码 
	private Double goodsValue;		// 订单商品货款    
	private Double freight;		// 订单商品运费 
	private String currency;		// 币制编号 
	private String tradeCountryCode;		// 贸易国别编号 
	private String remark;		// 备注 
	private String status;		// 状态
	private String type;		// 区分直邮（Z）和保税(B)
	private Date createTime;		// 入库时间
	private Date decltime;		// 申报时间
	private String ecpName;		// 电商平台名称
	private String cbeName;		// 电商企业名称
	private String flag;		// 是否单独报检   1 单独报检
	private Date storageTime;		// 报文入库时间
	private String ciq;		// ciq
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhOrderHead() {
		super();
	}

	public MhOrderHead(String id){
		super(id);
	}

	@ExcelField(title="订单流水号 ", align=2, sort=0)
	public String getOrderSerialNo() {
		return orderSerialNo;
	}

	public void setOrderSerialNo(String orderSerialNo) {
		this.orderSerialNo = orderSerialNo;
	}
	
	@ExcelField(title="订单编号 ", align=2, sort=1)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="订单批次号 ", align=2, sort=2)
	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	
	@ExcelField(title="电商平台编号    ", align=2, sort=3)
	public String getEcpCode() {
		return ecpCode;
	}

	public void setEcpCode(String ecpCode) {
		this.ecpCode = ecpCode;
	}
	
	@ExcelField(title="进/出口编号     I进口 E出口", align=2, sort=4)
	public String getIeType() {
		return ieType;
	}

	public void setIeType(String ieType) {
		this.ieType = ieType;
	}
	
	@ExcelField(title="电商企业编号    ", align=2, sort=5)
	public String getCbeCode() {
		return cbeCode;
	}

	public void setCbeCode(String cbeCode) {
		this.cbeCode = cbeCode;
	}
	
	@ExcelField(title="业务类型 1保税备货 2保税集货  3邮件 4快件", align=2, sort=6)
	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
	@ExcelField(title="代理申报企业编号", align=2, sort=7)
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	
	@ExcelField(title="收货人名称 ", align=2, sort=8)
	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	
	@ExcelField(title="收货人电话 ", align=2, sort=9)
	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}
	
	@ExcelField(title="收货人地址 ", align=2, sort=10)
	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	
	@ExcelField(title="收货人所在国编号", align=2, sort=11)
	public String getConsigneeCountryCode() {
		return consigneeCountryCode;
	}

	public void setConsigneeCountryCode(String consigneeCountryCode) {
		this.consigneeCountryCode = consigneeCountryCode;
	}
	
	@ExcelField(title="发货人 ", align=2, sort=12)
	public String getConsignorCname() {
		return consignorCname;
	}

	public void setConsignorCname(String consignorCname) {
		this.consignorCname = consignorCname;
	}
	
	@ExcelField(title="发货人地址 ", align=2, sort=13)
	public String getConsignorAddress() {
		return consignorAddress;
	}

	public void setConsignorAddress(String consignorAddress) {
		this.consignorAddress = consignorAddress;
	}
	
	@ExcelField(title="发货人电话 ", align=2, sort=14)
	public String getConsignorTel() {
		return consignorTel;
	}

	public void setConsignorTel(String consignorTel) {
		this.consignorTel = consignorTel;
	}
	
	@ExcelField(title="发货人所在国编号", align=2, sort=15)
	public String getConsignorCountryCode() {
		return consignorCountryCode;
	}

	public void setConsignorCountryCode(String consignorCountryCode) {
		this.consignorCountryCode = consignorCountryCode;
	}
	
	@ExcelField(title="证件类型编号 1身份证 2军官证 3护照 4其他", align=2, sort=16)
	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}
	
	@ExcelField(title="证件号码 ", align=2, sort=17)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	@ExcelField(title="订单商品货款    ", align=2, sort=18)
	public Double getGoodsValue() {
		return goodsValue;
	}

	public void setGoodsValue(Double goodsValue) {
		this.goodsValue = goodsValue;
	}
	
	@ExcelField(title="订单商品运费 ", align=2, sort=19)
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}
	
	@ExcelField(title="币制编号 ", align=2, sort=20)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@ExcelField(title="贸易国别编号 ", align=2, sort=21)
	public String getTradeCountryCode() {
		return tradeCountryCode;
	}

	public void setTradeCountryCode(String tradeCountryCode) {
		this.tradeCountryCode = tradeCountryCode;
	}
	
	@ExcelField(title="备注 ", align=2, sort=22)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="状态", align=2, sort=24)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="区分直邮（Z）和保税(B)", align=2, sort=25)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="入库时间", align=2, sort=26)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="申报时间", align=2, sort=27)
	public Date getDecltime() {
		return decltime;
	}

	public void setDecltime(Date decltime) {
		this.decltime = decltime;
	}
	
	@ExcelField(title="电商平台名称", align=2, sort=28)
	public String getEcpName() {
		return ecpName;
	}

	public void setEcpName(String ecpName) {
		this.ecpName = ecpName;
	}
	
	@ExcelField(title="电商企业名称", align=2, sort=29)
	public String getCbeName() {
		return cbeName;
	}

	public void setCbeName(String cbeName) {
		this.cbeName = cbeName;
	}
	
	@ExcelField(title="是否单独报检   1 单独报检", align=2, sort=30)
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=31)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	@ExcelField(title="ciq", align=2, sort=32)
	public String getCiq() {
		return ciq;
	}

	public void setCiq(String ciq) {
		this.ciq = ciq;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}