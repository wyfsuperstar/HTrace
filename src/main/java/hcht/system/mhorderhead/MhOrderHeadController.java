/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderhead;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import hcht.system.mhorderhead.MhOrderHead;
import hcht.system.mhorderhead.MhOrderHeadService;

/**
 * 订单表头Controller
 * @author BMW
 * @version 2018-11-19
 */
@Controller
@RequestMapping(value = "${adminPath}/mhorderhead/mhOrderHead")
public class MhOrderHeadController extends BaseController {

	@Autowired
	private MhOrderHeadService mhOrderHeadService;
	
	@ModelAttribute
	public MhOrderHead get(@RequestParam(required=false) String id) {
		MhOrderHead entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhOrderHeadService.get(id);
		}
		if (entity == null){
			entity = new MhOrderHead();
		}
		return entity;
	}
	
	/**
	 * 订单表头列表页面
	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhOrderHead mhOrderHead, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhOrderHead> page = mhOrderHeadService.findPage(new Page<MhOrderHead>(request, response), mhOrderHead); 
		model.addAttribute("page", page);
		return "system/mhorderhead/mhOrderHeadList";
	}

	/**
	 * 查看，增加，编辑订单表头表单页面
	 */
	@RequiresPermissions(value={"mhorderhead:mhOrderHead:view","mhorderhead:mhOrderHead:add","mhorderhead:mhOrderHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhOrderHead mhOrderHead, Model model) {
		model.addAttribute("mhOrderHead", mhOrderHead);
		return "system/mhorderhead/mhOrderHeadForm";
	}

	/**
	 * 保存订单表头
	 */
	@RequiresPermissions(value={"mhorderhead:mhOrderHead:add","mhorderhead:mhOrderHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhOrderHead mhOrderHead, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhOrderHead)){
			return form(mhOrderHead, model);
		}
		if(!mhOrderHead.getIsNewRecord()){//编辑表单保存
			MhOrderHead t = mhOrderHeadService.get(mhOrderHead.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhOrderHead, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhOrderHeadService.save(t);//保存
		}else{//新增表单保存
			mhOrderHeadService.save(mhOrderHead);//保存
		}
		addMessage(redirectAttributes, "保存订单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
	}
	
	/**
	 * 删除订单表头
	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:del")
	@RequestMapping(value = "delete")
	public String delete(MhOrderHead mhOrderHead, RedirectAttributes redirectAttributes) {
		mhOrderHeadService.delete(mhOrderHead);
		addMessage(redirectAttributes, "删除订单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
	}
	
	/**
	 * 批量删除订单表头
	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhOrderHeadService.delete(mhOrderHeadService.get(id));
		}
		addMessage(redirectAttributes, "删除订单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhOrderHead mhOrderHead, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单表头"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhOrderHead> page = mhOrderHeadService.findPage(new Page<MhOrderHead>(request, response, -1), mhOrderHead);
    		new ExportExcel("订单表头", MhOrderHead.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出订单表头记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhOrderHead> list = ei.getDataList(MhOrderHead.class);
			for (MhOrderHead mhOrderHead : list){
				try{
					mhOrderHeadService.save(mhOrderHead);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条订单表头记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条订单表头记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入订单表头失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
    }
	
	/**
	 * 下载导入订单表头数据模板
	 */
	@RequiresPermissions("mhorderhead:mhOrderHead:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单表头数据导入模板.xlsx";
    		List<MhOrderHead> list = Lists.newArrayList(); 
    		new ExportExcel("订单表头数据", MhOrderHead.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderhead/mhOrderHead/?repage";
    }
	
	
	

}