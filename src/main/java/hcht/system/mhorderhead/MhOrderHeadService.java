/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderhead;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.system.mhorderhead.MhOrderHead;
import hcht.system.mhorderhead.MhOrderHeadDao;

/**
 * 订单表头Service
 * @author BMW
 * @version 2018-11-19
 */
@Service
@Transactional(readOnly = true)
public class MhOrderHeadService extends CrudService<MhOrderHeadDao, MhOrderHead> {

	public MhOrderHead get(String id) {
		return super.get(id);
	}
	
	public List<MhOrderHead> findList(MhOrderHead mhOrderHead) {
		return super.findList(mhOrderHead);
	}
	
	public Page<MhOrderHead> findPage(Page<MhOrderHead> page, MhOrderHead mhOrderHead) {
		return super.findPage(page, mhOrderHead);
	}
	
	@Transactional(readOnly = false)
	public void save(MhOrderHead mhOrderHead) {
		super.save(mhOrderHead);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhOrderHead mhOrderHead) {
		super.delete(mhOrderHead);
	}
	
	
	
	
}