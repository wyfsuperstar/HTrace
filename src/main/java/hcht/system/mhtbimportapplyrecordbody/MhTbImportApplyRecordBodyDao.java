/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordbody;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 检验检疫报关表体DAO接口
 * @author BMW
 * @version 2018-11-19
 */
@MyBatisDao
public interface MhTbImportApplyRecordBodyDao extends CrudDao<MhTbImportApplyRecordBody> {

	
}