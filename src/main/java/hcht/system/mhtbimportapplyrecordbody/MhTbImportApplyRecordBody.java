/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordbody;


import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 检验检疫报关表体Entity
 * @author BMW
 * @version 2018-11-19
 */
public class MhTbImportApplyRecordBody extends DataEntity<MhTbImportApplyRecordBody> {
	
	private static final long serialVersionUID = 1L;
	private String iiSerialNo;		// 境外进区商品流水号
	private String remark;		// 备注
	private String activeInd;		// 操作状态0正常   1已删除
	private String entCode;		// 企业备案号
	private String goodsStatus;		// 商品状态代码：预留
	private String seqNo;		// 序号
	private String goodsRegNo;		// 商品备案号
	private String hsCode;		// HS编码
	private String entCname;		// 企业中文名
	private String goodsNo;		// 商品货号
	private String goodsName;		// 商品名称
	private String originCountryCode;		// 原产国代码
	private String qty;		// 包装数量
	private String qtyUnitCode;		// 包装单位代码
	private String smallQty;		// 最小包装数量
	private String smallQtyUnitCode;		// 最小包装单位代码
	private String goodsUnitPrice;		// 单价
	private String weight;		// 重量
	private String goodsTotalValues;		// 总价
	private String currUnit;		// 货币单位
	private String headId;		// 表头ID
	private String qrcode;		// 二维码
	
	public MhTbImportApplyRecordBody() {
		super();
	}

	public MhTbImportApplyRecordBody(String id){
		super(id);
	}

	@ExcelField(title="境外进区商品流水号", align=2, sort=1)
	public String getIiSerialNo() {
		return iiSerialNo;
	}

	public void setIiSerialNo(String iiSerialNo) {
		this.iiSerialNo = iiSerialNo;
	}
	
	@ExcelField(title="备注", align=2, sort=2)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="操作状态0正常   1已删除", align=2, sort=3)
	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}
	
	@ExcelField(title="企业备案号", align=2, sort=4)
	public String getEntCode() {
		return entCode;
	}

	public void setEntCode(String entCode) {
		this.entCode = entCode;
	}
	
	@ExcelField(title="商品状态代码：预留", align=2, sort=5)
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	
	@ExcelField(title="序号", align=2, sort=6)
	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	
	@ExcelField(title="商品备案号", align=2, sort=7)
	public String getGoodsRegNo() {
		return goodsRegNo;
	}

	public void setGoodsRegNo(String goodsRegNo) {
		this.goodsRegNo = goodsRegNo;
	}
	
	@ExcelField(title="HS编码", align=2, sort=8)
	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
	
	@ExcelField(title="企业中文名", align=2, sort=9)
	public String getEntCname() {
		return entCname;
	}

	public void setEntCname(String entCname) {
		this.entCname = entCname;
	}
	
	@ExcelField(title="商品货号", align=2, sort=10)
	public String getGoodsNo() {
		return goodsNo;
	}

	public void setGoodsNo(String goodsNo) {
		this.goodsNo = goodsNo;
	}
	
	@ExcelField(title="商品名称", align=2, sort=11)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	@ExcelField(title="原产国代码", align=2, sort=12)
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
	
	@ExcelField(title="包装数量", align=2, sort=13)
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@ExcelField(title="包装单位代码", align=2, sort=14)
	public String getQtyUnitCode() {
		return qtyUnitCode;
	}

	public void setQtyUnitCode(String qtyUnitCode) {
		this.qtyUnitCode = qtyUnitCode;
	}
	
	@ExcelField(title="最小包装数量", align=2, sort=15)
	public String getSmallQty() {
		return smallQty;
	}

	public void setSmallQty(String smallQty) {
		this.smallQty = smallQty;
	}
	
	@ExcelField(title="最小包装单位代码", align=2, sort=16)
	public String getSmallQtyUnitCode() {
		return smallQtyUnitCode;
	}

	public void setSmallQtyUnitCode(String smallQtyUnitCode) {
		this.smallQtyUnitCode = smallQtyUnitCode;
	}
	
	@ExcelField(title="单价", align=2, sort=17)
	public String getGoodsUnitPrice() {
		return goodsUnitPrice;
	}

	public void setGoodsUnitPrice(String goodsUnitPrice) {
		this.goodsUnitPrice = goodsUnitPrice;
	}
	
	@ExcelField(title="重量", align=2, sort=18)
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	@ExcelField(title="总价", align=2, sort=19)
	public String getGoodsTotalValues() {
		return goodsTotalValues;
	}

	public void setGoodsTotalValues(String goodsTotalValues) {
		this.goodsTotalValues = goodsTotalValues;
	}
	
	@ExcelField(title="货币单位", align=2, sort=20)
	public String getCurrUnit() {
		return currUnit;
	}

	public void setCurrUnit(String currUnit) {
		this.currUnit = currUnit;
	}
	
	@ExcelField(title="表头ID", align=2, sort=21)
	public String getHeadId() {
		return headId;
	}

	public void setHeadId(String headId) {
		this.headId = headId;
	}
	
	@ExcelField(title="二维码", align=2, sort=22)
	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
	
}