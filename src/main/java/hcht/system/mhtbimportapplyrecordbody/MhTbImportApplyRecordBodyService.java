/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordbody;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.system.mhtbimportapplyrecordbody.MhTbImportApplyRecordBody;
import hcht.system.mhtbimportapplyrecordbody.MhTbImportApplyRecordBodyDao;

/**
 * 检验检疫报关表体Service
 * @author BMW
 * @version 2018-11-19
 */
@Service
@Transactional(readOnly = true)
public class MhTbImportApplyRecordBodyService extends CrudService<MhTbImportApplyRecordBodyDao, MhTbImportApplyRecordBody> {

	public MhTbImportApplyRecordBody get(String id) {
		return super.get(id);
	}
	
	public List<MhTbImportApplyRecordBody> findList(MhTbImportApplyRecordBody mhTbImportApplyRecordBody) {
		return super.findList(mhTbImportApplyRecordBody);
	}
	
	public Page<MhTbImportApplyRecordBody> findPage(Page<MhTbImportApplyRecordBody> page, MhTbImportApplyRecordBody mhTbImportApplyRecordBody) {
		return super.findPage(page, mhTbImportApplyRecordBody);
	}
	
	@Transactional(readOnly = false)
	public void save(MhTbImportApplyRecordBody mhTbImportApplyRecordBody) {
		super.save(mhTbImportApplyRecordBody);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhTbImportApplyRecordBody mhTbImportApplyRecordBody) {
		super.delete(mhTbImportApplyRecordBody);
	}
	
	
	
	
}