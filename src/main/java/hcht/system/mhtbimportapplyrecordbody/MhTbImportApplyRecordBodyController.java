/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordbody;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import hcht.system.mhtbimportapplyrecordbody.MhTbImportApplyRecordBody;
import hcht.system.mhtbimportapplyrecordbody.MhTbImportApplyRecordBodyService;

/**
 * 检验检疫报关表体Controller
 * @author BMW
 * @version 2018-11-19
 */
@Controller
@RequestMapping(value = "${adminPath}/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody")
public class MhTbImportApplyRecordBodyController extends BaseController {

	@Autowired
	private MhTbImportApplyRecordBodyService mhTbImportApplyRecordBodyService;
	
	@ModelAttribute
	public MhTbImportApplyRecordBody get(@RequestParam(required=false) String id) {
		MhTbImportApplyRecordBody entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhTbImportApplyRecordBodyService.get(id);
		}
		if (entity == null){
			entity = new MhTbImportApplyRecordBody();
		}
		return entity;
	}
	
	/**
	 * 检验检疫报关表体列表页面
	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhTbImportApplyRecordBody mhTbImportApplyRecordBody, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhTbImportApplyRecordBody> page = mhTbImportApplyRecordBodyService.findPage(new Page<MhTbImportApplyRecordBody>(request, response), mhTbImportApplyRecordBody); 
		model.addAttribute("page", page);
		return "system/mhtbimportapplyrecordbody/mhTbImportApplyRecordBodyList";
	}

	/**
	 * 查看，增加，编辑检验检疫报关表体表单页面
	 */
	@RequiresPermissions(value={"mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:view","mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:add","mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhTbImportApplyRecordBody mhTbImportApplyRecordBody, Model model) {
		model.addAttribute("mhTbImportApplyRecordBody", mhTbImportApplyRecordBody);
		return "system/mhtbimportapplyrecordbody/mhTbImportApplyRecordBodyForm";
	}

	/**
	 * 保存检验检疫报关表体
	 */
	@RequiresPermissions(value={"mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:add","mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhTbImportApplyRecordBody mhTbImportApplyRecordBody, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhTbImportApplyRecordBody)){
			return form(mhTbImportApplyRecordBody, model);
		}
		if(!mhTbImportApplyRecordBody.getIsNewRecord()){//编辑表单保存
			MhTbImportApplyRecordBody t = mhTbImportApplyRecordBodyService.get(mhTbImportApplyRecordBody.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhTbImportApplyRecordBody, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhTbImportApplyRecordBodyService.save(t);//保存
		}else{//新增表单保存
			mhTbImportApplyRecordBodyService.save(mhTbImportApplyRecordBody);//保存
		}
		addMessage(redirectAttributes, "保存检验检疫报关表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
	}
	
	/**
	 * 删除检验检疫报关表体
	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:del")
	@RequestMapping(value = "delete")
	public String delete(MhTbImportApplyRecordBody mhTbImportApplyRecordBody, RedirectAttributes redirectAttributes) {
		mhTbImportApplyRecordBodyService.delete(mhTbImportApplyRecordBody);
		addMessage(redirectAttributes, "删除检验检疫报关表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
	}
	
	/**
	 * 批量删除检验检疫报关表体
	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhTbImportApplyRecordBodyService.delete(mhTbImportApplyRecordBodyService.get(id));
		}
		addMessage(redirectAttributes, "删除检验检疫报关表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhTbImportApplyRecordBody mhTbImportApplyRecordBody, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "检验检疫报关表体"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhTbImportApplyRecordBody> page = mhTbImportApplyRecordBodyService.findPage(new Page<MhTbImportApplyRecordBody>(request, response, -1), mhTbImportApplyRecordBody);
    		new ExportExcel("检验检疫报关表体", MhTbImportApplyRecordBody.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出检验检疫报关表体记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhTbImportApplyRecordBody> list = ei.getDataList(MhTbImportApplyRecordBody.class);
			for (MhTbImportApplyRecordBody mhTbImportApplyRecordBody : list){
				try{
					mhTbImportApplyRecordBodyService.save(mhTbImportApplyRecordBody);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条检验检疫报关表体记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条检验检疫报关表体记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入检验检疫报关表体失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
    }
	
	/**
	 * 下载导入检验检疫报关表体数据模板
	 */
	@RequiresPermissions("mhtbimportapplyrecordbody:mhTbImportApplyRecordBody:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "检验检疫报关表体数据导入模板.xlsx";
    		List<MhTbImportApplyRecordBody> list = Lists.newArrayList(); 
    		new ExportExcel("检验检疫报关表体数据", MhTbImportApplyRecordBody.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordbody/mhTbImportApplyRecordBody/?repage";
    }
	
	
	

}