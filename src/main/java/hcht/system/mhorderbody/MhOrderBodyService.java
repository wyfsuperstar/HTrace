/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderbody;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.system.mhorderbody.MhOrderBody;
import hcht.system.mhorderbody.MhOrderBodyDao;

/**
 * 订单表体Service
 * @author BMW
 * @version 2018-11-19
 */
@Service
@Transactional(readOnly = true)
public class MhOrderBodyService extends CrudService<MhOrderBodyDao, MhOrderBody> {

	public MhOrderBody get(String id) {
		return super.get(id);
	}
	
	public List<MhOrderBody> findList(MhOrderBody mhOrderBody) {
		return super.findList(mhOrderBody);
	}
	
	public Page<MhOrderBody> findPage(Page<MhOrderBody> page, MhOrderBody mhOrderBody) {
		return super.findPage(page, mhOrderBody);
	}
	
	@Transactional(readOnly = false)
	public void save(MhOrderBody mhOrderBody) {
		super.save(mhOrderBody);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhOrderBody mhOrderBody) {
		super.delete(mhOrderBody);
	}
	
	
	
	
}