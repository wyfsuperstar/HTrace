/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderbody;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import hcht.system.mhorderbody.MhOrderBody;
import hcht.system.mhorderbody.MhOrderBodyService;

/**
 * 订单表体Controller
 * @author BMW
 * @version 2018-11-19
 */
@Controller
@RequestMapping(value = "${adminPath}/mhorderbody/mhOrderBody")
public class MhOrderBodyController extends BaseController {

	@Autowired
	private MhOrderBodyService mhOrderBodyService;
	
	@ModelAttribute
	public MhOrderBody get(@RequestParam(required=false) String id) {
		MhOrderBody entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhOrderBodyService.get(id);
		}
		if (entity == null){
			entity = new MhOrderBody();
		}
		return entity;
	}
	
	/**
	 * 订单表体列表页面
	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhOrderBody mhOrderBody, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhOrderBody> page = mhOrderBodyService.findPage(new Page<MhOrderBody>(request, response), mhOrderBody); 
		model.addAttribute("page", page);
		return "system/mhorderbody/mhOrderBodyList";
	}

	/**
	 * 查看，增加，编辑订单表体表单页面
	 */
	@RequiresPermissions(value={"mhorderbody:mhOrderBody:view","mhorderbody:mhOrderBody:add","mhorderbody:mhOrderBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhOrderBody mhOrderBody, Model model) {
		model.addAttribute("mhOrderBody", mhOrderBody);
		return "system/mhorderbody/mhOrderBodyForm";
	}

	/**
	 * 保存订单表体
	 */
	@RequiresPermissions(value={"mhorderbody:mhOrderBody:add","mhorderbody:mhOrderBody:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhOrderBody mhOrderBody, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhOrderBody)){
			return form(mhOrderBody, model);
		}
		if(!mhOrderBody.getIsNewRecord()){//编辑表单保存
			MhOrderBody t = mhOrderBodyService.get(mhOrderBody.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhOrderBody, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhOrderBodyService.save(t);//保存
		}else{//新增表单保存
			mhOrderBodyService.save(mhOrderBody);//保存
		}
		addMessage(redirectAttributes, "保存订单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
	}
	
	/**
	 * 删除订单表体
	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:del")
	@RequestMapping(value = "delete")
	public String delete(MhOrderBody mhOrderBody, RedirectAttributes redirectAttributes) {
		mhOrderBodyService.delete(mhOrderBody);
		addMessage(redirectAttributes, "删除订单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
	}
	
	/**
	 * 批量删除订单表体
	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhOrderBodyService.delete(mhOrderBodyService.get(id));
		}
		addMessage(redirectAttributes, "删除订单表体成功");
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhOrderBody mhOrderBody, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单表体"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhOrderBody> page = mhOrderBodyService.findPage(new Page<MhOrderBody>(request, response, -1), mhOrderBody);
    		new ExportExcel("订单表体", MhOrderBody.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出订单表体记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhOrderBody> list = ei.getDataList(MhOrderBody.class);
			for (MhOrderBody mhOrderBody : list){
				try{
					mhOrderBodyService.save(mhOrderBody);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条订单表体记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条订单表体记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入订单表体失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
    }
	
	/**
	 * 下载导入订单表体数据模板
	 */
	@RequiresPermissions("mhorderbody:mhOrderBody:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单表体数据导入模板.xlsx";
    		List<MhOrderBody> list = Lists.newArrayList(); 
    		new ExportExcel("订单表体数据", MhOrderBody.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhorderbody/mhOrderBody/?repage";
    }
	
	
	

}