/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderbody;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 订单表体DAO接口
 * @author BMW
 * @version 2018-11-19
 */
@MyBatisDao
public interface MhOrderBodyDao extends CrudDao<MhOrderBody> {

	
}