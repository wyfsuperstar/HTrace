/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhorderbody;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 订单表体Entity
 * @author BMW
 * @version 2018-11-19
 */
public class MhOrderBody extends DataEntity<MhOrderBody> {
	
	private static final long serialVersionUID = 1L;
	private Long seqNo;		// 序号           
	private String goodsNo;		// 企业商品货号   
	private String goodsBarcode;		// 商品条码       
	private String goodsName;		// 商品名称       
	private String shelfGoodsName;		// 品牌           
	private Double price;		// 单价           
	private Double priceTotal;		// 总价           
	private String currency;		// 币制编号       
	private String countryCode;		// 原产国/产地编号
	private Double countNum;		// 成交数量       
	private String unitCode;		// 计量单位编号   
	private String wraptypeCode;		// 包装种类编号   
	private String purposeCode;		// 用途编号       
	private String goodsModel;		// 规格型号       
	private String remark;		// 备注           
	private String headid;		// 表头id
	private String goodsRegNo;		// 商品备案号(直邮专有)
	private String type;		// type
	private Date storageTime;		// 报文入库时间
	private String orderGoodId;		// order_good_id
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhOrderBody() {
		super();
	}

	public MhOrderBody(String id){
		super(id);
	}

	@ExcelField(title="序号           ", align=2, sort=0)
	public Long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	
	@ExcelField(title="企业商品货号   ", align=2, sort=1)
	public String getGoodsNo() {
		return goodsNo;
	}

	public void setGoodsNo(String goodsNo) {
		this.goodsNo = goodsNo;
	}
	
	@ExcelField(title="商品条码       ", align=2, sort=2)
	public String getGoodsBarcode() {
		return goodsBarcode;
	}

	public void setGoodsBarcode(String goodsBarcode) {
		this.goodsBarcode = goodsBarcode;
	}
	
	@ExcelField(title="商品名称       ", align=2, sort=3)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	@ExcelField(title="品牌           ", align=2, sort=4)
	public String getShelfGoodsName() {
		return shelfGoodsName;
	}

	public void setShelfGoodsName(String shelfGoodsName) {
		this.shelfGoodsName = shelfGoodsName;
	}
	
	@ExcelField(title="单价           ", align=2, sort=5)
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@ExcelField(title="总价           ", align=2, sort=6)
	public Double getPriceTotal() {
		return priceTotal;
	}

	public void setPriceTotal(Double priceTotal) {
		this.priceTotal = priceTotal;
	}
	
	@ExcelField(title="币制编号       ", align=2, sort=7)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@ExcelField(title="原产国/产地编号", align=2, sort=8)
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	@ExcelField(title="成交数量       ", align=2, sort=9)
	public Double getCountNum() {
		return countNum;
	}

	public void setCountNum(Double countNum) {
		this.countNum = countNum;
	}
	
	@ExcelField(title="计量单位编号   ", align=2, sort=10)
	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	
	@ExcelField(title="包装种类编号   ", align=2, sort=11)
	public String getWraptypeCode() {
		return wraptypeCode;
	}

	public void setWraptypeCode(String wraptypeCode) {
		this.wraptypeCode = wraptypeCode;
	}
	
	@ExcelField(title="用途编号       ", align=2, sort=12)
	public String getPurposeCode() {
		return purposeCode;
	}

	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}
	
	@ExcelField(title="规格型号       ", align=2, sort=13)
	public String getGoodsModel() {
		return goodsModel;
	}

	public void setGoodsModel(String goodsModel) {
		this.goodsModel = goodsModel;
	}
	
	@ExcelField(title="备注           ", align=2, sort=14)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="表头id", align=2, sort=15)
	public String getHeadid() {
		return headid;
	}

	public void setHeadid(String headid) {
		this.headid = headid;
	}
	
	@ExcelField(title="商品备案号(直邮专有)", align=2, sort=17)
	public String getGoodsRegNo() {
		return goodsRegNo;
	}

	public void setGoodsRegNo(String goodsRegNo) {
		this.goodsRegNo = goodsRegNo;
	}
	
	@ExcelField(title="type", align=2, sort=18)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=19)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	@ExcelField(title="order_good_id", align=2, sort=20)
	public String getOrderGoodId() {
		return orderGoodId;
	}

	public void setOrderGoodId(String orderGoodId) {
		this.orderGoodId = orderGoodId;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}