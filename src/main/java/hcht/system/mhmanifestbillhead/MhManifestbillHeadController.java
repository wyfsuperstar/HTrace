/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillhead;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 清单表头Controller
 * @author BMW
 * @version 2018-11-16
 */
@Controller
@RequestMapping(value = "${adminPath}/mhmanifestbillhead/mhManifestbillHead")
public class MhManifestbillHeadController extends BaseController {

	@Autowired
	private MhManifestbillHeadService mhManifestbillHeadService;
	
	@ModelAttribute
	public MhManifestbillHead get(@RequestParam(required=false) String id) {
		MhManifestbillHead entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhManifestbillHeadService.get(id);
		}
		if (entity == null){
			entity = new MhManifestbillHead();
		}
		return entity;
	}
	
	/**
	 * 清单表头列表页面
	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhManifestbillHead mhManifestbillHead, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhManifestbillHead> page = mhManifestbillHeadService.findPage(new Page<MhManifestbillHead>(request, response), mhManifestbillHead); 
		model.addAttribute("page", page);
		return "system/mhmanifestbillhead/mhManifestbillHeadList";
	}

	/**
	 * 查看，增加，编辑清单表头表单页面
	 */
	@RequiresPermissions(value={"mhmanifestbillhead:mhManifestbillHead:view","mhmanifestbillhead:mhManifestbillHead:add","mhmanifestbillhead:mhManifestbillHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhManifestbillHead mhManifestbillHead, Model model) {
		model.addAttribute("mhManifestbillHead", mhManifestbillHead);
		return "system/mhmanifestbillhead/mhManifestbillHeadForm";
	}

	/**
	 * 保存清单表头
	 */
	@RequiresPermissions(value={"mhmanifestbillhead:mhManifestbillHead:add","mhmanifestbillhead:mhManifestbillHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhManifestbillHead mhManifestbillHead, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhManifestbillHead)){
			return form(mhManifestbillHead, model);
		}
		if(!mhManifestbillHead.getIsNewRecord()){//编辑表单保存
			MhManifestbillHead t = mhManifestbillHeadService.get(mhManifestbillHead.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhManifestbillHead, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhManifestbillHeadService.save(t);//保存
		}else{//新增表单保存
			mhManifestbillHeadService.save(mhManifestbillHead);//保存
		}
		addMessage(redirectAttributes, "保存清单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
	}
	
	/**
	 * 删除清单表头
	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:del")
	@RequestMapping(value = "delete")
	public String delete(MhManifestbillHead mhManifestbillHead, RedirectAttributes redirectAttributes) {
		mhManifestbillHeadService.delete(mhManifestbillHead);
		addMessage(redirectAttributes, "删除清单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
	}
	
	/**
	 * 批量删除清单表头
	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhManifestbillHeadService.delete(mhManifestbillHeadService.get(id));
		}
		addMessage(redirectAttributes, "删除清单表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhManifestbillHead mhManifestbillHead, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "清单表头"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhManifestbillHead> page = mhManifestbillHeadService.findPage(new Page<MhManifestbillHead>(request, response, -1), mhManifestbillHead);
    		new ExportExcel("清单表头", MhManifestbillHead.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出清单表头记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhManifestbillHead> list = ei.getDataList(MhManifestbillHead.class);
			for (MhManifestbillHead mhManifestbillHead : list){
				try{
					mhManifestbillHeadService.save(mhManifestbillHead);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条清单表头记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条清单表头记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入清单表头失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
    }
	
	/**
	 * 下载导入清单表头数据模板
	 */
	@RequiresPermissions("mhmanifestbillhead:mhManifestbillHead:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "清单表头数据导入模板.xlsx";
    		List<MhManifestbillHead> list = Lists.newArrayList(); 
    		new ExportExcel("清单表头数据", MhManifestbillHead.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhmanifestbillhead/mhManifestbillHead/?repage";
    }
	
	
	

}