/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillhead;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 清单表头Entity
 * @author BMW
 * @version 2018-11-16
 */
public class MhManifestbillHead extends DataEntity<MhManifestbillHead> {
	
	private static final long serialVersionUID = 1L;
	private String ioSerialNo;		// 出区进口流水号
	private String declNo;		// 申报号
	private String declPerson;		// 申报人名称
	private Date declDate;		// 申报时间
	private String declTel;		// 申报人联系方式
	private String mainDeclNo;		// 汇总申报单号
	private String combineFlag;		// 并单标志
	private Long orderNum;		// 订单总数
	private Long declCount;		// 出区申报单数量
	private Long orderCount;		// 电商订单数量 
	private Long billCount;		// 运单数量 
	private Long paymentCount;		// 支付单数量
	private String logisticsno;		// 运单号
	private String orderNo;		// 订单号
	private String paymentNo;		// 支付交易号
	private String declTypeCode;		// 申报类型代码：FI-境外进区、SI-出区进口
	private String entCode;		// 申报单位代码
	private String cbecode;		// 电商企业代码
	private String logisticsCode;		// 物流仓储企业编号
	private String checkOrgCode;		// 施检机构代码
	private String idType;		// 发货人证件类型编号(默认身份证)
	private Double totalValues;		// 总货值
	private String stockFlag;		// 集货/备货标识
	private String consigneeNo;		// 收货人编号
	private String consigneeCname;		// 收货人
	private String consigneeEname;		// 收货人英文名称
	private String consigneeTel;		// 收货人电话
	private String consigneeAddress;		// 收货人地址
	private String consginorNo;		// 发货人编号
	private String consignorCname;		// 发货人
	private String consginorEname;		// 发货人英文名称
	private String consignorTel;		// 发货人电话
	private String consignorAddress;		// 发货人地址
	private String idCard;		// 发货人证件号码
	private String remark;		// 备注
	private String billsMesCode;		// 单据回执状态 状态代码:1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过 
	private String quarantineMesCode;		// 检疫回执状态 状态代码，1待检疫 2合格 3不合格
	private String proveMesCode;		// 查验回执状态 状态代码，0未查验 1待查验 2合格 3不合格
	private String goodsStatus;		// 商品状态回执
	private String declarestatus;		// 推送状态（未推送：1  已推送：2 ）
	private String flag;		// 是否生成撤单申请  1 已生成
	private String status;		// 是否是单独报检    1为单独报检
	private String cbename;		// 电商企业名称
	private String entName;		// 申报企业名称
	private Date storageTime;		// 报文入库时间
	private String mark;		// 默认值为0，读取后改为1
	private String ioSerialNoBk;		// 原始流水号
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhManifestbillHead() {
		super();
	}

	public MhManifestbillHead(String id){
		super(id);
	}

	@ExcelField(title="出区进口流水号", align=2, sort=0)
	public String getIoSerialNo() {
		return ioSerialNo;
	}

	public void setIoSerialNo(String ioSerialNo) {
		this.ioSerialNo = ioSerialNo;
	}
	
	@ExcelField(title="申报号", align=2, sort=1)
	public String getDeclNo() {
		return declNo;
	}

	public void setDeclNo(String declNo) {
		this.declNo = declNo;
	}
	
	@ExcelField(title="申报人名称", align=2, sort=2)
	public String getDeclPerson() {
		return declPerson;
	}

	public void setDeclPerson(String declPerson) {
		this.declPerson = declPerson;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="申报时间", align=2, sort=3)
	public Date getDeclDate() {
		return declDate;
	}

	public void setDeclDate(Date declDate) {
		this.declDate = declDate;
	}
	
	@ExcelField(title="申报人联系方式", align=2, sort=4)
	public String getDeclTel() {
		return declTel;
	}

	public void setDeclTel(String declTel) {
		this.declTel = declTel;
	}
	
	@ExcelField(title="汇总申报单号", align=2, sort=5)
	public String getMainDeclNo() {
		return mainDeclNo;
	}

	public void setMainDeclNo(String mainDeclNo) {
		this.mainDeclNo = mainDeclNo;
	}
	
	@ExcelField(title="并单标志", align=2, sort=6)
	public String getCombineFlag() {
		return combineFlag;
	}

	public void setCombineFlag(String combineFlag) {
		this.combineFlag = combineFlag;
	}
	
	@ExcelField(title="订单总数", align=2, sort=7)
	public Long getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}
	
	@ExcelField(title="出区申报单数量", align=2, sort=8)
	public Long getDeclCount() {
		return declCount;
	}

	public void setDeclCount(Long declCount) {
		this.declCount = declCount;
	}
	
	@ExcelField(title="电商订单数量 ", align=2, sort=9)
	public Long getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Long orderCount) {
		this.orderCount = orderCount;
	}
	
	@ExcelField(title="运单数量 ", align=2, sort=10)
	public Long getBillCount() {
		return billCount;
	}

	public void setBillCount(Long billCount) {
		this.billCount = billCount;
	}
	
	@ExcelField(title="支付单数量", align=2, sort=11)
	public Long getPaymentCount() {
		return paymentCount;
	}

	public void setPaymentCount(Long paymentCount) {
		this.paymentCount = paymentCount;
	}
	
	@ExcelField(title="运单号", align=2, sort=12)
	public String getLogisticsno() {
		return logisticsno;
	}

	public void setLogisticsno(String logisticsno) {
		this.logisticsno = logisticsno;
	}
	
	@ExcelField(title="订单号", align=2, sort=13)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="支付交易号", align=2, sort=14)
	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	
	@ExcelField(title="申报类型代码：FI-境外进区、SI-出区进口", align=2, sort=15)
	public String getDeclTypeCode() {
		return declTypeCode;
	}

	public void setDeclTypeCode(String declTypeCode) {
		this.declTypeCode = declTypeCode;
	}
	
	@ExcelField(title="申报单位代码", align=2, sort=16)
	public String getEntCode() {
		return entCode;
	}

	public void setEntCode(String entCode) {
		this.entCode = entCode;
	}
	
	@ExcelField(title="电商企业代码", align=2, sort=17)
	public String getCbecode() {
		return cbecode;
	}

	public void setCbecode(String cbecode) {
		this.cbecode = cbecode;
	}
	
	@ExcelField(title="物流仓储企业编号", align=2, sort=18)
	public String getLogisticsCode() {
		return logisticsCode;
	}

	public void setLogisticsCode(String logisticsCode) {
		this.logisticsCode = logisticsCode;
	}
	
	@ExcelField(title="施检机构代码", align=2, sort=19)
	public String getCheckOrgCode() {
		return checkOrgCode;
	}

	public void setCheckOrgCode(String checkOrgCode) {
		this.checkOrgCode = checkOrgCode;
	}
	
	@ExcelField(title="发货人证件类型编号(默认身份证)", align=2, sort=20)
	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}
	
	@ExcelField(title="总货值", align=2, sort=21)
	public Double getTotalValues() {
		return totalValues;
	}

	public void setTotalValues(Double totalValues) {
		this.totalValues = totalValues;
	}
	
	@ExcelField(title="集货/备货标识", align=2, sort=22)
	public String getStockFlag() {
		return stockFlag;
	}

	public void setStockFlag(String stockFlag) {
		this.stockFlag = stockFlag;
	}
	
	@ExcelField(title="收货人编号", align=2, sort=23)
	public String getConsigneeNo() {
		return consigneeNo;
	}

	public void setConsigneeNo(String consigneeNo) {
		this.consigneeNo = consigneeNo;
	}
	
	@ExcelField(title="收货人", align=2, sort=24)
	public String getConsigneeCname() {
		return consigneeCname;
	}

	public void setConsigneeCname(String consigneeCname) {
		this.consigneeCname = consigneeCname;
	}
	
	@ExcelField(title="收货人英文名称", align=2, sort=25)
	public String getConsigneeEname() {
		return consigneeEname;
	}

	public void setConsigneeEname(String consigneeEname) {
		this.consigneeEname = consigneeEname;
	}
	
	@ExcelField(title="收货人电话", align=2, sort=26)
	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}
	
	@ExcelField(title="收货人地址", align=2, sort=27)
	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	
	@ExcelField(title="发货人编号", align=2, sort=28)
	public String getConsginorNo() {
		return consginorNo;
	}

	public void setConsginorNo(String consginorNo) {
		this.consginorNo = consginorNo;
	}
	
	@ExcelField(title="发货人", align=2, sort=29)
	public String getConsignorCname() {
		return consignorCname;
	}

	public void setConsignorCname(String consignorCname) {
		this.consignorCname = consignorCname;
	}
	
	@ExcelField(title="发货人英文名称", align=2, sort=30)
	public String getConsginorEname() {
		return consginorEname;
	}

	public void setConsginorEname(String consginorEname) {
		this.consginorEname = consginorEname;
	}
	
	@ExcelField(title="发货人电话", align=2, sort=31)
	public String getConsignorTel() {
		return consignorTel;
	}

	public void setConsignorTel(String consignorTel) {
		this.consignorTel = consignorTel;
	}
	
	@ExcelField(title="发货人地址", align=2, sort=32)
	public String getConsignorAddress() {
		return consignorAddress;
	}

	public void setConsignorAddress(String consignorAddress) {
		this.consignorAddress = consignorAddress;
	}
	
	@ExcelField(title="发货人证件号码", align=2, sort=33)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	@ExcelField(title="备注", align=2, sort=34)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="单据回执状态 状态代码:1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过 ", align=2, sort=35)
	public String getBillsMesCode() {
		return billsMesCode;
	}

	public void setBillsMesCode(String billsMesCode) {
		this.billsMesCode = billsMesCode;
	}
	
	@ExcelField(title="检疫回执状态 状态代码，1待检疫 2合格 3不合格", align=2, sort=36)
	public String getQuarantineMesCode() {
		return quarantineMesCode;
	}

	public void setQuarantineMesCode(String quarantineMesCode) {
		this.quarantineMesCode = quarantineMesCode;
	}
	
	@ExcelField(title="查验回执状态 状态代码，0未查验 1待查验 2合格 3不合格", align=2, sort=37)
	public String getProveMesCode() {
		return proveMesCode;
	}

	public void setProveMesCode(String proveMesCode) {
		this.proveMesCode = proveMesCode;
	}
	
	@ExcelField(title="商品状态回执", align=2, sort=39)
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	
	@ExcelField(title="推送状态（未推送：1  已推送：2 ）", align=2, sort=40)
	public String getDeclarestatus() {
		return declarestatus;
	}

	public void setDeclarestatus(String declarestatus) {
		this.declarestatus = declarestatus;
	}
	
	@ExcelField(title="是否生成撤单申请  1 已生成", align=2, sort=41)
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	@ExcelField(title="是否是单独报检    1为单独报检", align=2, sort=42)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="电商企业名称", align=2, sort=43)
	public String getCbename() {
		return cbename;
	}

	public void setCbename(String cbename) {
		this.cbename = cbename;
	}
	
	@ExcelField(title="申报企业名称", align=2, sort=44)
	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=45)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	@ExcelField(title="默认值为0，读取后改为1", align=2, sort=46)
	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}
	
	@ExcelField(title="原始流水号", align=2, sort=47)
	public String getIoSerialNoBk() {
		return ioSerialNoBk;
	}

	public void setIoSerialNoBk(String ioSerialNoBk) {
		this.ioSerialNoBk = ioSerialNoBk;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}