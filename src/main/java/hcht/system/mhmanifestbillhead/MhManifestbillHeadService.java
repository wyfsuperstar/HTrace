/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhmanifestbillhead;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 清单表头Service
 * @author BMW
 * @version 2018-11-16
 */
@Service
@Transactional(readOnly = true)
public class MhManifestbillHeadService extends CrudService<MhManifestbillHeadDao, MhManifestbillHead> {

	public MhManifestbillHead get(String id) {
		return super.get(id);
	}
	
	public List<MhManifestbillHead> findList(MhManifestbillHead mhManifestbillHead) {
		return super.findList(mhManifestbillHead);
	}
	
	public Page<MhManifestbillHead> findPage(Page<MhManifestbillHead> page, MhManifestbillHead mhManifestbillHead) {
		return super.findPage(page, mhManifestbillHead);
	}
	
	@Transactional(readOnly = false)
	public void save(MhManifestbillHead mhManifestbillHead) {
		super.save(mhManifestbillHead);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhManifestbillHead mhManifestbillHead) {
		super.delete(mhManifestbillHead);
	}
	
	
	
	
}