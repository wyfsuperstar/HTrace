/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.htdatareadlog;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.htdatareadlog.HtDataReadLog;

/**
 * 读入日志DAO接口
 * @author BMW
 * @version 2018-11-21
 */
@MyBatisDao
public interface HtDataReadLogDao extends CrudDao<HtDataReadLog> {

	
}