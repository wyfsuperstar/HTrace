/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.htdatareadlog;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 读入日志Service
 * @author BMW
 * @version 2018-11-21
 */
@Service
@Transactional(readOnly = true)
public class HtDataReadLogService extends CrudService<HtDataReadLogDao, HtDataReadLog> {

	public HtDataReadLog get(String id) {
		return super.get(id);
	}
	
	public List<HtDataReadLog> findList(HtDataReadLog htDataReadLog) {
		return super.findList(htDataReadLog);
	}
	
	public Page<HtDataReadLog> findPage(Page<HtDataReadLog> page, HtDataReadLog htDataReadLog) {
		return super.findPage(page, htDataReadLog);
	}
	
	@Transactional(readOnly = false)
	public void save(HtDataReadLog htDataReadLog) {
		super.save(htDataReadLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(HtDataReadLog htDataReadLog) {
		super.delete(htDataReadLog);
	}
	
	
	
	
}