/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.htdatareadlog;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 读入日志Controller
 * @author BMW
 * @version 2018-11-21
 */
@Controller
@RequestMapping(value = "${adminPath}/htdatareadlog/htDataReadLog")
public class HtDataReadLogController extends BaseController {

	@Autowired
	private HtDataReadLogService htDataReadLogService;
	
	@ModelAttribute
	public HtDataReadLog get(@RequestParam(required=false) String id) {
		HtDataReadLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = htDataReadLogService.get(id);
		}
		if (entity == null){
			entity = new HtDataReadLog();
		}
		return entity;
	}
	
	/**
	 * 读入日志列表页面
	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:list")
	@RequestMapping(value = {"list", ""})
	public String list(HtDataReadLog htDataReadLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HtDataReadLog> page = htDataReadLogService.findPage(new Page<HtDataReadLog>(request, response), htDataReadLog); 
		model.addAttribute("page", page);
		return "system/htdatareadlog/htDataReadLogList";
	}

	/**
	 * 查看，增加，编辑读入日志表单页面
	 */
	@RequiresPermissions(value={"htdatareadlog:htDataReadLog:view","htdatareadlog:htDataReadLog:add","htdatareadlog:htDataReadLog:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(HtDataReadLog htDataReadLog, Model model) {
		model.addAttribute("htDataReadLog", htDataReadLog);
		return "system/htdatareadlog/htDataReadLogForm";
	}

	/**
	 * 保存读入日志
	 */
	@RequiresPermissions(value={"htdatareadlog:htDataReadLog:add","htdatareadlog:htDataReadLog:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(HtDataReadLog htDataReadLog, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, htDataReadLog)){
			return form(htDataReadLog, model);
		}
		if(!htDataReadLog.getIsNewRecord()){//编辑表单保存
			HtDataReadLog t = htDataReadLogService.get(htDataReadLog.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(htDataReadLog, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			htDataReadLogService.save(t);//保存
		}else{//新增表单保存
			htDataReadLogService.save(htDataReadLog);//保存
		}
		addMessage(redirectAttributes, "保存读入日志成功");
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
	}
	
	/**
	 * 删除读入日志
	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:del")
	@RequestMapping(value = "delete")
	public String delete(HtDataReadLog htDataReadLog, RedirectAttributes redirectAttributes) {
		htDataReadLogService.delete(htDataReadLog);
		addMessage(redirectAttributes, "删除读入日志成功");
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
	}
	
	/**
	 * 批量删除读入日志
	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			htDataReadLogService.delete(htDataReadLogService.get(id));
		}
		addMessage(redirectAttributes, "删除读入日志成功");
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(HtDataReadLog htDataReadLog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "读入日志"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<HtDataReadLog> page = htDataReadLogService.findPage(new Page<HtDataReadLog>(request, response, -1), htDataReadLog);
    		new ExportExcel("读入日志", HtDataReadLog.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出读入日志记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<HtDataReadLog> list = ei.getDataList(HtDataReadLog.class);
			for (HtDataReadLog htDataReadLog : list){
				try{
					htDataReadLogService.save(htDataReadLog);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条读入日志记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条读入日志记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入读入日志失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
    }
	
	/**
	 * 下载导入读入日志数据模板
	 */
	@RequiresPermissions("htdatareadlog:htDataReadLog:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "读入日志数据导入模板.xlsx";
    		List<HtDataReadLog> list = Lists.newArrayList(); 
    		new ExportExcel("读入日志数据", HtDataReadLog.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/htdatareadlog/htDataReadLog/?repage";
    }
	
	
	

}