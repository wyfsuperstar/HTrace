/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.htdatareadlog;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 读入日志Entity
 * @author BMW
 * @version 2018-11-21
 */
public class HtDataReadLog extends DataEntity<HtDataReadLog> {
	
	private static final long serialVersionUID = 1L;
	private Date readTime;		// 执行时间
	private String msgNo;		// 消息编号(00开始,99结束)
	private String msgType;		// 消息类型(I消息,W-警告,E错误)
	private String declNo;		// 报检号
	private String msgInfo;		// 消息内容
	private Date beginReadTime;		// 开始 执行时间
	private Date endReadTime;		// 结束 执行时间
	
	public HtDataReadLog() {
		super();
	}

	public HtDataReadLog(String id){
		super(id);
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="执行时间", align=2, sort=1)
	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}
	
	@ExcelField(title="消息编号(00开始,99结束)", align=2, sort=2)
	public String getMsgNo() {
		return msgNo;
	}

	public void setMsgNo(String msgNo) {
		this.msgNo = msgNo;
	}
	
	@ExcelField(title="消息类型(I消息,W-警告,E错误)", align=2, sort=3)
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	@ExcelField(title="报检号", align=2, sort=4)
	public String getDeclNo() {
		return declNo;
	}

	public void setDeclNo(String declNo) {
		this.declNo = declNo;
	}
	
	@ExcelField(title="消息内容", align=2, sort=5)
	public String getMsgInfo() {
		return msgInfo;
	}

	public void setMsgInfo(String msgInfo) {
		this.msgInfo = msgInfo;
	}
	
	public Date getBeginReadTime() {
		return beginReadTime;
	}

	public void setBeginReadTime(Date beginReadTime) {
		this.beginReadTime = beginReadTime;
	}
	
	public Date getEndReadTime() {
		return endReadTime;
	}

	public void setEndReadTime(Date endReadTime) {
		this.endReadTime = endReadTime;
	}
		
}