/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordhead;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 检验检疫报关表头Controller
 * @author BMW
 * @version 2018-11-19
 */
@Controller
@RequestMapping(value = "${adminPath}/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead")
public class MhTbImportApplyRecordHeadController extends BaseController {

	@Autowired
	private MhTbImportApplyRecordHeadService mhTbImportApplyRecordHeadService;
	
	@ModelAttribute
	public MhTbImportApplyRecordHead get(@RequestParam(required=false) String id) {
		MhTbImportApplyRecordHead entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhTbImportApplyRecordHeadService.get(id);
		}
		if (entity == null){
			entity = new MhTbImportApplyRecordHead();
		}
		return entity;
	}
	
	/**
	 * 检验检疫报关表头列表页面
	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhTbImportApplyRecordHead mhTbImportApplyRecordHead, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhTbImportApplyRecordHead> page = mhTbImportApplyRecordHeadService.findPage(new Page<MhTbImportApplyRecordHead>(request, response), mhTbImportApplyRecordHead); 
		model.addAttribute("page", page);
		return "system/mhtbimportapplyrecordhead/mhTbImportApplyRecordHeadList";
	}

	/**
	 * 查看，增加，编辑检验检疫报关表头表单页面
	 */
	@RequiresPermissions(value={"mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:view","mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:add","mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhTbImportApplyRecordHead mhTbImportApplyRecordHead, Model model) {
		model.addAttribute("mhTbImportApplyRecordHead", mhTbImportApplyRecordHead);
		return "system/mhtbimportapplyrecordhead/mhTbImportApplyRecordHeadForm";
	}

	/**
	 * 保存检验检疫报关表头
	 */
	@RequiresPermissions(value={"mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:add","mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhTbImportApplyRecordHead mhTbImportApplyRecordHead, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhTbImportApplyRecordHead)){
			return form(mhTbImportApplyRecordHead, model);
		}
		if(!mhTbImportApplyRecordHead.getIsNewRecord()){//编辑表单保存
			MhTbImportApplyRecordHead t = mhTbImportApplyRecordHeadService.get(mhTbImportApplyRecordHead.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhTbImportApplyRecordHead, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhTbImportApplyRecordHeadService.save(t);//保存
		}else{//新增表单保存
			mhTbImportApplyRecordHeadService.save(mhTbImportApplyRecordHead);//保存
		}
		addMessage(redirectAttributes, "保存检验检疫报关表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
	}
	
	/**
	 * 删除检验检疫报关表头
	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:del")
	@RequestMapping(value = "delete")
	public String delete(MhTbImportApplyRecordHead mhTbImportApplyRecordHead, RedirectAttributes redirectAttributes) {
		mhTbImportApplyRecordHeadService.delete(mhTbImportApplyRecordHead);
		addMessage(redirectAttributes, "删除检验检疫报关表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
	}
	
	/**
	 * 批量删除检验检疫报关表头
	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhTbImportApplyRecordHeadService.delete(mhTbImportApplyRecordHeadService.get(id));
		}
		addMessage(redirectAttributes, "删除检验检疫报关表头成功");
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhTbImportApplyRecordHead mhTbImportApplyRecordHead, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "检验检疫报关表头"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhTbImportApplyRecordHead> page = mhTbImportApplyRecordHeadService.findPage(new Page<MhTbImportApplyRecordHead>(request, response, -1), mhTbImportApplyRecordHead);
    		new ExportExcel("检验检疫报关表头", MhTbImportApplyRecordHead.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出检验检疫报关表头记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhTbImportApplyRecordHead> list = ei.getDataList(MhTbImportApplyRecordHead.class);
			for (MhTbImportApplyRecordHead mhTbImportApplyRecordHead : list){
				try{
					mhTbImportApplyRecordHeadService.save(mhTbImportApplyRecordHead);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条检验检疫报关表头记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条检验检疫报关表头记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入检验检疫报关表头失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
    }
	
	/**
	 * 下载导入检验检疫报关表头数据模板
	 */
	@RequiresPermissions("mhtbimportapplyrecordhead:mhTbImportApplyRecordHead:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "检验检疫报关表头数据导入模板.xlsx";
    		List<MhTbImportApplyRecordHead> list = Lists.newArrayList(); 
    		new ExportExcel("检验检疫报关表头数据", MhTbImportApplyRecordHead.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/?repage";
    }
	
	
	

}