/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordhead;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 检验检疫报关表头Service
 * @author BMW
 * @version 2018-11-19
 */
@Service
@Transactional(readOnly = true)
public class MhTbImportApplyRecordHeadService extends CrudService<MhTbImportApplyRecordHeadDao, MhTbImportApplyRecordHead> {

	public MhTbImportApplyRecordHead get(String id) {
		return super.get(id);
	}
	
	public List<MhTbImportApplyRecordHead> findList(MhTbImportApplyRecordHead mhTbImportApplyRecordHead) {
		return super.findList(mhTbImportApplyRecordHead);
	}
	
	public Page<MhTbImportApplyRecordHead> findPage(Page<MhTbImportApplyRecordHead> page, MhTbImportApplyRecordHead mhTbImportApplyRecordHead) {
		return super.findPage(page, mhTbImportApplyRecordHead);
	}
	
	@Transactional(readOnly = false)
	public void save(MhTbImportApplyRecordHead mhTbImportApplyRecordHead) {
		super.save(mhTbImportApplyRecordHead);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhTbImportApplyRecordHead mhTbImportApplyRecordHead) {
		super.delete(mhTbImportApplyRecordHead);
	}
	
	
	
	
}