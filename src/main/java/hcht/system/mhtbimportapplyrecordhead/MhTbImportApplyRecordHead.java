/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhtbimportapplyrecordhead;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 检验检疫报关表头Entity
 * @author BMW
 * @version 2018-11-19
 */
public class MhTbImportApplyRecordHead extends DataEntity<MhTbImportApplyRecordHead> {
	
	private static final long serialVersionUID = 1L;
	private String iiSerialNo;		// 境外进区流水号
	private String declNo;		// 申报号
	private String remark;		// 备注
	private String declPerson;		// 单申报人名称
	private String status;		// 申请单状态代码：1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过
	private String orgNo;		// 组织机构代码
	private String activeInd;		// 操作状态0正常   1已删除
	private Date declDate;		// 申报时间
	private String sampleNo;		// 抽样单号
	private String declTypeCode;		// 申报类型代码
	private String tradeModeCode;		// 贸易方式
	private String entCode;		// 申报单位代码
	private String cbecode;		// 电商企业代码
	private String checkOrgCode;		// 施检机构代码
	private String orgCode;		// 目的机构代码
	private String consigneeIdtype;		// 收货人证件类型
	private String consignorIdtype;		// 发货人证件类型
	private String transTypeCode;		// 运输工具代码
	private String tradeCountryCode;		// 贸易国别/地区代码
	private String despCountryCode;		// 启运国代码
	private String despPortCode;		// 启运口岸代码
	private String entryPortCode;		// 入境口岸代码
	private String stockFlag;		// 集货/备货标识 C备货 B集货
	private String consigneeNo;		// 收货人编号
	private String consigneeCname;		// 收货人
	private String consigneeEname;		// 收货人英文名称
	private String consigneeAddress;		// 收货人地址
	private String consigneeTel;		// 收货人电话
	private String consigneeIdnum;		// 收货人证件号码
	private String consginorNo;		// 发货人编号
	private String consignorCname;		// 发货人
	private String consginorEname;		// 发货人英文名称
	private String consignorAddress;		// 发货人地址
	private String consignorTel;		// 发货人电话
	private String consignorIdnum;		// 发货人证件号码
	private String wasteFlag;		// 是否有废旧物品
	private String packFlag;		// 是否带有植物性包装及铺垫材料
	private String transTypeNo;		// 运输号码
	private String contractNo;		// 合同号
	private String carrierNoteNo;		// 提/运单号
	private String sheetTypeCodes;		// 随附单据代码串,由相应单据代码串联而得
	private String goodsAddress;		// 货物存放地点
	private String lawNo;		// 报检单号
	private String insStatus;		// 检验检疫状态代码：1待检疫 2合格 3不合格
	private String goodsStatus;		// 商品状态代码：预留
	private String checkStatus;		// 查验状态代码：0未查验 1待查验 2合格 3不合格
	private Date beginDeclDate;		// 开始 申报时间
	private Date endDeclDate;		// 结束 申报时间
	
	public MhTbImportApplyRecordHead() {
		super();
	}

	public MhTbImportApplyRecordHead(String id){
		super(id);
	}

	@ExcelField(title="境外进区流水号", align=2, sort=1)
	public String getIiSerialNo() {
		return iiSerialNo;
	}

	public void setIiSerialNo(String iiSerialNo) {
		this.iiSerialNo = iiSerialNo;
	}
	
	@ExcelField(title="申报号", align=2, sort=2)
	public String getDeclNo() {
		return declNo;
	}

	public void setDeclNo(String declNo) {
		this.declNo = declNo;
	}
	
	@ExcelField(title="备注", align=2, sort=3)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="单申报人名称", align=2, sort=4)
	public String getDeclPerson() {
		return declPerson;
	}

	public void setDeclPerson(String declPerson) {
		this.declPerson = declPerson;
	}
	
	@ExcelField(title="申请单状态代码：1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过", align=2, sort=5)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="组织机构代码", align=2, sort=6)
	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	
	@ExcelField(title="操作状态0正常   1已删除", align=2, sort=7)
	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="申报时间", align=2, sort=8)
	public Date getDeclDate() {
		return declDate;
	}

	public void setDeclDate(Date declDate) {
		this.declDate = declDate;
	}
	
	@ExcelField(title="抽样单号", align=2, sort=9)
	public String getSampleNo() {
		return sampleNo;
	}

	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}
	
	@ExcelField(title="申报类型代码", align=2, sort=10)
	public String getDeclTypeCode() {
		return declTypeCode;
	}

	public void setDeclTypeCode(String declTypeCode) {
		this.declTypeCode = declTypeCode;
	}
	
	@ExcelField(title="贸易方式", align=2, sort=11)
	public String getTradeModeCode() {
		return tradeModeCode;
	}

	public void setTradeModeCode(String tradeModeCode) {
		this.tradeModeCode = tradeModeCode;
	}
	
	@ExcelField(title="申报单位代码", align=2, sort=12)
	public String getEntCode() {
		return entCode;
	}

	public void setEntCode(String entCode) {
		this.entCode = entCode;
	}
	
	@ExcelField(title="电商企业代码", align=2, sort=13)
	public String getCbecode() {
		return cbecode;
	}

	public void setCbecode(String cbecode) {
		this.cbecode = cbecode;
	}
	
	@ExcelField(title="施检机构代码", align=2, sort=14)
	public String getCheckOrgCode() {
		return checkOrgCode;
	}

	public void setCheckOrgCode(String checkOrgCode) {
		this.checkOrgCode = checkOrgCode;
	}
	
	@ExcelField(title="目的机构代码", align=2, sort=15)
	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
	@ExcelField(title="收货人证件类型", align=2, sort=16)
	public String getConsigneeIdtype() {
		return consigneeIdtype;
	}

	public void setConsigneeIdtype(String consigneeIdtype) {
		this.consigneeIdtype = consigneeIdtype;
	}
	
	@ExcelField(title="发货人证件类型", align=2, sort=17)
	public String getConsignorIdtype() {
		return consignorIdtype;
	}

	public void setConsignorIdtype(String consignorIdtype) {
		this.consignorIdtype = consignorIdtype;
	}
	
	@ExcelField(title="运输工具代码", align=2, sort=18)
	public String getTransTypeCode() {
		return transTypeCode;
	}

	public void setTransTypeCode(String transTypeCode) {
		this.transTypeCode = transTypeCode;
	}
	
	@ExcelField(title="贸易国别/地区代码", align=2, sort=19)
	public String getTradeCountryCode() {
		return tradeCountryCode;
	}

	public void setTradeCountryCode(String tradeCountryCode) {
		this.tradeCountryCode = tradeCountryCode;
	}
	
	@ExcelField(title="启运国代码", align=2, sort=20)
	public String getDespCountryCode() {
		return despCountryCode;
	}

	public void setDespCountryCode(String despCountryCode) {
		this.despCountryCode = despCountryCode;
	}
	
	@ExcelField(title="启运口岸代码", align=2, sort=21)
	public String getDespPortCode() {
		return despPortCode;
	}

	public void setDespPortCode(String despPortCode) {
		this.despPortCode = despPortCode;
	}
	
	@ExcelField(title="入境口岸代码", align=2, sort=22)
	public String getEntryPortCode() {
		return entryPortCode;
	}

	public void setEntryPortCode(String entryPortCode) {
		this.entryPortCode = entryPortCode;
	}
	
	@ExcelField(title="集货/备货标识 C备货 B集货", align=2, sort=23)
	public String getStockFlag() {
		return stockFlag;
	}

	public void setStockFlag(String stockFlag) {
		this.stockFlag = stockFlag;
	}
	
	@ExcelField(title="收货人编号", align=2, sort=24)
	public String getConsigneeNo() {
		return consigneeNo;
	}

	public void setConsigneeNo(String consigneeNo) {
		this.consigneeNo = consigneeNo;
	}
	
	@ExcelField(title="收货人", align=2, sort=25)
	public String getConsigneeCname() {
		return consigneeCname;
	}

	public void setConsigneeCname(String consigneeCname) {
		this.consigneeCname = consigneeCname;
	}
	
	@ExcelField(title="收货人英文名称", align=2, sort=26)
	public String getConsigneeEname() {
		return consigneeEname;
	}

	public void setConsigneeEname(String consigneeEname) {
		this.consigneeEname = consigneeEname;
	}
	
	@ExcelField(title="收货人地址", align=2, sort=27)
	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	
	@ExcelField(title="收货人电话", align=2, sort=28)
	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}
	
	@ExcelField(title="收货人证件号码", align=2, sort=29)
	public String getConsigneeIdnum() {
		return consigneeIdnum;
	}

	public void setConsigneeIdnum(String consigneeIdnum) {
		this.consigneeIdnum = consigneeIdnum;
	}
	
	@ExcelField(title="发货人编号", align=2, sort=30)
	public String getConsginorNo() {
		return consginorNo;
	}

	public void setConsginorNo(String consginorNo) {
		this.consginorNo = consginorNo;
	}
	
	@ExcelField(title="发货人", align=2, sort=31)
	public String getConsignorCname() {
		return consignorCname;
	}

	public void setConsignorCname(String consignorCname) {
		this.consignorCname = consignorCname;
	}
	
	@ExcelField(title="发货人英文名称", align=2, sort=32)
	public String getConsginorEname() {
		return consginorEname;
	}

	public void setConsginorEname(String consginorEname) {
		this.consginorEname = consginorEname;
	}
	
	@ExcelField(title="发货人地址", align=2, sort=33)
	public String getConsignorAddress() {
		return consignorAddress;
	}

	public void setConsignorAddress(String consignorAddress) {
		this.consignorAddress = consignorAddress;
	}
	
	@ExcelField(title="发货人电话", align=2, sort=34)
	public String getConsignorTel() {
		return consignorTel;
	}

	public void setConsignorTel(String consignorTel) {
		this.consignorTel = consignorTel;
	}
	
	@ExcelField(title="发货人证件号码", align=2, sort=35)
	public String getConsignorIdnum() {
		return consignorIdnum;
	}

	public void setConsignorIdnum(String consignorIdnum) {
		this.consignorIdnum = consignorIdnum;
	}
	
	@ExcelField(title="是否有废旧物品", align=2, sort=36)
	public String getWasteFlag() {
		return wasteFlag;
	}

	public void setWasteFlag(String wasteFlag) {
		this.wasteFlag = wasteFlag;
	}
	
	@ExcelField(title="是否带有植物性包装及铺垫材料", align=2, sort=37)
	public String getPackFlag() {
		return packFlag;
	}

	public void setPackFlag(String packFlag) {
		this.packFlag = packFlag;
	}
	
	@ExcelField(title="运输号码", align=2, sort=38)
	public String getTransTypeNo() {
		return transTypeNo;
	}

	public void setTransTypeNo(String transTypeNo) {
		this.transTypeNo = transTypeNo;
	}
	
	@ExcelField(title="合同号", align=2, sort=39)
	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
	@ExcelField(title="提/运单号", align=2, sort=40)
	public String getCarrierNoteNo() {
		return carrierNoteNo;
	}

	public void setCarrierNoteNo(String carrierNoteNo) {
		this.carrierNoteNo = carrierNoteNo;
	}
	
	@ExcelField(title="随附单据代码串,由相应单据代码串联而得", align=2, sort=41)
	public String getSheetTypeCodes() {
		return sheetTypeCodes;
	}

	public void setSheetTypeCodes(String sheetTypeCodes) {
		this.sheetTypeCodes = sheetTypeCodes;
	}
	
	@ExcelField(title="货物存放地点", align=2, sort=42)
	public String getGoodsAddress() {
		return goodsAddress;
	}

	public void setGoodsAddress(String goodsAddress) {
		this.goodsAddress = goodsAddress;
	}
	
	@ExcelField(title="报检单号", align=2, sort=43)
	public String getLawNo() {
		return lawNo;
	}

	public void setLawNo(String lawNo) {
		this.lawNo = lawNo;
	}
	
	@ExcelField(title="检验检疫状态代码：1待检疫 2合格 3不合格", align=2, sort=44)
	public String getInsStatus() {
		return insStatus;
	}

	public void setInsStatus(String insStatus) {
		this.insStatus = insStatus;
	}
	
	@ExcelField(title="商品状态代码：预留", align=2, sort=45)
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	
	@ExcelField(title="查验状态代码：0未查验 1待查验 2合格 3不合格", align=2, sort=46)
	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	
	public Date getBeginDeclDate() {
		return beginDeclDate;
	}

	public void setBeginDeclDate(Date beginDeclDate) {
		this.beginDeclDate = beginDeclDate;
	}
	
	public Date getEndDeclDate() {
		return endDeclDate;
	}

	public void setEndDeclDate(Date endDeclDate) {
		this.endDeclDate = endDeclDate;
	}
		
}