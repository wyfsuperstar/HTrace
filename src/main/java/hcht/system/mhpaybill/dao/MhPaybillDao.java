/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhpaybill.dao;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.mhpaybill.entity.MhPaybill;

/**
 * 支付单DAO接口
 * @author BMW
 * @version 2018-11-19
 */
@MyBatisDao
public interface MhPaybillDao extends CrudDao<MhPaybill> {

	
}