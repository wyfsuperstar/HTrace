/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhpaybill.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 支付单Entity
 * @author BMW
 * @version 2018-11-19
 */
public class MhPaybill extends DataEntity<MhPaybill> {
	
	private static final long serialVersionUID = 1L;
	private String paySerialNo;		// 支付单流水号
	private String paymentNo;		// 支付交易号
	private String orderNo;		// 订单编号
	private String cbecode;		// 电商企业代码
	private String paymentCode;		// 支付企业编号
	private Double amount;		// 费用
	private String currencyCode;		// 币制编号
	private String paymentTime;		// 支付时间
	private String remark;		// 备注
	private String msgCode;		// 状态代码    01已入库 02入库失败
	private String msgName;		// 状态名称
	private String msgDesc;		// 操作信息
	private Date msgTime;		// 回执时间
	private String type;		// 区分直邮（Z）和保税(B)
	private Date decltime;		// 申报时间
	private String paymentName;		// 支付企业名称
	private String cbename;		// 电商企业名称
	private String flag;		// 是否单独报检   1 单独报检
	private Date storageTime;		// 报文入库时间
	private Date beginStorageTime;		// 开始 报文入库时间
	private Date endStorageTime;		// 结束 报文入库时间
	
	public MhPaybill() {
		super();
	}

	public MhPaybill(String id){
		super(id);
	}

	@ExcelField(title="支付单流水号", align=2, sort=0)
	public String getPaySerialNo() {
		return paySerialNo;
	}

	public void setPaySerialNo(String paySerialNo) {
		this.paySerialNo = paySerialNo;
	}
	
	@ExcelField(title="支付交易号", align=2, sort=1)
	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	
	@ExcelField(title="订单编号", align=2, sort=2)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="电商企业代码", align=2, sort=3)
	public String getCbecode() {
		return cbecode;
	}

	public void setCbecode(String cbecode) {
		this.cbecode = cbecode;
	}
	
	@ExcelField(title="支付企业编号", align=2, sort=4)
	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	
	@ExcelField(title="费用", align=2, sort=5)
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="币制编号", align=2, sort=6)
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	@ExcelField(title="支付时间", align=2, sort=7)
	public String getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(String paymentTime) {
		this.paymentTime = paymentTime;
	}
	
	@ExcelField(title="备注", align=2, sort=8)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="状态代码    01已入库 02入库失败", align=2, sort=9)
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	
	@ExcelField(title="状态名称", align=2, sort=10)
	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}
	
	@ExcelField(title="操作信息", align=2, sort=11)
	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="回执时间", align=2, sort=12)
	public Date getMsgTime() {
		return msgTime;
	}

	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	
	@ExcelField(title="区分直邮（Z）和保税(B)", align=2, sort=13)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="申报时间", align=2, sort=14)
	public Date getDecltime() {
		return decltime;
	}

	public void setDecltime(Date decltime) {
		this.decltime = decltime;
	}
	
	@ExcelField(title="支付企业名称", align=2, sort=15)
	public String getPaymentName() {
		return paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	
	@ExcelField(title="电商企业名称", align=2, sort=16)
	public String getCbename() {
		return cbename;
	}

	public void setCbename(String cbename) {
		this.cbename = cbename;
	}
	
	@ExcelField(title="是否单独报检   1 单独报检", align=2, sort=17)
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="报文入库时间", align=2, sort=18)
	public Date getStorageTime() {
		return storageTime;
	}

	public void setStorageTime(Date storageTime) {
		this.storageTime = storageTime;
	}
	
	public Date getBeginStorageTime() {
		return beginStorageTime;
	}

	public void setBeginStorageTime(Date beginStorageTime) {
		this.beginStorageTime = beginStorageTime;
	}
	
	public Date getEndStorageTime() {
		return endStorageTime;
	}

	public void setEndStorageTime(Date endStorageTime) {
		this.endStorageTime = endStorageTime;
	}
		
}