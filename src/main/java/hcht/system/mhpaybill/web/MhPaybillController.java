/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhpaybill.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import hcht.system.mhpaybill.entity.MhPaybill;
import hcht.system.mhpaybill.service.MhPaybillService;

/**
 * 支付单Controller
 * @author BMW
 * @version 2018-11-19
 */
@Controller
@RequestMapping(value = "${adminPath}/mhpaybill/mhPaybill")
public class MhPaybillController extends BaseController {

	@Autowired
	private MhPaybillService mhPaybillService;
	
	@ModelAttribute
	public MhPaybill get(@RequestParam(required=false) String id) {
		MhPaybill entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhPaybillService.get(id);
		}
		if (entity == null){
			entity = new MhPaybill();
		}
		return entity;
	}
	
	/**
	 * 支付单列表页面
	 */
	@RequiresPermissions("mhpaybill:mhPaybill:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhPaybill mhPaybill, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhPaybill> page = mhPaybillService.findPage(new Page<MhPaybill>(request, response), mhPaybill); 
		model.addAttribute("page", page);
		return "system/mhpaybill/mhPaybillList";
	}

	/**
	 * 查看，增加，编辑支付单表单页面
	 */
	@RequiresPermissions(value={"mhpaybill:mhPaybill:view","mhpaybill:mhPaybill:add","mhpaybill:mhPaybill:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhPaybill mhPaybill, Model model) {
		model.addAttribute("mhPaybill", mhPaybill);
		return "system/mhpaybill/mhPaybillForm";
	}

	/**
	 * 保存支付单
	 */
	@RequiresPermissions(value={"mhpaybill:mhPaybill:add","mhpaybill:mhPaybill:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhPaybill mhPaybill, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhPaybill)){
			return form(mhPaybill, model);
		}
		if(!mhPaybill.getIsNewRecord()){//编辑表单保存
			MhPaybill t = mhPaybillService.get(mhPaybill.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhPaybill, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhPaybillService.save(t);//保存
		}else{//新增表单保存
			mhPaybillService.save(mhPaybill);//保存
		}
		addMessage(redirectAttributes, "保存支付单成功");
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
	}
	
	/**
	 * 删除支付单
	 */
	@RequiresPermissions("mhpaybill:mhPaybill:del")
	@RequestMapping(value = "delete")
	public String delete(MhPaybill mhPaybill, RedirectAttributes redirectAttributes) {
		mhPaybillService.delete(mhPaybill);
		addMessage(redirectAttributes, "删除支付单成功");
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
	}
	
	/**
	 * 批量删除支付单
	 */
	@RequiresPermissions("mhpaybill:mhPaybill:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhPaybillService.delete(mhPaybillService.get(id));
		}
		addMessage(redirectAttributes, "删除支付单成功");
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhpaybill:mhPaybill:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhPaybill mhPaybill, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "支付单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhPaybill> page = mhPaybillService.findPage(new Page<MhPaybill>(request, response, -1), mhPaybill);
    		new ExportExcel("支付单", MhPaybill.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出支付单记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhpaybill:mhPaybill:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhPaybill> list = ei.getDataList(MhPaybill.class);
			for (MhPaybill mhPaybill : list){
				try{
					mhPaybillService.save(mhPaybill);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条支付单记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条支付单记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入支付单失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
    }
	
	/**
	 * 下载导入支付单数据模板
	 */
	@RequiresPermissions("mhpaybill:mhPaybill:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "支付单数据导入模板.xlsx";
    		List<MhPaybill> list = Lists.newArrayList(); 
    		new ExportExcel("支付单数据", MhPaybill.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhpaybill/mhPaybill/?repage";
    }
	
	
	

}