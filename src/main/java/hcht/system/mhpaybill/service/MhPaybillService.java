/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhpaybill.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.system.mhpaybill.entity.MhPaybill;
import hcht.system.mhpaybill.dao.MhPaybillDao;

/**
 * 支付单Service
 * @author BMW
 * @version 2018-11-19
 */
@Service
@Transactional(readOnly = true)
public class MhPaybillService extends CrudService<MhPaybillDao, MhPaybill> {

	public MhPaybill get(String id) {
		return super.get(id);
	}
	
	public List<MhPaybill> findList(MhPaybill mhPaybill) {
		return super.findList(mhPaybill);
	}
	
	public Page<MhPaybill> findPage(Page<MhPaybill> page, MhPaybill mhPaybill) {
		return super.findPage(page, mhPaybill);
	}
	
	@Transactional(readOnly = false)
	public void save(MhPaybill mhPaybill) {
		super.save(mhPaybill);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhPaybill mhPaybill) {
		super.delete(mhPaybill);
	}
	
	
	
	
}