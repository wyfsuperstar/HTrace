/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhappparam;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 参数设置Entity
 * @author BMW
 * @version 2018-11-16
 */
public class MhAppParam extends DataEntity<MhAppParam> {
	
	private static final long serialVersionUID = 1L;
	private String paramId;		// param_id
	private String typeId;		// type_id
	private String paramCode;		// param_code
	private String paramName;		// param_name
	private Integer paramSeries;		// param_series
	private String paramEnable;		// param_enable
	private String codeType;		// code_type
	private String paramRemark;		// param_remark
	private String creater;		// creater
	private Date modifyDate;		// modify_date
	private String modifier;		// modifier
	
	public MhAppParam() {
		super();
	}

	public MhAppParam(String paramId){
		super(paramId);
	}

	@ExcelField(title="param_id", align=2, sort=0)
	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
		this.id = paramId;
	}
	
	@ExcelField(title="type_id", align=2, sort=1)
	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
	@ExcelField(title="param_code", align=2, sort=2)
	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}
	
	@ExcelField(title="param_name", align=2, sort=3)
	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	@ExcelField(title="param_series", align=2, sort=4)
	public Integer getParamSeries() {
		return paramSeries;
	}

	public void setParamSeries(Integer paramSeries) {
		this.paramSeries = paramSeries;
	}
	
	@ExcelField(title="param_enable", align=2, sort=5)
	public String getParamEnable() {
		return paramEnable;
	}

	public void setParamEnable(String paramEnable) {
		this.paramEnable = paramEnable;
	}
	
	@ExcelField(title="code_type", align=2, sort=6)
	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	
	@ExcelField(title="param_remark", align=2, sort=7)
	public String getParamRemark() {
		return paramRemark;
	}

	public void setParamRemark(String paramRemark) {
		this.paramRemark = paramRemark;
	}
	
	@ExcelField(title="creater", align=2, sort=9)
	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="modify_date", align=2, sort=10)
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	@ExcelField(title="modifier", align=2, sort=11)
	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}