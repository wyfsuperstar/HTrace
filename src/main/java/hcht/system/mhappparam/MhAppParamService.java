/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhappparam;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 参数设置Service
 * @author BMW
 * @version 2018-11-16
 */
@Service
@Transactional(readOnly = true)
public class MhAppParamService extends CrudService<MhAppParamDao, MhAppParam> {

	public MhAppParam get(String id) {
		return super.get(id);
	}
	
	public List<MhAppParam> findList(MhAppParam mhAppParam) {
		return super.findList(mhAppParam);
	}
	
	public Page<MhAppParam> findPage(Page<MhAppParam> page, MhAppParam mhAppParam) {
		return super.findPage(page, mhAppParam);
	}
	
	@Transactional(readOnly = false)
	public void save(MhAppParam mhAppParam) {
		super.save(mhAppParam);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhAppParam mhAppParam) {
		super.delete(mhAppParam);
	}
	
	
	
	
}