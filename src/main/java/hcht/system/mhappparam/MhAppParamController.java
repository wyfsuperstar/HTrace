/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhappparam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 参数设置Controller
 * @author BMW
 * @version 2018-11-16
 */
@Controller
@RequestMapping(value = "${adminPath}/mhappparam/mhAppParam")
public class MhAppParamController extends BaseController {

	@Autowired
	private MhAppParamService mhAppParamService;
	
	@ModelAttribute
	public MhAppParam get(@RequestParam(required=false) String id) {
		MhAppParam entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhAppParamService.get(id);
		}
		if (entity == null){
			entity = new MhAppParam();
		}
		return entity;
	}
	
	/**
	 * 参数设置列表页面
	 */
	@RequiresPermissions("mhappparam:mhAppParam:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhAppParam mhAppParam, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhAppParam> page = mhAppParamService.findPage(new Page<MhAppParam>(request, response), mhAppParam); 
		model.addAttribute("page", page);
		return "system/mhappparam/mhAppParamList";
	}

	/**
	 * 查看，增加，编辑参数设置表单页面
	 */
	@RequiresPermissions(value={"mhappparam:mhAppParam:view","mhappparam:mhAppParam:add","mhappparam:mhAppParam:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhAppParam mhAppParam, Model model) {
		model.addAttribute("mhAppParam", mhAppParam);
		return "system/mhappparam/mhAppParamForm";
	}

	/**
	 * 保存参数设置
	 */
	@RequiresPermissions(value={"mhappparam:mhAppParam:add","mhappparam:mhAppParam:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhAppParam mhAppParam, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhAppParam)){
			return form(mhAppParam, model);
		}
		if(!mhAppParam.getIsNewRecord()){//编辑表单保存
			MhAppParam t = mhAppParamService.get(mhAppParam.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhAppParam, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhAppParamService.save(t);//保存
		}else{//新增表单保存
			mhAppParamService.save(mhAppParam);//保存
		}
		addMessage(redirectAttributes, "保存参数设置成功");
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
	}
	
	/**
	 * 删除参数设置
	 */
	@RequiresPermissions("mhappparam:mhAppParam:del")
	@RequestMapping(value = "delete")
	public String delete(MhAppParam mhAppParam, RedirectAttributes redirectAttributes) {
		mhAppParamService.delete(mhAppParam);
		addMessage(redirectAttributes, "删除参数设置成功");
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
	}
	
	/**
	 * 批量删除参数设置
	 */
	@RequiresPermissions("mhappparam:mhAppParam:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhAppParamService.delete(mhAppParamService.get(id));
		}
		addMessage(redirectAttributes, "删除参数设置成功");
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("mhappparam:mhAppParam:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhAppParam mhAppParam, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "参数设置"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhAppParam> page = mhAppParamService.findPage(new Page<MhAppParam>(request, response, -1), mhAppParam);
    		new ExportExcel("参数设置", MhAppParam.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出参数设置记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("mhappparam:mhAppParam:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhAppParam> list = ei.getDataList(MhAppParam.class);
			for (MhAppParam mhAppParam : list){
				try{
					mhAppParamService.save(mhAppParam);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条参数设置记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条参数设置记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入参数设置失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
    }
	
	/**
	 * 下载导入参数设置数据模板
	 */
	@RequiresPermissions("mhappparam:mhAppParam:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "参数设置数据导入模板.xlsx";
    		List<MhAppParam> list = Lists.newArrayList(); 
    		new ExportExcel("参数设置数据", MhAppParam.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mhappparam/mhAppParam/?repage";
    }
	
	
	

}