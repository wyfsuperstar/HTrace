/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.system.mhappparam;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.system.mhappparam.MhAppParam;

/**
 * 参数设置DAO接口
 * @author BMW
 * @version 2018-11-16
 */
@MyBatisDao
public interface MhAppParamDao extends CrudDao<MhAppParam> {

	
}