/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.goods;

import com.jeeplus.common.mapper.JsonMapper;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import hcht.manage.feedback.FeedbackStatistic;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * 跨境电商反馈信息查询Controller
 * @author why
 * @version 2018-03-21
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/goodsOrder")
public class GoodsOrderController extends BaseController {

	@Autowired
	private OrderService orderService;

	/**
	 * 各启运国订单商品数量统计（柱图）
	 */
	@RequiresPermissions("manage:tracelog:statistics")
	@RequestMapping(value = "despCountryStatistic")
	public String despCountryStatistic(OrderStatistic orderStatistic, Model model) {
		List<OrderStatistic> list = null;
		Page<OrderStatistic> page = new Page<>();
		page.setPageSize(10);
		orderStatistic.setPage(page);
		if(orderStatistic.getBeginDate() != null && orderStatistic.getEndDate() != null){
			list = orderService.findDespCountryStatistic(orderStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/goodsOrder/statistic/despCountryStatistics";
	}

	/**
	 * 各电商订单商品数量统计（柱图）
	 */
	@RequiresPermissions("manage:tracelog:statistics")
	@RequestMapping(value = "cbeStatistic")
	public String cbeStatistic(OrderStatistic orderStatistic, Model model) {
		List<OrderStatistic> list = null;
		Page<OrderStatistic> page = new Page<>();
		page.setPageSize(10);
		orderStatistic.setPage(page);
		if(orderStatistic.getBeginDate() != null && orderStatistic.getEndDate() != null){
			list = orderService.findCbeStatistic(orderStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/goodsOrder/statistic/cbeStatistics";
	}

	/**
	 * 各商品类别的订单商品数量统计（柱图）
	 */
	@RequiresPermissions("manage:tracelog:statistics")
	@RequestMapping(value = "goodsCategoryStatistic")
	public String goodsCategoryStatistic(OrderStatistic orderStatistic, Model model) {
		List<OrderStatistic> list = null;
		Page<OrderStatistic> page = new Page<>();
		page.setPageSize(10);
		orderStatistic.setPage(page);
		if(orderStatistic.getBeginDate() != null && orderStatistic.getEndDate() != null){
			list = orderService.findGoodsCategoryStatistic(orderStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/goodsOrder/statistic/goodsCategoryStatistics";
	}
	
	
	

}