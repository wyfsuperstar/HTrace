package hcht.manage.goods;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 商品订单统计信息
 * Created by gf on 2018/5/25.
 */
public class OrderStatistic extends DataEntity<OrderStatistic> {

    private static final long serialVersionUID = 1L;
    private String itemName;		// 项目名称
    private Double itemNum;		// 项目数量

    //统计查询条件
    private String beginDate;		//起始时间
    private String endDate;		//截止时间

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getItemNum() {
        return itemNum;
    }

    public void setItemNum(Double itemNum) {
        this.itemNum = itemNum;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
