package hcht.manage.goods;

import com.jeeplus.common.service.CrudService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 商品订单Service
 * Created by gf on 2018/5/25.
 */
@Service
@Transactional(readOnly = true)
public class OrderService extends CrudService<OrderDao, OrderStatistic> {

    public List<OrderStatistic> findDespCountryStatistic(OrderStatistic orderStatistic){
        return dao.findDespCountryStatistic(orderStatistic);
    }
    public List<OrderStatistic> findCbeStatistic(OrderStatistic orderStatistic){
        return dao.findCbeStatistic(orderStatistic);
    }
    public List<OrderStatistic> findGoodsCategoryStatistic(OrderStatistic orderStatistic){
        return dao.findGoodsCategoryStatistic(orderStatistic);
    }

}
