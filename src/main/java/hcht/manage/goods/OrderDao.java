package hcht.manage.goods;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

import java.util.List;

/**
 * 商品订单Dao
 * Created by gf on 2018/5/25.
 */
@MyBatisDao
public interface OrderDao extends CrudDao<OrderStatistic> {

    List<OrderStatistic> findDespCountryStatistic(OrderStatistic orderStatistic);
    List<OrderStatistic> findCbeStatistic(OrderStatistic orderStatistic);
    List<OrderStatistic> findGoodsCategoryStatistic(OrderStatistic orderStatistic);
}
