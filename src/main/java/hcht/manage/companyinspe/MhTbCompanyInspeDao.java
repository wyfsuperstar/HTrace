/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.companyinspe;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 企业备案管理DAO接口
 * @author BMW
 * @version 2018-05-08
 */
@MyBatisDao
public interface MhTbCompanyInspeDao extends CrudDao<MhTbCompanyInspe> {

	
}