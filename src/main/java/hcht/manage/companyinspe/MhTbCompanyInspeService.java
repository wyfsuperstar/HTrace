/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.companyinspe;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.manage.companyinspe.MhTbCompanyInspe;
import hcht.manage.companyinspe.MhTbCompanyInspeDao;

/**
 * 企业备案管理Service
 * @author BMW
 * @version 2018-05-08
 */
@Service
@Transactional(readOnly = true)
public class MhTbCompanyInspeService extends CrudService<MhTbCompanyInspeDao, MhTbCompanyInspe> {

	public MhTbCompanyInspe get(String id) {
		return super.get(id);
	}
	
	public List<MhTbCompanyInspe> findList(MhTbCompanyInspe mhTbCompanyInspe) {
		return super.findList(mhTbCompanyInspe);
	}
	
	public Page<MhTbCompanyInspe> findPage(Page<MhTbCompanyInspe> page, MhTbCompanyInspe mhTbCompanyInspe) {
		return super.findPage(page, mhTbCompanyInspe);
	}
	
	@Transactional(readOnly = false)
	public void save(MhTbCompanyInspe mhTbCompanyInspe) {
		super.save(mhTbCompanyInspe);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhTbCompanyInspe mhTbCompanyInspe) {
		super.delete(mhTbCompanyInspe);
	}
	
	
	
	
}