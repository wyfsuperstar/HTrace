/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.companyinspe;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import hcht.manage.companyinspe.MhTbCompanyInspe;
import hcht.manage.companyinspe.MhTbCompanyInspeService;

/**
 * 企业备案管理Controller
 * @author BMW
 * @version 2018-05-08
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/companyinspe")
public class MhTbCompanyInspeController extends BaseController {

	@Autowired
	private MhTbCompanyInspeService mhTbCompanyInspeService;
	
	@ModelAttribute
	public MhTbCompanyInspe get(@RequestParam(required=false) String id) {
		MhTbCompanyInspe entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhTbCompanyInspeService.get(id);
		}
		if (entity == null){
			entity = new MhTbCompanyInspe();
		}
		return entity;
	}
	
	/**
	 * 修改企业备案信息成功列表页面
	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhTbCompanyInspe mhTbCompanyInspe, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhTbCompanyInspe> page = mhTbCompanyInspeService.findPage(new Page<MhTbCompanyInspe>(request, response), mhTbCompanyInspe); 
		model.addAttribute("page", page);
		return "hcht/manage/companyinspe/mhTbCompanyInspeList";
	}

	/**
	 * 查看，增加，编辑修改企业备案信息成功表单页面
	 */
//	@RequiresPermissions(value={"companyinspe:mhTbCompanyInspe:view","companyinspe:mhTbCompanyInspe:add","companyinspe:mhTbCompanyInspe:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhTbCompanyInspe mhTbCompanyInspe, Model model) {
		model.addAttribute("mhTbCompanyInspe", mhTbCompanyInspe);
		return "hcht/manage/companyinspe/mhTbCompanyInspeForm";
	}

	/**
	 * 保存修改企业备案信息成功
	 */
//	@RequiresPermissions(value={"companyinspe:mhTbCompanyInspe:add","companyinspe:mhTbCompanyInspe:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhTbCompanyInspe mhTbCompanyInspe, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhTbCompanyInspe)){
			return form(mhTbCompanyInspe, model);
		}
		if(!mhTbCompanyInspe.getIsNewRecord()){//编辑表单保存
			MhTbCompanyInspe t = mhTbCompanyInspeService.get(mhTbCompanyInspe.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhTbCompanyInspe, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhTbCompanyInspeService.save(t);//保存
		}else{//新增表单保存
			mhTbCompanyInspeService.save(mhTbCompanyInspe);//保存
		}
		addMessage(redirectAttributes, "保存修改企业备案信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
	}

	/**
	 * 删除修改企业备案信息成功
	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:del")
	@RequestMapping(value = "delete")
	public String delete(MhTbCompanyInspe mhTbCompanyInspe, RedirectAttributes redirectAttributes) {
		mhTbCompanyInspeService.delete(mhTbCompanyInspe);
		addMessage(redirectAttributes, "删除修改企业备案信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
	}

	/**
	 * 批量删除修改企业备案信息成功
	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhTbCompanyInspeService.delete(mhTbCompanyInspeService.get(id));
		}
		addMessage(redirectAttributes, "删除修改企业备案信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
	}

	/**
	 * 导出excel文件
	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhTbCompanyInspe mhTbCompanyInspe, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "修改企业备案信息成功"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhTbCompanyInspe> page = mhTbCompanyInspeService.findPage(new Page<MhTbCompanyInspe>(request, response, -1), mhTbCompanyInspe);
    		new ExportExcel("修改企业备案信息成功", MhTbCompanyInspe.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出修改企业备案信息成功记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
    }

	/**
	 * 导入Excel数据

	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhTbCompanyInspe> list = ei.getDataList(MhTbCompanyInspe.class);
			for (MhTbCompanyInspe mhTbCompanyInspe : list){
				try{
					mhTbCompanyInspeService.save(mhTbCompanyInspe);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条修改企业备案信息成功记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条修改企业备案信息成功记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入修改企业备案信息成功失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
    }

	/**
	 * 下载导入修改企业备案信息成功数据模板
	 */
//	@RequiresPermissions("companyinspe:mhTbCompanyInspe:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "修改企业备案信息成功数据导入模板.xlsx";
    		List<MhTbCompanyInspe> list = Lists.newArrayList();
    		new ExportExcel("修改企业备案信息成功数据", MhTbCompanyInspe.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/companyinspe/?repage";
    }
	
	
	

}