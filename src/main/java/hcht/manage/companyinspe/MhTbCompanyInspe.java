/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.companyinspe;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 企业备案管理Entity
 * @author BMW
 * @version 2018-05-08
 */
public class MhTbCompanyInspe extends DataEntity<MhTbCompanyInspe> {
	
	private static final long serialVersionUID = 1L;
	private String ciqOrgTypeCode;		// 属地检验检疫机构代码
	private String registerAddress;		// 注册地址
	private String fax;		// 单位联系人联系传真
	private Date applyTime;		// 申报日期
	private String busLcNo;		// 营业执照号
	private String busLcSignUnit;		// 营业执照签发单位
	private String lawMan;		// 法定代表人
	private String contactMan;		// 单位联系人
	private String contactManMobile;		// 单位联系人手机
	private String contactManEmail;		// 单位联系人邮箱
	private String remark;		// 备注
	private String declPerson;		// 申报人
	private String url;		// 企业备案网址信息
	private String siteName;		// 网站名
	private String busScope;		// 经营商品
	private String entSerialNo;		// 企业备案流水号
	private String entCname;		// 企业中文名
	private String entEname;		// 企业英文名
	private String entTypePl;		// 企业类型：平台（Y是/N否）
	private String entTypeCbe;		// 企业类型：电商（Y是/N否）
	private String entTypeLst;		// 企业类型：物流（Y是/N否）
	private String entTypeAge;		// 企业类型：代理公司（Y是/N否）
	private String entTypeSto;		// 企业类型：仓储（Y是/N否）
	private String entTypePay;		// 企业类型：支付企业（Y是/N否）
	private String entTypeRgl;		// 企业类型：监管场所（Y是/N否）
	private String entTypeOt;		// 企业类型：其他（Y是/N否）
	private String agentEntNo;		// 代理企业编号
	private String agentEntName;		// 代理企业名称
	private String agentEntMan;		// 代理企业联系人
	private String agentEntTel;		// 代理企业联系电话
	private String agentEntLawMan;		// 代理企业法人代表
	private String agentEntLawManTel;		// 代理企业法人电话
	private String agentEntOrgNo;		// 代理企业组织机构代码
	private String agentEntEmail;		// 代理企业邮箱
	private String agentEntCountry;		// 代理企业国别
	private String agentEntAddress;		// 代理企业地址
	private String status;		// 申报状态   1一级审核未通过  2二级审核通过  3已提交 03待审核  4二级审核未通过  5一级审核通过  6暂存    9电子审单未通过 
	private String lockflag;		// 锁定状态    N未锁定 Y已锁定
	private String refCompanyId;		// 企业ID
	private String attribute;		// 企业备案号
	private String orgNo;		// 统一社会信用代码/组织机构代码
	private String officeAddress;		// 单位办公地址
	private String empCount;		// 员工数
	private String srvEmployeeCount;		// 售后服务人数
	private String investSource;		// 投资来源（国别/地区）
	private String lawManTel;		// 法人代表联系电话
	private String emergencyMan;		// 应急联络负责人
	private String emergencyManMobile;		// 应急联络负责人手机
	private String emergencyManTel;		// 应急联络负责人电话
	private String qaSafeMan;		// 质量安全管理员
	private String qaSafeManMobile;		// 质量安全管理员手机
	private String qaSafeManEmail;		// 质量安全管理员邮箱
	private String activeInd;		// 操作状态0正常   1已删除
	private String changestatus;		// 变更状态： 1通过 2 未通过  3已修改 4已提交变更
	private String statusName;		// 状态名称
	private String lockflagName;		// 锁定状态名称
	private String hgAttribute;		// 海关企业备案号
	private String inspectionflag;		// 是否单独报检  0 否  1 是
	
	public MhTbCompanyInspe() {
		super();
	}

	public MhTbCompanyInspe(String id){
		super(id);
	}

	@ExcelField(title="属地检验检疫机构代码", align=2, sort=1)
	public String getCiqOrgTypeCode() {
		return ciqOrgTypeCode;
	}

	public void setCiqOrgTypeCode(String ciqOrgTypeCode) {
		this.ciqOrgTypeCode = ciqOrgTypeCode;
	}
	
	@ExcelField(title="注册地址", align=2, sort=2)
	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}
	
	@ExcelField(title="单位联系人联系传真", align=2, sort=3)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="申报日期", align=2, sort=4)
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	
	@ExcelField(title="营业执照号", align=2, sort=5)
	public String getBusLcNo() {
		return busLcNo;
	}

	public void setBusLcNo(String busLcNo) {
		this.busLcNo = busLcNo;
	}
	
	@ExcelField(title="营业执照签发单位", align=2, sort=6)
	public String getBusLcSignUnit() {
		return busLcSignUnit;
	}

	public void setBusLcSignUnit(String busLcSignUnit) {
		this.busLcSignUnit = busLcSignUnit;
	}
	
	@ExcelField(title="法定代表人", align=2, sort=7)
	public String getLawMan() {
		return lawMan;
	}

	public void setLawMan(String lawMan) {
		this.lawMan = lawMan;
	}
	
	@ExcelField(title="单位联系人", align=2, sort=8)
	public String getContactMan() {
		return contactMan;
	}

	public void setContactMan(String contactMan) {
		this.contactMan = contactMan;
	}
	
	@ExcelField(title="单位联系人手机", align=2, sort=9)
	public String getContactManMobile() {
		return contactManMobile;
	}

	public void setContactManMobile(String contactManMobile) {
		this.contactManMobile = contactManMobile;
	}
	
	@ExcelField(title="单位联系人邮箱", align=2, sort=10)
	public String getContactManEmail() {
		return contactManEmail;
	}

	public void setContactManEmail(String contactManEmail) {
		this.contactManEmail = contactManEmail;
	}
	
	@ExcelField(title="备注", align=2, sort=11)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="申报人", align=2, sort=12)
	public String getDeclPerson() {
		return declPerson;
	}

	public void setDeclPerson(String declPerson) {
		this.declPerson = declPerson;
	}
	
	@ExcelField(title="企业备案网址信息", align=2, sort=13)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@ExcelField(title="网站名", align=2, sort=14)
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	@ExcelField(title="经营商品", align=2, sort=15)
	public String getBusScope() {
		return busScope;
	}

	public void setBusScope(String busScope) {
		this.busScope = busScope;
	}
	
	@ExcelField(title="企业备案流水号", align=2, sort=16)
	public String getEntSerialNo() {
		return entSerialNo;
	}

	public void setEntSerialNo(String entSerialNo) {
		this.entSerialNo = entSerialNo;
	}
	
	@ExcelField(title="企业中文名", align=2, sort=17)
	public String getEntCname() {
		return entCname;
	}

	public void setEntCname(String entCname) {
		this.entCname = entCname;
	}
	
	@ExcelField(title="企业英文名", align=2, sort=18)
	public String getEntEname() {
		return entEname;
	}

	public void setEntEname(String entEname) {
		this.entEname = entEname;
	}
	
	@ExcelField(title="企业类型：平台（Y是/N否）", align=2, sort=19)
	public String getEntTypePl() {
		return entTypePl;
	}

	public void setEntTypePl(String entTypePl) {
		this.entTypePl = entTypePl;
	}
	
	@ExcelField(title="企业类型：电商（Y是/N否）", align=2, sort=20)
	public String getEntTypeCbe() {
		return entTypeCbe;
	}

	public void setEntTypeCbe(String entTypeCbe) {
		this.entTypeCbe = entTypeCbe;
	}
	
	@ExcelField(title="企业类型：物流（Y是/N否）", align=2, sort=21)
	public String getEntTypeLst() {
		return entTypeLst;
	}

	public void setEntTypeLst(String entTypeLst) {
		this.entTypeLst = entTypeLst;
	}
	
	@ExcelField(title="企业类型：代理公司（Y是/N否）", align=2, sort=22)
	public String getEntTypeAge() {
		return entTypeAge;
	}

	public void setEntTypeAge(String entTypeAge) {
		this.entTypeAge = entTypeAge;
	}
	
	@ExcelField(title="企业类型：仓储（Y是/N否）", align=2, sort=23)
	public String getEntTypeSto() {
		return entTypeSto;
	}

	public void setEntTypeSto(String entTypeSto) {
		this.entTypeSto = entTypeSto;
	}
	
	@ExcelField(title="企业类型：支付企业（Y是/N否）", align=2, sort=24)
	public String getEntTypePay() {
		return entTypePay;
	}

	public void setEntTypePay(String entTypePay) {
		this.entTypePay = entTypePay;
	}
	
	@ExcelField(title="企业类型：监管场所（Y是/N否）", align=2, sort=25)
	public String getEntTypeRgl() {
		return entTypeRgl;
	}

	public void setEntTypeRgl(String entTypeRgl) {
		this.entTypeRgl = entTypeRgl;
	}
	
	@ExcelField(title="企业类型：其他（Y是/N否）", align=2, sort=26)
	public String getEntTypeOt() {
		return entTypeOt;
	}

	public void setEntTypeOt(String entTypeOt) {
		this.entTypeOt = entTypeOt;
	}
	
	@ExcelField(title="代理企业编号", align=2, sort=27)
	public String getAgentEntNo() {
		return agentEntNo;
	}

	public void setAgentEntNo(String agentEntNo) {
		this.agentEntNo = agentEntNo;
	}
	
	@ExcelField(title="代理企业名称", align=2, sort=28)
	public String getAgentEntName() {
		return agentEntName;
	}

	public void setAgentEntName(String agentEntName) {
		this.agentEntName = agentEntName;
	}
	
	@ExcelField(title="代理企业联系人", align=2, sort=29)
	public String getAgentEntMan() {
		return agentEntMan;
	}

	public void setAgentEntMan(String agentEntMan) {
		this.agentEntMan = agentEntMan;
	}
	
	@ExcelField(title="代理企业联系电话", align=2, sort=30)
	public String getAgentEntTel() {
		return agentEntTel;
	}

	public void setAgentEntTel(String agentEntTel) {
		this.agentEntTel = agentEntTel;
	}
	
	@ExcelField(title="代理企业法人代表", align=2, sort=31)
	public String getAgentEntLawMan() {
		return agentEntLawMan;
	}

	public void setAgentEntLawMan(String agentEntLawMan) {
		this.agentEntLawMan = agentEntLawMan;
	}
	
	@ExcelField(title="代理企业法人电话", align=2, sort=32)
	public String getAgentEntLawManTel() {
		return agentEntLawManTel;
	}

	public void setAgentEntLawManTel(String agentEntLawManTel) {
		this.agentEntLawManTel = agentEntLawManTel;
	}
	
	@ExcelField(title="代理企业组织机构代码", align=2, sort=33)
	public String getAgentEntOrgNo() {
		return agentEntOrgNo;
	}

	public void setAgentEntOrgNo(String agentEntOrgNo) {
		this.agentEntOrgNo = agentEntOrgNo;
	}
	
	@ExcelField(title="代理企业邮箱", align=2, sort=34)
	public String getAgentEntEmail() {
		return agentEntEmail;
	}

	public void setAgentEntEmail(String agentEntEmail) {
		this.agentEntEmail = agentEntEmail;
	}
	
	@ExcelField(title="代理企业国别", align=2, sort=35)
	public String getAgentEntCountry() {
		return agentEntCountry;
	}

	public void setAgentEntCountry(String agentEntCountry) {
		this.agentEntCountry = agentEntCountry;
	}
	
	@ExcelField(title="代理企业地址", align=2, sort=36)
	public String getAgentEntAddress() {
		return agentEntAddress;
	}

	public void setAgentEntAddress(String agentEntAddress) {
		this.agentEntAddress = agentEntAddress;
	}
	
	@ExcelField(title="申报状态   1一级审核未通过  2二级审核通过  3已提交 03待审核  4二级审核未通过  5一级审核通过  6暂存    9电子审单未通过 ", align=2, sort=37)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="锁定状态    N未锁定 Y已锁定", align=2, sort=38)
	public String getLockflag() {
		return lockflag;
	}

	public void setLockflag(String lockflag) {
		this.lockflag = lockflag;
	}
	
	@ExcelField(title="企业ID", align=2, sort=39)
	public String getRefCompanyId() {
		return refCompanyId;
	}

	public void setRefCompanyId(String refCompanyId) {
		this.refCompanyId = refCompanyId;
	}
	
	@ExcelField(title="企业备案号", align=2, sort=41)
	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	@ExcelField(title="统一社会信用代码/组织机构代码", align=2, sort=42)
	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	
	@ExcelField(title="单位办公地址", align=2, sort=43)
	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	
	@ExcelField(title="员工数", align=2, sort=44)
	public String getEmpCount() {
		return empCount;
	}

	public void setEmpCount(String empCount) {
		this.empCount = empCount;
	}
	
	@ExcelField(title="售后服务人数", align=2, sort=45)
	public String getSrvEmployeeCount() {
		return srvEmployeeCount;
	}

	public void setSrvEmployeeCount(String srvEmployeeCount) {
		this.srvEmployeeCount = srvEmployeeCount;
	}
	
	@ExcelField(title="投资来源（国别/地区）", align=2, sort=46)
	public String getInvestSource() {
		return investSource;
	}

	public void setInvestSource(String investSource) {
		this.investSource = investSource;
	}
	
	@ExcelField(title="法人代表联系电话", align=2, sort=47)
	public String getLawManTel() {
		return lawManTel;
	}

	public void setLawManTel(String lawManTel) {
		this.lawManTel = lawManTel;
	}
	
	@ExcelField(title="应急联络负责人", align=2, sort=48)
	public String getEmergencyMan() {
		return emergencyMan;
	}

	public void setEmergencyMan(String emergencyMan) {
		this.emergencyMan = emergencyMan;
	}
	
	@ExcelField(title="应急联络负责人手机", align=2, sort=49)
	public String getEmergencyManMobile() {
		return emergencyManMobile;
	}

	public void setEmergencyManMobile(String emergencyManMobile) {
		this.emergencyManMobile = emergencyManMobile;
	}
	
	@ExcelField(title="应急联络负责人电话", align=2, sort=50)
	public String getEmergencyManTel() {
		return emergencyManTel;
	}

	public void setEmergencyManTel(String emergencyManTel) {
		this.emergencyManTel = emergencyManTel;
	}
	
	@ExcelField(title="质量安全管理员", align=2, sort=51)
	public String getQaSafeMan() {
		return qaSafeMan;
	}

	public void setQaSafeMan(String qaSafeMan) {
		this.qaSafeMan = qaSafeMan;
	}
	
	@ExcelField(title="质量安全管理员手机", align=2, sort=52)
	public String getQaSafeManMobile() {
		return qaSafeManMobile;
	}

	public void setQaSafeManMobile(String qaSafeManMobile) {
		this.qaSafeManMobile = qaSafeManMobile;
	}
	
	@ExcelField(title="质量安全管理员邮箱", align=2, sort=53)
	public String getQaSafeManEmail() {
		return qaSafeManEmail;
	}

	public void setQaSafeManEmail(String qaSafeManEmail) {
		this.qaSafeManEmail = qaSafeManEmail;
	}
	
	@ExcelField(title="操作状态0正常   1已删除", align=2, sort=54)
	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}
	
	@ExcelField(title="变更状态： 1通过 2 未通过  3已修改 4已提交变更", align=2, sort=55)
	public String getChangestatus() {
		return changestatus;
	}

	public void setChangestatus(String changestatus) {
		this.changestatus = changestatus;
	}
	
	@ExcelField(title="状态名称", align=2, sort=56)
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	@ExcelField(title="锁定状态名称", align=2, sort=57)
	public String getLockflagName() {
		return lockflagName;
	}

	public void setLockflagName(String lockflagName) {
		this.lockflagName = lockflagName;
	}
	
	@ExcelField(title="海关企业备案号", align=2, sort=58)
	public String getHgAttribute() {
		return hgAttribute;
	}

	public void setHgAttribute(String hgAttribute) {
		this.hgAttribute = hgAttribute;
	}
	
	@ExcelField(title="是否单独报检  0 否  1 是", align=2, sort=59)
	public String getInspectionflag() {
		return inspectionflag;
	}

	public void setInspectionflag(String inspectionflag) {
		this.inspectionflag = inspectionflag;
	}
	
}