/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.feedback;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.jeeplus.common.mapper.JsonMapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 跨境电商反馈信息查询Controller
 * @author why
 * @version 2018-03-21
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/feedback")
public class FeedbackController extends BaseController {

	@Autowired
	private FeedbackService feedbackService;
	
	@ModelAttribute
	public Feedback get(@RequestParam(required=false) String id) {
		Feedback entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = feedbackService.get(id);
		}
		if (entity == null){
			entity = new Feedback();
		}
		return entity;
	}
	
	/**
	 * 跨境电商反馈信息查询列表页面
	 */
	@RequiresPermissions("manage:feedback:list")
	@RequestMapping(value = {"list", ""})
	public String list(Feedback feedback, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Feedback> page = feedbackService.findPage(new Page<Feedback>(request, response), feedback); 
		model.addAttribute("page", page);
		return "hcht/manage/feedback/feedbackList";
	}

	/**
	 * 查看，增加，编辑跨境电商反馈信息查询表单页面
	 */
//	@RequiresPermissions(value={"manage:feedback:view","feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Feedback feedback, Model model) {
		model.addAttribute("feedback", feedback);
		return "hcht/manage/feedback/feedbackForm";
	}
	@RequestMapping(value = "backform")
	public String backform(Feedback feedback, Model model) {
		model.addAttribute("feedback", feedback);
		return "hcht/manage/feedback/feedbackReturn";
	}

	/**
	 * 保存跨境电商反馈信息查询
	 */
//	@RequiresPermissions(value={"feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Feedback feedback, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, feedback)){
			return form(feedback, model);
		}
		if(!feedback.getIsNewRecord()){//编辑表单保存
			Feedback t = feedbackService.get(feedback.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(feedback, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			feedbackService.save(t);//保存
		}else{//新增表单保存
			feedbackService.save(feedback);//保存
		}
		addMessage(redirectAttributes, "保存跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/manage/feedback/?repage";
	}
	/**
	 * 保存跨境电商反馈信息查询
	 */
	@RequestMapping(value = "return")
	public String back(Feedback feedback, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, feedback)){
			return form(feedback, model);
		}
		if(!feedback.getIsNewRecord()){//编辑表单保存
			Feedback t = feedbackService.get(feedback.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(feedback, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			feedbackService.save(t);//保存
		}else{//新增表单保存
			feedbackService.save(feedback);//保存
		}
		addMessage(redirectAttributes, "保存跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	/**
	 * 删除跨境电商反馈信息查询
	 */
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "delete")
	public String delete(Feedback feedback, RedirectAttributes redirectAttributes) {
		feedbackService.delete(feedback);
		addMessage(redirectAttributes, "删除跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	
	/**
	 * 批量删除跨境电商反馈信息查询
	 */
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			feedbackService.delete(feedbackService.get(id));
		}
		addMessage(redirectAttributes, "删除跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("manage:feedback:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(Feedback feedback, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "跨境电商反馈信息查询"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Feedback> page = feedbackService.findPage(new Page<Feedback>(request, response, -1), feedback);
    		new ExportExcel("跨境电商反馈信息查询", Feedback.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出跨境电商反馈信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/feedback/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Feedback> list = ei.getDataList(Feedback.class);
			for (Feedback feedback : list){
				try{
					feedbackService.save(feedback);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条跨境电商反馈信息查询记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条跨境电商反馈信息查询记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入跨境电商反馈信息查询失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
    }
	
	/**
	 * 下载导入跨境电商反馈信息查询数据模板
	 */
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "跨境电商反馈信息查询数据导入模板.xlsx";
    		List<Feedback> list = Lists.newArrayList(); 
    		new ExportExcel("跨境电商反馈信息查询数据", Feedback.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
    }

	/**
	 * 反馈次数统计（柱图）
	 */
	@RequiresPermissions("manage:feedback:statistics")
	@RequestMapping(value = "statistics1")
	public String statistics1(Feedback feedback, HttpServletRequest request, HttpServletResponse response, Model model) {

		Calendar cTmp = Calendar.getInstance();
		if(feedback.getBeginCreateDate() == null && feedback.getEndCreateDate() == null) {
			cTmp.set(Calendar.MONTH, Calendar.JANUARY);
			cTmp.set(Calendar.DATE, 1);
			feedback.setBeginCreateDate(cTmp.getTime());
			cTmp.set(Calendar.MONTH, Calendar.DECEMBER);
			cTmp.set(Calendar.DATE, 31);
			feedback.setEndCreateDate(cTmp.getTime());
		}

		//X轴的数据
		List<String> xAxisData = new ArrayList<>();
		//Y轴的数据
		Map<String, List<Double>> yAxisData = new HashMap<>();


		List<Feedback> list = feedbackService.findStatistics1(feedback);

		List<Double> yDatas = new ArrayList<>();

		for (Feedback data : list) {
			//X轴数据
			xAxisData.add(data.getCompanyName());
			//Y轴数据
			yDatas.add(data.getCounts());

		}

		//y轴数据
		yAxisData.put("反馈次数", yDatas);

		request.setAttribute("xAxisData", xAxisData);
		request.setAttribute("yAxisData", yAxisData);
		return "hcht/manage/feedback/statistic/FeedbackStatistics1";
	}

	/**
	 * 处置措施统计（饼图）
	 */
	@RequiresPermissions("manage:feedback:statistics")
	@RequestMapping(value = "czcsStatistic")
	public String czcsStatistic(FeedbackStatistic feedbackStatistic, Model model) {
		List<FeedbackStatistic> list = null;
		if(feedbackStatistic.getBeginDate() != null && feedbackStatistic.getEndDate() != null){
			list = feedbackService.findCzcsStatistics(feedbackStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/feedback/statistic/czcsStatistics";
	}

	/**
	 * 商品类别统计（饼图）
	 */
	@RequiresPermissions("manage:feedback:statistics")
	@RequestMapping(value = "categoryStatistic")
	public String categoryStatistic(FeedbackStatistic feedbackStatistic, Model model) {
		List<FeedbackStatistic> list = null;
		Page<FeedbackStatistic> page = new Page<>();
		page.setPageSize(10);
		feedbackStatistic.setPage(page);
		if(feedbackStatistic.getBeginDate() != null && feedbackStatistic.getEndDate() != null){
			list = feedbackService.findGoodsCategoryStatistics(feedbackStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/feedback/statistic/goodsCategoryStatistics";
	}

	/**
	 * 反馈位置统计（饼图）
	 */
	@RequiresPermissions("manage:feedback:statistics")
	@RequestMapping(value = "locationStatistic")
	public String locationStatistic(FeedbackStatistic feedbackStatistic, Model model) {
		List<FeedbackStatistic> list = null;
		if(feedbackStatistic.getBeginDate() != null && feedbackStatistic.getEndDate() != null){
			list = feedbackService.findGpsStatistics(feedbackStatistic);
		}
		model.addAttribute("dataList", JsonMapper.toJsonString(list));

		return "hcht/manage/feedback/statistic/locationStatistics";
	}
	
	
	

}