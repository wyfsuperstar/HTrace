/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.feedback;

import com.google.common.collect.Lists;
import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

import java.util.Date;
import java.util.List;


/**
 * 跨境电商反馈统计Entity
 *
 * @author gf
 * @version 2018-05-25
 */
public class FeedbackStatistic extends DataEntity<FeedbackStatistic> {

    private static final long serialVersionUID = 1L;
    private String itemName;        //项目名称
    private Double itemNum;        //项目数量
    private Double gpsx;        //百度坐标X
    private Double gpsy;        //百度坐标Y

    //统计查询条件
    private Date beginDate;        //起始时间
    private Date endDate;        //截止时间

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getItemNum() {
        return itemNum;
    }

    public void setItemNum(Double itemNum) {
        this.itemNum = itemNum;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getGpsx() {
        return gpsx;
    }

    public void setGpsx(Double gpsx) {
        this.gpsx = gpsx;
    }

    public Double getGpsy() {
        return gpsy;
    }

    public void setGpsy(Double gpsy) {
        this.gpsy = gpsy;
    }
}