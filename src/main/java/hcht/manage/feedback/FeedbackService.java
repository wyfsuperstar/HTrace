/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.feedback;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 跨境电商反馈信息查询Service
 * @author why
 * @version 2018-03-21
 */
@Service
@Transactional(readOnly = true)
public class FeedbackService extends CrudService<FeedbackDao, Feedback> {

//	private FeedbackPicDao feedbackPicDao;
//	private FeedbackPicService feedbackPicService;

	public Feedback get(String id) {
		Feedback feedback = super.get(id);
		List<FeedbackPic> list = dao.getPicInfo(id);
//		VhGoodDetails good = dao.getGoodInfo(feedback.getOrderGoodId());
		feedback.setFeedbackpicList(list);
//		feedback.setVhGoodDetails(good);
		return feedback;
	}
	
	public List<Feedback> findList(Feedback feedback) {
		return super.findList(feedback);
	}
	
	public Page<Feedback> findPage(Page<Feedback> page, Feedback feedback) {
		return super.findPage(page, feedback);
	}
	
	@Transactional(readOnly = false)
	public void save(Feedback feedback) {
		super.save(feedback);
	}
	
	@Transactional(readOnly = false)
	public void delete(Feedback feedback) {
		super.delete(feedback);
	}

	public List<Feedback> findStatistics1(Feedback feedback) {
		return dao.findStatistics1(feedback);
	}

	public List<FeedbackStatistic> findCzcsStatistics(FeedbackStatistic feedbackStatistic){
		return dao.findCzcsStatistics(feedbackStatistic);
	}

	public List<FeedbackStatistic> findGoodsCategoryStatistics(FeedbackStatistic feedbackStatistic){
		return dao.findGoodsCategoryStatistics(feedbackStatistic);
	}

	public List<FeedbackStatistic> findGpsStatistics(FeedbackStatistic feedbackStatistic){
		return dao.findGpsStatistics(feedbackStatistic);
	}
	
	
}