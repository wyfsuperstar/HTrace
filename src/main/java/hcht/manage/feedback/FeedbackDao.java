/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.feedback;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;


import java.util.List;

/**
 * 跨境电商反馈信息查询DAO接口
 * @author why
 * @version 2018-03-21
 */
@MyBatisDao
public interface FeedbackDao extends CrudDao<Feedback> {

    public VhGoodDetails getGoodInfo(String good_id);
    public List<FeedbackPic> getPicInfo(String feedback_id);

    List<Feedback> findStatistics1(Feedback feedback);

    List<FeedbackStatistic> findCzcsStatistics(FeedbackStatistic feedbackStatistic);
    List<FeedbackStatistic> findGoodsCategoryStatistics(FeedbackStatistic feedbackStatistic);
    List<FeedbackStatistic> findGpsStatistics(FeedbackStatistic feedbackStatistic);

}