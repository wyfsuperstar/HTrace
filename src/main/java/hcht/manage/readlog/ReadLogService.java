/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.readlog;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 溯源信息查询Service
 * @author gf
 * @version 2018-03-20
 */
@Service
@Transactional(readOnly = true)
public class ReadLogService extends CrudService<ReadLogDao, ReadLog> {

	public ReadLog get(String id) {
		return super.get(id);
	}
	
	public List<ReadLog> findList(ReadLog readLog) {
		return super.findList(readLog);
	}
	
	public Page<ReadLog> findPage(Page<ReadLog> page, ReadLog readLog) {
		return super.findPage(page, readLog);
	}
	
	@Transactional(readOnly = false)
	public void save(ReadLog readLog) {
		super.save(readLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(ReadLog readLog) {
		super.delete(readLog);
	}
	
	
	
	
}