/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.readlog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 溯源信息查询Controller
 * @author gf
 * @version 2018-03-20
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/readlog")
public class ReadLogController extends BaseController {

	@Autowired
	private ReadLogService readLogService;
	
	@ModelAttribute
	public ReadLog get(@RequestParam(required=false) String id) {
		ReadLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = readLogService.get(id);
		}
		if (entity == null){
			entity = new ReadLog();
		}
		return entity;
	}
	
	/**
	 * 溯源信息查询列表页面
	 */
	@RequiresPermissions("manage:readlog:list")
	@RequestMapping(value = {"list", ""})
	public String list(ReadLog readLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		if(readLog.getBeginCreateDate()==null||readLog.getBeginCreateDate().equals("")){
			return "hcht/manage/readlog/readLogList";
		}
		Page<ReadLog> page = readLogService.findPage(new Page<ReadLog>(request, response), readLog);
		model.addAttribute("page", page);
		return "hcht/manage/readlog/readLogList";
	}

	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("manage:readlog:export")
	@RequestMapping(value = "export", method= RequestMethod.POST)
	public String exportFile(ReadLog readLog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			String fileName = "溯源信息查询"+ DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
			Page<ReadLog> page = readLogService.findPage(new Page<ReadLog>(request, response, -1), readLog);
			new ExportExcel("溯源信息查询", ReadLog.class).setDataList(page.getList()).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出溯源信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+ Global.getAdminPath()+"/manage/readlog/?repage";
	}
	
	
	

}