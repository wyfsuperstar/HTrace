/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.tracelog;

import java.util.Date;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 溯源商品查询Entity
 *
 * @author gf
 * @version 2018-03-20
 */
public class TraceLog extends DataEntity<TraceLog> {

    private static final long serialVersionUID = 1L;
    private String idCard;        // 证件号
    private String consigneeTel;        // 电话号码
    private VhGoodsTrace vhGoodsTrace;        // 订单商品ID
    private String gpsx;        // GPS信息
    private String gpsy;        // GPS信息
    private String sourceType;        // 来源类型(01-iso,02-安卓,03-微信,04-网站)
    private Date beginCreateDate;        // 开始 日志时间
    private Date endCreateDate;        // 结束 日志时间
    private String useWay;
    /************统计用属性************/
    private String yearMonth;        //年月(X轴)
    private double counts;        //商品次数(Y轴)


    public TraceLog() {
        super();
    }

    public TraceLog(String id) {
        super(id);
    }

    @ExcelField(title = "证件号", align = 2, sort = 1)
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @ExcelField(title = "电话号码", align = 2, sort = 2)
    public String getConsigneeTel() {
        return consigneeTel;
    }

    public void setConsigneeTel(String consigneeTel) {
        this.consigneeTel = consigneeTel;
    }

    //	@ExcelField(title="订单商品ID", align=2, sort=3)
    public VhGoodsTrace getVhGoodsTrace() {
        return vhGoodsTrace;
    }

    public void setVhGoodsTrace(VhGoodsTrace vhGoodsTrace) {
        this.vhGoodsTrace = vhGoodsTrace;
    }

    @ExcelField(title = "商品名称", fieldType = VhGoodsTrace.class, value = "vhGoodsTrace.goodsName", align = 2, sort = 3)
    public VhGoodsTrace getGoodsName() {
        return vhGoodsTrace;
    }

    @ExcelField(title = "企业名称", fieldType = VhGoodsTrace.class, value = "vhGoodsTrace.cbeName", align = 2, sort = 4)
    public VhGoodsTrace getCbeName() {
        return vhGoodsTrace;
    }

    @ExcelField(title = "日志时间", align = 2, sort = 5)
    public Date getCreateDate() {
        return createDate;
    }


    //	@ExcelField(title="GPS信息", align=2, sort=4)
    public String getGpsx() {
        return gpsx;
    }

    public void setGpsx(String gpsx) {
        this.gpsx = gpsx;
    }

    //	@ExcelField(title="GPS信息", align=2, sort=5)
    public String getGpsy() {
        return gpsy;
    }

    public void setGpsy(String gpsy) {
        this.gpsy = gpsy;
    }

    @ExcelField(title = "来源类型", dictType = "source_type", align = 2, sort = 6)
    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public Date getBeginCreateDate() {
        return beginCreateDate;
    }

    public void setBeginCreateDate(Date beginCreateDate) {
        this.beginCreateDate = beginCreateDate;
    }

    public Date getEndCreateDate() {
        return endCreateDate;
    }

    public void setEndCreateDate(Date endCreateDate) {
        this.endCreateDate = endCreateDate;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    public double getCounts() {
        return counts;
    }

    public void setCounts(double counts) {
        this.counts = counts;
    }

    public String getUseWay() {
        return useWay;
    }

    public void setUseWay(String useWay) {
        this.useWay = useWay;
    }
}