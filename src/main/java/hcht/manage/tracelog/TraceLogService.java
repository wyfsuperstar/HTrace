/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.tracelog;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 溯源商品查询Service
 * @author gf
 * @version 2018-03-20
 */
@Service
@Transactional(readOnly = true)
public class TraceLogService extends CrudService<TraceLogDao, TraceLog> {

	public TraceLog get(String id) {
		return super.get(id);
	}
	
	public List<TraceLog> findList(TraceLog traceLog) {
		return super.findList(traceLog);
	}
	
	public Page<TraceLog> findPage(Page<TraceLog> page, TraceLog traceLog) {
		return super.findPage(page, traceLog);
	}
	
	@Transactional(readOnly = false)
	public void save(TraceLog traceLog) {
		super.save(traceLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(TraceLog traceLog) {
		super.delete(traceLog);
	}

	public List<TraceLog> findStatistics1(TraceLog traceLog) {
		return dao.findStatistics1(traceLog);
	}
	
	
	
	
}