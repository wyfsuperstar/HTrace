/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.tracelog;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.modules.echarts.entity.ChinaWeatherDataBean;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

/**
 * 溯源商品查询Controller
 *
 * @author gf
 * @version 2018-03-20
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/tracelog")
public class TraceLogController extends BaseController {

    @Autowired
    private TraceLogService traceLogService;

    @ModelAttribute
    public TraceLog get(@RequestParam(required = false) String id) {
        TraceLog entity = null;
        if (StringUtils.isNotBlank(id)) {
            entity = traceLogService.get(id);
        }
        if (entity == null) {
            entity = new TraceLog();
        }
        return entity;
    }

    /**
     * 溯源商品查询列表页面
     */
    @RequiresPermissions("manage:tracelog:list")
    @RequestMapping(value = {"list", ""})
    public String list(TraceLog traceLog, HttpServletRequest request, HttpServletResponse response, Model model) {
        if(traceLog.getBeginCreateDate()==null||traceLog.getBeginCreateDate().equals("")){
            return "hcht/manage/tracelog/traceLogList";
        }
        Page<TraceLog> page = traceLogService.findPage(new Page<TraceLog>(request, response), traceLog);
        model.addAttribute("page", page);
        return "hcht/manage/tracelog/traceLogList";
    }

    /**
     * 导出excel文件
     */
    @RequiresPermissions("manage:tracelog:export")
    @RequestMapping(value = "export", method = RequestMethod.POST)
    public String exportFile(TraceLog traceLog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        try {
            String fileName = "溯源商品查询" + DateUtils.getDate("yyyyMMddHHmmss") + ".xlsx";
            Page<TraceLog> page = traceLogService.findPage(new Page<TraceLog>(request, response, -1), traceLog);
            new ExportExcel("溯源商品查询", TraceLog.class).setDataList(page.getList()).write(response, fileName).dispose();
            return null;
        } catch (Exception e) {
            addMessage(redirectAttributes, "导出溯源商品查询记录失败！失败信息：" + e.getMessage());
        }
        return "redirect:" + Global.getAdminPath() + "/manage/tracelog/?repage";
    }

    /**
     * 商品次统计（折线图）
     */
    @RequiresPermissions("manage:tracelog:statistics")
    @RequestMapping(value = "statistics1")
    public String statistics1(TraceLog traceLog, HttpServletRequest request, HttpServletResponse response, Model model) {

        Calendar cTmp = Calendar.getInstance();
        if(traceLog.getBeginCreateDate() == null && traceLog.getEndCreateDate() == null) {
            cTmp.set(Calendar.MONTH, Calendar.JANUARY);
            traceLog.setBeginCreateDate(cTmp.getTime());
            cTmp.set(Calendar.MONTH, Calendar.DECEMBER);
            traceLog.setEndCreateDate(cTmp.getTime());
        }

        //X轴的数据
        List<String> xAxisData = new ArrayList<>();
        //Y轴的数据
        Map<String, List<Double>> yAxisData = new HashMap<>();


        List<TraceLog> list = traceLogService.findStatistics1(traceLog);

        List<Double> yDatas = new ArrayList<>();

        for (TraceLog data : list) {
            //X轴数据
            xAxisData.add(data.getYearMonth());
            //Y轴数据
            yDatas.add(data.getCounts());

        }

        //y轴数据
        yAxisData.put("商品次", yDatas);

        request.setAttribute("xAxisData", xAxisData);
        request.setAttribute("yAxisData", yAxisData);
        return "hcht/manage/tracelog/traceLogStatistics1";
    }


}