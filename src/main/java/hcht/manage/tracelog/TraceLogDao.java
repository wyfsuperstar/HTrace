/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.tracelog;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

import java.util.List;

/**
 * 溯源商品查询DAO接口
 * @author gf
 * @version 2018-03-20
 */
@MyBatisDao
public interface TraceLogDao extends CrudDao<TraceLog> {

    List<TraceLog> findStatistics1(TraceLog traceLog);
	
}