package hcht.manage.tracelog;

import com.jeeplus.common.persistence.DataEntity;

import java.util.Date;

/**
 * 订单商品视图 v_h_goods_trace
 * Created by gf on 2018/3/20.
 */
public class VhGoodsTrace extends DataEntity<VhGoodsTrace> {

    protected static final long serialVersionUID = 1L;
    protected String orderGoodId;     //商品ID
    protected String goodsName;       //商品名称
    protected String originCountry;       //原产国
    protected String spec;        //规格型号
    protected String useWay;      //用途
    protected String masterBase;      //主要成分
    protected String brand;       //产品品牌
    protected String proEnt;      //生产企业 生产商
    protected String cbeName;     //企业名称(销售企业)
    protected String cbeCode;     //企业代码
    protected String declDate;      //申请日期
    protected String declNo;      //申报单号
    protected String consignorCname;      //发货人名称
    protected String despCountryName;     //启运国 发货国家
    protected String despPortName;        //启运口岸
    protected String entryPortName;       //入境口岸 到岸港口
    protected String createTime;      //入库时间 到岸时间 入境时间
    protected String goodsAddress;        //仓库
    protected String relsDate;      //放行日期
    protected String transEnt;        //运输企业 物流企业
    protected String goodsUrl;        //产品图片地址
    protected String hscode;        //产品图片地址
    protected String url;        //产品图片地址
    protected String sitename;        //产品图片地址
    protected String importtypename;        //产品图片地址
    protected String registerAddress;        //产品图片地址

    public String getOrderGoodId() {
        return orderGoodId;
    }

    public void setOrderGoodId(String orderGoodId) {
        this.orderGoodId = orderGoodId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getUseWay() {
        return useWay;
    }

    public void setUseWay(String useWay) {
        this.useWay = useWay;
    }

    public String getMasterBase() {
        return masterBase;
    }

    public void setMasterBase(String masterBase) {
        this.masterBase = masterBase;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProEnt() {
        return proEnt;
    }

    public void setProEnt(String proEnt) {
        this.proEnt = proEnt;
    }

    public String getCbeName() {
        return cbeName;
    }

    public void setCbeName(String cbeName) {
        this.cbeName = cbeName;
    }

    public String getCbeCode() { return cbeCode; }

    public void setCbeCode(String cbeCode) { this.cbeCode = cbeCode; }

    public String getDeclDate() {
        return declDate;
    }

    public void setDeclDate(String declDate) { this.declDate = declDate; }

    public String getDeclNo() {
        return declNo;
    }

    public void setDeclNo(String declNo) {
        this.declNo = declNo;
    }

    public String getConsignorCname() {
        return consignorCname;
    }

    public void setConsignorCname(String consignorCname) {
        this.consignorCname = consignorCname;
    }

    public String getDespCountryName() {
        return despCountryName;
    }

    public void setDespCountryName(String despCountryName) {
        this.despCountryName = despCountryName;
    }

    public String getDespPortName() {
        return despPortName;
    }

    public void setDespPortName(String despPortName) {
        this.despPortName = despPortName;
    }

    public String getEntryPortName() {
        return entryPortName;
    }

    public void setEntryPortName(String entryPortName) {
        this.entryPortName = entryPortName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getGoodsAddress() {
        return goodsAddress;
    }

    public void setGoodsAddress(String goodsAddress) {
        this.goodsAddress = goodsAddress;
    }

    public String getRelsDate() {
        return relsDate;
    }

    public void setRelsDate(String relsDate) {
        this.relsDate = relsDate;
    }

    public String getTransEnt() {
        return transEnt;
    }

    public void setTransEnt(String transEnt) {
        this.transEnt = transEnt;
    }

    public String getGoodsUrl() {
        return goodsUrl;
    }

    public void setGoodsUrl(String goodsUrl) {
        this.goodsUrl = goodsUrl;
    }

    public String getHscode() {
        return hscode;
    }

    public void setHscode(String hscode) {
        this.hscode = hscode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSitename() {
        return sitename;
    }

    public String getImporttypename() {
        return importtypename;
    }

    public void setImporttypename(String importtypename) {
        this.importtypename = importtypename;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }
}
