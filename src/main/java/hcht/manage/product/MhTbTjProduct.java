/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.product;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 追溯主体管理Entity
 * @author BMW
 * @version 2018-05-08
 */
public class MhTbTjProduct extends DataEntity<MhTbTjProduct> {
	
	private static final long serialVersionUID = 1L;
	private String goodSerialNo;		// 商品流水号
	private String entCode;		// 企业备案号
	private String hscode;		// HS编码
	private String goodCategories;		// 商品大类
	private String iefLag;		// 进出口标志
	private Date applyTime;		// 导入时间
	private String goodSkuNo;		// 商品货号
	private String goodName;		// 商品名称
	private String masterBase;		// 主要成分
	private String spec;		// 规格型号
	private String originCountry;		// 原产国
	private String proEnt;		// 生产企业
	private String brand;		// 产品品牌
	private String supplier;		// 供应商
	private String qtyUnitCode;		// 包装单位代码
	private String smallQtyUnitCode;		// 最小包装单位代码
	private String useWay;		// 用途
	private String accordWith;		// 是否符合国家相关法规
	private Date rksj;		// 导入时间
	private String creater;		// creater
	private Long batchno;		// 批次号
	private String declEntCode;		// 申报单位代码
	private String goodCode;		// 商品备案编号
	private String msgCode;		// 检验检疫状态
	private String msgcodeoflock;		// 商品锁定状态 N未锁定 Y已锁定
	private String msgcodeofchange;		// 变更回执状态
	private String delflag;		// 删除标志，1-已删除
	private String ciqOrgTypeCode;		// 属地检验检疫机构代码
	private String msgName;		// 状态名称
	private String msgcodeoflockName;		// 锁定状态名称
	private Date secondAuditTime;		// 二级审核通过日期
	
	public MhTbTjProduct() {
		super();
	}

	public MhTbTjProduct(String id){
		super(id);
	}

	@ExcelField(title="商品流水号", align=2, sort=0)
	public String getGoodSerialNo() {
		return goodSerialNo;
	}

	public void setGoodSerialNo(String goodSerialNo) {
		this.goodSerialNo = goodSerialNo;
	}
	
	@ExcelField(title="企业备案号", align=2, sort=1)
	public String getEntCode() {
		return entCode;
	}

	public void setEntCode(String entCode) {
		this.entCode = entCode;
	}
	
	@ExcelField(title="HS编码", align=2, sort=2)
	public String getHscode() {
		return hscode;
	}

	public void setHscode(String hscode) {
		this.hscode = hscode;
	}
	
	@ExcelField(title="商品大类", align=2, sort=3)
	public String getGoodCategories() {
		return goodCategories;
	}

	public void setGoodCategories(String goodCategories) {
		this.goodCategories = goodCategories;
	}
	
	@ExcelField(title="进出口标志", align=2, sort=4)
	public String getIefLag() {
		return iefLag;
	}

	public void setIefLag(String iefLag) {
		this.iefLag = iefLag;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="导入时间", align=2, sort=5)
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	
	@ExcelField(title="商品货号", align=2, sort=6)
	public String getGoodSkuNo() {
		return goodSkuNo;
	}

	public void setGoodSkuNo(String goodSkuNo) {
		this.goodSkuNo = goodSkuNo;
	}
	
	@ExcelField(title="商品名称", align=2, sort=7)
	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	
	@ExcelField(title="主要成分", align=2, sort=8)
	public String getMasterBase() {
		return masterBase;
	}

	public void setMasterBase(String masterBase) {
		this.masterBase = masterBase;
	}
	
	@ExcelField(title="规格型号", align=2, sort=9)
	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}
	
	@ExcelField(title="原产国", align=2, sort=10)
	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	
	@ExcelField(title="生产企业", align=2, sort=11)
	public String getProEnt() {
		return proEnt;
	}

	public void setProEnt(String proEnt) {
		this.proEnt = proEnt;
	}
	
	@ExcelField(title="产品品牌", align=2, sort=12)
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@ExcelField(title="供应商", align=2, sort=13)
	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	@ExcelField(title="包装单位代码", align=2, sort=14)
	public String getQtyUnitCode() {
		return qtyUnitCode;
	}

	public void setQtyUnitCode(String qtyUnitCode) {
		this.qtyUnitCode = qtyUnitCode;
	}
	
	@ExcelField(title="最小包装单位代码", align=2, sort=15)
	public String getSmallQtyUnitCode() {
		return smallQtyUnitCode;
	}

	public void setSmallQtyUnitCode(String smallQtyUnitCode) {
		this.smallQtyUnitCode = smallQtyUnitCode;
	}
	
	@ExcelField(title="用途", align=2, sort=16)
	public String getUseWay() {
		return useWay;
	}

	public void setUseWay(String useWay) {
		this.useWay = useWay;
	}
	
	@ExcelField(title="是否符合国家相关法规", align=2, sort=17)
	public String getAccordWith() {
		return accordWith;
	}

	public void setAccordWith(String accordWith) {
		this.accordWith = accordWith;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="导入时间", align=2, sort=18)
	public Date getRksj() {
		return rksj;
	}

	public void setRksj(Date rksj) {
		this.rksj = rksj;
	}
	
	@ExcelField(title="creater", align=2, sort=19)
	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}
	
	@ExcelField(title="批次号", align=2, sort=20)
	public Long getBatchno() {
		return batchno;
	}

	public void setBatchno(Long batchno) {
		this.batchno = batchno;
	}
	
	@ExcelField(title="申报单位代码", align=2, sort=21)
	public String getDeclEntCode() {
		return declEntCode;
	}

	public void setDeclEntCode(String declEntCode) {
		this.declEntCode = declEntCode;
	}
	
	@ExcelField(title="商品备案编号", align=2, sort=22)
	public String getGoodCode() {
		return goodCode;
	}

	public void setGoodCode(String goodCode) {
		this.goodCode = goodCode;
	}
	
	@ExcelField(title="检验检疫状态", align=2, sort=23)
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	
	@ExcelField(title="商品锁定状态 N未锁定 Y已锁定", align=2, sort=25)
	public String getMsgcodeoflock() {
		return msgcodeoflock;
	}

	public void setMsgcodeoflock(String msgcodeoflock) {
		this.msgcodeoflock = msgcodeoflock;
	}
	
	@ExcelField(title="变更回执状态", align=2, sort=26)
	public String getMsgcodeofchange() {
		return msgcodeofchange;
	}

	public void setMsgcodeofchange(String msgcodeofchange) {
		this.msgcodeofchange = msgcodeofchange;
	}
	
	@ExcelField(title="删除标志，1-已删除", align=2, sort=27)
	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}
	
	@ExcelField(title="属地检验检疫机构代码", align=2, sort=28)
	public String getCiqOrgTypeCode() {
		return ciqOrgTypeCode;
	}

	public void setCiqOrgTypeCode(String ciqOrgTypeCode) {
		this.ciqOrgTypeCode = ciqOrgTypeCode;
	}
	
	@ExcelField(title="状态名称", align=2, sort=29)
	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}
	
	@ExcelField(title="锁定状态名称", align=2, sort=30)
	public String getMsgcodeoflockName() {
		return msgcodeoflockName;
	}

	public void setMsgcodeoflockName(String msgcodeoflockName) {
		this.msgcodeoflockName = msgcodeoflockName;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="二级审核通过日期", align=2, sort=31)
	public Date getSecondAuditTime() {
		return secondAuditTime;
	}

	public void setSecondAuditTime(Date secondAuditTime) {
		this.secondAuditTime = secondAuditTime;
	}
	
}