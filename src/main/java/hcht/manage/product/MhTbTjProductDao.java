/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.product;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 追溯主体管理DAO接口
 * @author BMW
 * @version 2018-05-08
 */
@MyBatisDao
public interface MhTbTjProductDao extends CrudDao<MhTbTjProduct> {

	
}