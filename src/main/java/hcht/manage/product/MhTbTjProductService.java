/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.product;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 追溯主体管理Service
 * @author BMW
 * @version 2018-05-08
 */
@Service
@Transactional(readOnly = true)
public class MhTbTjProductService extends CrudService<MhTbTjProductDao, MhTbTjProduct> {

	public MhTbTjProduct get(String id) {
		return super.get(id);
	}
	
	public List<MhTbTjProduct> findList(MhTbTjProduct mhTbTjProduct) {
		return super.findList(mhTbTjProduct);
	}
	
	public Page<MhTbTjProduct> findPage(Page<MhTbTjProduct> page, MhTbTjProduct mhTbTjProduct) {
		return super.findPage(page, mhTbTjProduct);
	}
	
	@Transactional(readOnly = false)
	public void save(MhTbTjProduct mhTbTjProduct) {
		super.save(mhTbTjProduct);
	}
	
	@Transactional(readOnly = false)
	public void delete(MhTbTjProduct mhTbTjProduct) {
		super.delete(mhTbTjProduct);
	}
	
	
	
	
}