/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 追溯主体管理Controller
 * @author BMW
 * @version 2018-05-08
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/product")
public class MhTbTjProductController extends BaseController {

	@Autowired
	private MhTbTjProductService mhTbTjProductService;
	
	@ModelAttribute
	public MhTbTjProduct get(@RequestParam(required=false) String id) {
		MhTbTjProduct entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mhTbTjProductService.get(id);
		}
		if (entity == null){
			entity = new MhTbTjProduct();
		}
		return entity;
	}
	
	/**
	 * 追溯主体管理列表页面
	 */
//	@RequiresPermissions("product:mhTbTjProduct:list")
	@RequestMapping(value = {"list", ""})
	public String list(MhTbTjProduct mhTbTjProduct, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MhTbTjProduct> page = mhTbTjProductService.findPage(new Page<MhTbTjProduct>(request, response), mhTbTjProduct); 
		model.addAttribute("page", page);
		return "hcht/manage/product/mhTbTjProductList";
	}

	/**
	 * 查看，增加，编辑追溯主体管理表单页面
	 */
//	@RequiresPermissions(value={"product:mhTbTjProduct:view","product:mhTbTjProduct:add","product:mhTbTjProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MhTbTjProduct mhTbTjProduct, Model model) {
		model.addAttribute("mhTbTjProduct", mhTbTjProduct);
		return "hcht/manage/product/mhTbTjProductForm";
	}

	/**
	 * 保存追溯主体管理
	 */
//	@RequiresPermissions(value={"product:mhTbTjProduct:add","product:mhTbTjProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MhTbTjProduct mhTbTjProduct, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, mhTbTjProduct)){
			return form(mhTbTjProduct, model);
		}
		if(!mhTbTjProduct.getIsNewRecord()){//编辑表单保存
			MhTbTjProduct t = mhTbTjProductService.get(mhTbTjProduct.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(mhTbTjProduct, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			mhTbTjProductService.save(t);//保存
		}else{//新增表单保存
			mhTbTjProductService.save(mhTbTjProduct);//保存
		}
		addMessage(redirectAttributes, "保存追溯主体管理成功");
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
	}
	
	/**
	 * 删除追溯主体管理
	 */
//	@RequiresPermissions("product:mhTbTjProduct:del")
	@RequestMapping(value = "delete")
	public String delete(MhTbTjProduct mhTbTjProduct, RedirectAttributes redirectAttributes) {
		mhTbTjProductService.delete(mhTbTjProduct);
		addMessage(redirectAttributes, "删除追溯主体管理成功");
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
	}
	
	/**
	 * 批量删除追溯主体管理
	 */
//	@RequiresPermissions("product:mhTbTjProduct:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			mhTbTjProductService.delete(mhTbTjProductService.get(id));
		}
		addMessage(redirectAttributes, "删除追溯主体管理成功");
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
//	@RequiresPermissions("product:mhTbTjProduct:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MhTbTjProduct mhTbTjProduct, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "追溯主体管理"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MhTbTjProduct> page = mhTbTjProductService.findPage(new Page<MhTbTjProduct>(request, response, -1), mhTbTjProduct);
    		new ExportExcel("追溯主体管理", MhTbTjProduct.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出追溯主体管理记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
    }

	/**
	 * 导入Excel数据

	 */
//	@RequiresPermissions("product:mhTbTjProduct:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MhTbTjProduct> list = ei.getDataList(MhTbTjProduct.class);
			for (MhTbTjProduct mhTbTjProduct : list){
				try{
					mhTbTjProductService.save(mhTbTjProduct);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条追溯主体管理记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条追溯主体管理记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入追溯主体管理失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
    }
	
	/**
	 * 下载导入追溯主体管理数据模板
	 */
//	@RequiresPermissions("product:mhTbTjProduct:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "追溯主体管理数据导入模板.xlsx";
    		List<MhTbTjProduct> list = Lists.newArrayList(); 
    		new ExportExcel("追溯主体管理数据", MhTbTjProduct.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/product/?repage";
    }
	
	
	

}