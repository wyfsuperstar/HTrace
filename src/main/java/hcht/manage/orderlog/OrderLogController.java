/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.orderlog;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 跨境电商订单读取日志Controller
 * @author why
 * @version 2018-03-29
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/orderlog")
public class OrderLogController extends BaseController {

	@Autowired
	private OrderLogService orderLogService;
	
	@ModelAttribute
	public OrderLog get(@RequestParam(required=false) String id) {
		OrderLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = orderLogService.get(id);
		}
		if (entity == null){
			entity = new OrderLog();
		}
		return entity;
	}
	
	/**
	 * 跨境电商订单读取日志列表页面
	 */
	@RequiresPermissions("manage:orderlog:list")
	@RequestMapping(value = {"list", ""})
	public String list(OrderLog orderLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		if(orderLog.getBeginCreateDate()==null||orderLog.getBeginCreateDate().equals("")){
			return "hcht/manage/orderlog/orderLogList";
		}
		Page<OrderLog> page = orderLogService.findPage(new Page<OrderLog>(request, response), orderLog);
		model.addAttribute("page", page);
		return "hcht/manage/orderlog/orderLogList";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("manage:orderlog:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(OrderLog orderLog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "跨境电商订单读取日志"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<OrderLog> page = orderLogService.findPage(new Page<OrderLog>(request, response, -1), orderLog);
    		new ExportExcel("跨境电商订单读取日志", OrderLog.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出跨境电商订单读取日志记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/manage/orderlog/?repage";
    }


}