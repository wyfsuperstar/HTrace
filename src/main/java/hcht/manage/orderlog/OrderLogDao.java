/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.orderlog;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 跨境电商订单读取日志DAO接口
 * @author why
 * @version 2018-03-29
 */
@MyBatisDao
public interface OrderLogDao extends CrudDao<OrderLog> {

	
}