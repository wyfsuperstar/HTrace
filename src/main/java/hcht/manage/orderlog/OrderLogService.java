/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.orderlog;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 跨境电商订单读取日志Service
 * @author why
 * @version 2018-03-29
 */
@Service
@Transactional(readOnly = true)
public class OrderLogService extends CrudService<OrderLogDao, OrderLog> {

	public OrderLog get(String id) {
		return super.get(id);
	}
	
	public List<OrderLog> findList(OrderLog orderLog) {
		return super.findList(orderLog);
	}
	
	public Page<OrderLog> findPage(Page<OrderLog> page, OrderLog orderLog) {
		return super.findPage(page, orderLog);
	}
	
	@Transactional(readOnly = false)
	public void save(OrderLog orderLog) {
		super.save(orderLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(OrderLog orderLog) {
		super.delete(orderLog);
	}
	
	
	
	
}