/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.orderlog;

import java.util.Date;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 跨境电商订单读取日志Entity
 * @author why
 * @version 2018-03-29
 */
public class OrderLog extends DataEntity<OrderLog> {
	
	private static final long serialVersionUID = 1L;
	private String orderSerialNo;		// 订单流水号
	private String gpsx;		// GPS信息
	private String gpsy;		// GPS信息
	private String sourceType;		// 来源类型(01-iso,02-安卓,03-微信,04-网站)
	private Date beginCreateDate;		// 开始 日志时间
	private Date endCreateDate;		// 结束 日志时间
	
	public OrderLog() {
		super();
	}

	public OrderLog(String id){
		super(id);
	}

	@ExcelField(title="订单流水号", align=2, sort=1)
	public String getOrderSerialNo() {
		return orderSerialNo;
	}

	public void setOrderSerialNo(String orderSerialNo) {
		this.orderSerialNo = orderSerialNo;
	}
	
	@ExcelField(title="GPS信息", align=2, sort=2)
	public String getGpsx() {
		return gpsx;
	}

	public void setGpsx(String gpsx) {
		this.gpsx = gpsx;
	}
	
	@ExcelField(title="GPS信息", align=2, sort=3)
	public String getGpsy() {
		return gpsy;
	}

	public void setGpsy(String gpsy) {
		this.gpsy = gpsy;
	}
	
	@ExcelField(title="来源类型(01-iso,02-安卓,03-微信,04-网站)", align=2, sort=5)
	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}
	
	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}
		
}