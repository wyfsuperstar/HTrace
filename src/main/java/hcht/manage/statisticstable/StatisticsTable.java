/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.statisticstable;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

import java.util.Date;

/**
 * 溯源统计表Entity
 * @author bmw
 * @version 2018-10-08
 */
public class StatisticsTable extends DataEntity<StatisticsTable> {

	private static final long serialVersionUID = 1L;
	private String thisvalue;		// 当月值
	private String lastvalue;		// 上月值
	private String lastyearvalue;		// 同期值
	private String total;		// 累计值
	private String lastpre;		// 环比
	private String lastyearpre;		// 同比
	private String name;		// 项目
	private String beginDate;		// 开始日期
	private String endDate;		// 结束日期
	private String statisticsType;		// 统计类型
	private Date date;


	public StatisticsTable() {
		super();
	}

	public StatisticsTable(String id){
		super(id);
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@ExcelField(title="项目", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ExcelField(title="本期", align=2, sort=2)
	public String getThisvalue() {
		return thisvalue;
	}

	public void setThisvalue(String thisvalue) {
		this.thisvalue = thisvalue;
	}

	@ExcelField(title="上月", align=2, sort=3)
	public String getLastvalue() {
		return lastvalue;
	}

	public void setLastvalue(String lastvalue) {
		this.lastvalue = lastvalue;
	}
	@ExcelField(title="环比%", align=2, sort=4)
	public String getLastpre() {
		return lastpre;
	}

	public void setLastpre(String lastpre) {
		this.lastpre = lastpre;
	}


	@ExcelField(title="同期", align=2, sort=5)
	public String getLastyearvalue() {
		return lastyearvalue;
	}

	public void setLastyearvalue(String lastyearvalue) {
		this.lastyearvalue = lastyearvalue;
	}
	@ExcelField(title="同比%", align=2, sort=6)
	public String getLastyearpre() {
		return lastyearpre;
	}

	public void setLastyearpre(String lastyearpre) {
		this.lastyearpre = lastyearpre;
	}

//	@ExcelField(title="当年累计", align=2, sort=1)
	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStatisticsType() {
		return statisticsType;
	}

	public void setStatisticsType(String statisticsType) {
		this.statisticsType = statisticsType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}