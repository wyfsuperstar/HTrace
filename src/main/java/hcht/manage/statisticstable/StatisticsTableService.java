/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.statisticstable;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import hcht.manage.readlog.ReadLog;
import hcht.manage.readlog.ReadLogDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 溯源统计表Service
 * @author bmw
 * @version 2018-10-8
 */
@Service
@Transactional(readOnly = true)
public class StatisticsTableService extends CrudService<StatisticsTableDao, StatisticsTable> {

	public StatisticsTable get(String id) {
		return super.get(id);
	}

	public List<StatisticsTable> findOrderGoodsList(StatisticsTable statisticsTable) {
		return dao.findOrderGoodsList(statisticsTable);
	}
	public List<StatisticsTable> findTraceGoodsList(StatisticsTable statisticsTable) {
		return dao.findTraceGoodsList(statisticsTable);
	}
	public List<StatisticsTable> findFeedbackGoodsList(StatisticsTable statisticsTable) {
		return dao.findFeedbackGoodsList(statisticsTable);
	}


	
	
}