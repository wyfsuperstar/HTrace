/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.statisticstable;

import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.web.BaseController;
import hcht.manage.readlog.ReadLog;
import hcht.manage.readlog.ReadLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 溯源统计表Controller
 * @author bmw
 * @version 2018-10-08
 */
@Controller
@RequestMapping(value = "${adminPath}/manage/statisticstable")
public class StatisticsTableController extends BaseController {

	@Autowired
	private StatisticsTableService statisticsTableService;
	
	@ModelAttribute
	public StatisticsTable get(@RequestParam(required=false) String id) {
		StatisticsTable entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = statisticsTableService.get(id);
		}
		if (entity == null){
			entity = new StatisticsTable();
		}
		return entity;
	}
	
	/**
	 * 订单商品统计页面
	 */
	@RequestMapping(value = {"ordergoodslist"})
	public String ordergoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, Model model) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
			statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
		}
		statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
		statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(), 1)));

		if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
			statisticsTable.setStatisticsType("01");
		}
		List<StatisticsTable> list = statisticsTableService.findOrderGoodsList(statisticsTable);
		model.addAttribute("list", list);
		model.addAttribute("statisticsTable",statisticsTable);
		return "hcht/manage/statisticstable/ordergoodslist";
	}

	/**
	 * 订单商品统计导出excel文件
	 */
	@RequestMapping(value = "exportordergoodslist", method= RequestMethod.POST)
	public String exportordergoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
				statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
			}
			statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
			statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(),1)));
			String fileName = "订单商品统计"+sdf.format(statisticsTable.getDate())+".xlsx";

			if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
				statisticsTable.setStatisticsType("01");
			}
			List<StatisticsTable> list = statisticsTableService.findOrderGoodsList(statisticsTable);
			new ExportExcel("订单商品统计", StatisticsTable.class).setDataList(list).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出溯源信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+ Global.getAdminPath()+"/manage/statisticstable/ordergoodslist/?repage";
	}

	/**
	 * 溯源商品统计页面
	 */
	@RequestMapping(value = {"tracegoodslist"})
	public String tracegoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, Model model) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
			statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
		}
		statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
		statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(), 1)));

		if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
			statisticsTable.setStatisticsType("01");
		}
		List<StatisticsTable> list = statisticsTableService.findTraceGoodsList(statisticsTable);
		model.addAttribute("list", list);
		model.addAttribute("statisticsTable",statisticsTable);
		return "hcht/manage/statisticstable/tracegoodslist";
	}

	/**
	 * 溯源商品统计导出excel文件
	 */
	@RequestMapping(value = "exporttracegoodslist", method= RequestMethod.POST)
	public String exporttracegoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
				statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
			}
			statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
			statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(),1)));
			String fileName = "追溯商品统计"+sdf.format(statisticsTable.getDate())+".xlsx";

			if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
				statisticsTable.setStatisticsType("01");
			}
			List<StatisticsTable> list = statisticsTableService.findTraceGoodsList(statisticsTable);
			new ExportExcel("追溯商品统计", StatisticsTable.class).setDataList(list).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出溯源信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+ Global.getAdminPath()+"/manage/statisticstable/tracegoodslist/?repage";
	}

	/**
	 * 反馈商品统计页面
	 */
	@RequestMapping(value = {"feedbackgoodslist"})
	public String feedbackgoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, Model model) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
			statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
		}
		statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
		statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(), 1)));

		if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
			statisticsTable.setStatisticsType("01");
		}
		List<StatisticsTable> list = statisticsTableService.findFeedbackGoodsList(statisticsTable);
		model.addAttribute("list", list);
		model.addAttribute("statisticsTable",statisticsTable);
		return "hcht/manage/statisticstable/feedbackgoodslist";
	}

	/**
	 * 溯源商品统计导出excel文件
	 */
	@RequestMapping(value = "exportfeedbackgoodslist", method= RequestMethod.POST)
	public String exportfeedbackgoodslist(StatisticsTable statisticsTable, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(statisticsTable.getDate() == null || statisticsTable.getDate().equals("")){
//			Date date = sdf.parse("2008-08-08 12:10:12");
				statisticsTable.setDate(sdf.parse(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-01"));
			}
			statisticsTable.setBeginDate(sdf.format(statisticsTable.getDate()));
			statisticsTable.setEndDate(sdf.format(DateUtils.addMonths(statisticsTable.getDate(),1)));
			String fileName = "反馈商品统计"+sdf.format(statisticsTable.getDate())+".xlsx";

			if(statisticsTable.getStatisticsType() == null || statisticsTable.getStatisticsType().equals("")){
				statisticsTable.setStatisticsType("01");
			}
			List<StatisticsTable> list = statisticsTableService.findFeedbackGoodsList(statisticsTable);
			new ExportExcel("反馈商品统计", StatisticsTable.class).setDataList(list).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出溯源信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+ Global.getAdminPath()+"/manage/statisticstable/feedbackgoodslist/?repage";
	}


}