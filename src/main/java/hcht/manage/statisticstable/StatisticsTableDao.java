/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.manage.statisticstable;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.manage.readlog.ReadLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 溯源统计表DAO
 * @author bmw
 * @version 2018-10-8
 */
@MyBatisDao
public interface StatisticsTableDao extends CrudDao<StatisticsTable> {
    public List<StatisticsTable> findOrderGoodsList(StatisticsTable statisticsTable);
    public List<StatisticsTable> findTraceGoodsList(StatisticsTable statisticsTable);
    public List<StatisticsTable> findFeedbackGoodsList(StatisticsTable statisticsTable);

}