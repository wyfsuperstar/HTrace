/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.feedback;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;


import java.util.List;

/**
 * 跨境电商反馈信息查询DAO接口
 * @author why
 * @version 2018-03-21
 */
@MyBatisDao
public interface EnterpriseFeedbackDao extends CrudDao<EnterpriseFeedback> {

    public EnterpriseVhGoodDetails getGoodInfo(String good_id);
    public List<EnterpriseFeedbackPic> getPicInfo(String feedback_id);

}