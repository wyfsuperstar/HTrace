/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.feedback;


import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 跨境电商反馈图片表Entity
 * @author why
 * @version 2018-03-21
 */
public class EnterpriseFeedbackPic extends DataEntity<EnterpriseFeedbackPic> {
	
	private static final long serialVersionUID = 1L;
	private Integer seq;		// 图片序号
	private String feedbackId;		// 反馈ID
	private String picUrl;		// 图片
	
	public EnterpriseFeedbackPic() {
		super();
	}

	public EnterpriseFeedbackPic(String id){
		super(id);
	}

	@ExcelField(title="图片序号", align=2, sort=1)
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	
	@ExcelField(title="反馈ID", align=2, sort=2)
	public String getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(String feedbackId) {
		this.feedbackId = feedbackId;
	}
	
	@ExcelField(title="图片", align=2, sort=3)
	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	
}