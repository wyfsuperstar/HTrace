/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.feedback;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;
import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;
import com.jeeplus.modules.sys.entity.User;


/**
 * 跨境电商反馈信息查询Entity
 * @author why
 * @version 2018-03-21
 */
public class EnterpriseFeedback extends DataEntity<EnterpriseFeedback> {
	
	private static final long serialVersionUID = 1L;
	private String idCard;		// 证件号
	private String consigneeTel;		// 电话号码
	private String orderGoodId;		// 订单商品ID
	private String gpsx;		// GPS信息
	private String gpsy;		// GPS信息
	private String sourceType;		// 来源类型(01-iso,02-安卓,03-微信,04-网站)
	private String title;		// 反馈标题
	private String desc;		// 反馈描述
	private Date beginCreateDate;		// 开始 反馈时间
	private Date endCreateDate;		// 结束 反馈时间

	private List<EnterpriseFeedbackPic> feedbackpicList = Lists.newArrayList();        // 反馈图片表列表
	private EnterpriseVhGoodDetails vhGoodDetails;		// 商品详情

	public EnterpriseFeedback() {
		super();
	}

	public EnterpriseFeedback(String id){
		super(id);
	}

	@ExcelField(title="证件号", align=2, sort=1)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	@ExcelField(title="电话号码", align=2, sort=2)
	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}
	
	@ExcelField(title="订单商品ID", align=2, sort=3)
	public String getOrderGoodId() {
		return orderGoodId;
	}

	public void setOrderGoodId(String orderGoodId) {
		this.orderGoodId = orderGoodId;
	}
	
	@ExcelField(title="GPS信息", align=2, sort=4)
	public String getGpsx() {
		return gpsx;
	}

	public void setGpsx(String gpsx) {
		this.gpsx = gpsx;
	}
	
	@ExcelField(title="GPS信息", align=2, sort=5)
	public String getGpsy() {
		return gpsy;
	}

	public void setGpsy(String gpsy) {
		this.gpsy = gpsy;
	}
	
	@ExcelField(title="来源类型(01-iso,02-安卓,03-微信,04-网站)", dictType="source_type", align=2, sort=6)
	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	@ExcelField(title="反馈标题", align=2, sort=8)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="反馈描述", align=2, sort=9)
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}
	
	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}


	public List<EnterpriseFeedbackPic> getFeedbackpicList() {
		return feedbackpicList;
	}

	public void setFeedbackpicList(List<EnterpriseFeedbackPic> feedbackpicList) {
		this.feedbackpicList = feedbackpicList;
	}

	public EnterpriseVhGoodDetails getVhGoodDetails() {
		return vhGoodDetails;
	}

	public void setVhGoodDetails(EnterpriseVhGoodDetails vhGoodDetails) {
		this.vhGoodDetails = vhGoodDetails;
	}

}