/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.feedback;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 跨境电商反馈信息查询Controller
 * @author why
 * @version 2018-03-21
 */
@Controller
@RequestMapping(value = "${adminPath}/enterprise/feedback")
public class EnterpriseFeedbackController extends BaseController {

	@Autowired
	private EnterpriseFeedbackService feedbackService;
	
	@ModelAttribute
	public EnterpriseFeedback get(@RequestParam(required=false) String id) {
		EnterpriseFeedback entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = feedbackService.get(id);
		}
		if (entity == null){
			entity = new EnterpriseFeedback();
		}
		return entity;
	}
	
	/**
	 * 跨境电商反馈信息查询列表页面
	 */
	@RequiresPermissions("enterprise:feedback:list")
	@RequestMapping(value = {"list", ""})
	public String list(EnterpriseFeedback feedback, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<EnterpriseFeedback> page = feedbackService.findPage(new Page<EnterpriseFeedback>(request, response), feedback);
		model.addAttribute("page", page);
		return "hcht/enterprise/feedback/enterpriseFeedbackList";
	}

	/**
	 * 查看，增加，编辑跨境电商反馈信息查询表单页面
	 */
	@RequiresPermissions(value={"enterprise:feedback:view","feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(EnterpriseFeedback feedback, Model model) {
		model.addAttribute("enterpriseFeedback", feedback);
		return "hcht/enterprise/feedback/enterpriseFeedbackForm";
	}

	/**
	 * 保存跨境电商反馈信息查询
	 */
	@RequiresPermissions(value={"feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(EnterpriseFeedback feedback, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, feedback)){
			return form(feedback, model);
		}
		if(!feedback.getIsNewRecord()){//编辑表单保存
			EnterpriseFeedback t = feedbackService.get(feedback.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(feedback, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			feedbackService.save(t);//保存
		}else{//新增表单保存
			feedbackService.save(feedback);//保存
		}
		addMessage(redirectAttributes, "保存跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	
	/**
	 * 删除跨境电商反馈信息查询
	 */
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "delete")
	public String delete(EnterpriseFeedback feedback, RedirectAttributes redirectAttributes) {
		feedbackService.delete(feedback);
		addMessage(redirectAttributes, "删除跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	
	/**
	 * 批量删除跨境电商反馈信息查询
	 */
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			feedbackService.delete(feedbackService.get(id));
		}
		addMessage(redirectAttributes, "删除跨境电商反馈信息查询成功");
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("enterprise:feedback:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(EnterpriseFeedback feedback, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "跨境电商反馈信息查询"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<EnterpriseFeedback> page = feedbackService.findPage(new Page<EnterpriseFeedback>(request, response, -1), feedback);
    		new ExportExcel("跨境电商反馈信息查询", EnterpriseFeedback.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出跨境电商反馈信息查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/enterprise/feedback/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<EnterpriseFeedback> list = ei.getDataList(EnterpriseFeedback.class);
			for (EnterpriseFeedback feedback : list){
				try{
					feedbackService.save(feedback);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条跨境电商反馈信息查询记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条跨境电商反馈信息查询记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入跨境电商反馈信息查询失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
    }
	
	/**
	 * 下载导入跨境电商反馈信息查询数据模板
	 */
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "跨境电商反馈信息查询数据导入模板.xlsx";
    		List<EnterpriseFeedback> list = Lists.newArrayList();
    		new ExportExcel("跨境电商反馈信息查询数据", EnterpriseFeedback.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/feedback/feedback/?repage";
    }
	
	
	

}