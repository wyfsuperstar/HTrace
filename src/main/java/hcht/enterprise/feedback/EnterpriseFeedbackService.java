/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.feedback;

import java.util.List;

import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 跨境电商反馈信息查询Service
 * @author why
 * @version 2018-03-21
 */
@Service
@Transactional(readOnly = true)
public class EnterpriseFeedbackService extends CrudService<EnterpriseFeedbackDao, EnterpriseFeedback> {

	public EnterpriseFeedback get(String id) {
		EnterpriseFeedback feedback = super.get(id);
		List<EnterpriseFeedbackPic> list = dao.getPicInfo(id);
		feedback.setFeedbackpicList(list);
		return feedback;
	}
	
	public List<EnterpriseFeedback> findList(EnterpriseFeedback feedback) {
		return super.findList(feedback);
	}
	
	public Page<EnterpriseFeedback> findPage(Page<EnterpriseFeedback> page, EnterpriseFeedback feedback) {
		feedback.setCreateBy(UserUtils.getUser());
		return super.findPage(page, feedback);
	}
	
	@Transactional(readOnly = false)
	public void save(EnterpriseFeedback feedback) {
		super.save(feedback);
	}
	
	@Transactional(readOnly = false)
	public void delete(EnterpriseFeedback feedback) {
		super.delete(feedback);
	}
	
	
	
	
}