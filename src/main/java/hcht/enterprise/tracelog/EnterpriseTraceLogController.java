/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.tracelog;


import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 溯源商品查询Controller
 * @author gf
 * @version 2018-03-20
 */
@Controller
@RequestMapping(value = "${adminPath}/enterprise/tracelog")
public class EnterpriseTraceLogController extends BaseController {

	@Autowired
	private EnterpriseTraceLogService traceLogService;
	
	@ModelAttribute
	public EnterpriseTraceLog get(@RequestParam(required=false) String id) {
		EnterpriseTraceLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = traceLogService.get(id);
		}
		if (entity == null){
			entity = new EnterpriseTraceLog();
		}
		return entity;
	}
	
	/**
	 * 溯源商品查询列表页面
	 */
	@RequiresPermissions("enterprise:tracelog:list")
	@RequestMapping(value = {"list", ""})
	public String list(EnterpriseTraceLog traceLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		traceLog.setCreateBy(UserUtils.getUser());
		Page<EnterpriseTraceLog> page = traceLogService.findPage(new Page<EnterpriseTraceLog>(request, response), traceLog);
		model.addAttribute("page", page);
		return "hcht/enterprise/tracelog/enterpriseTraceLogList";
	}

	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("enterprise:tracelog:export")
	@RequestMapping(value = "export", method= RequestMethod.POST)
	public String exportFile(EnterpriseTraceLog traceLog, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			String fileName = "溯源商品查询"+ DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
			Page<EnterpriseTraceLog> page = traceLogService.findPage(new Page<EnterpriseTraceLog>(request, response, -1), traceLog);
			new ExportExcel("溯源商品查询", EnterpriseTraceLog.class).setDataList(page.getList()).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出溯源商品查询记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+ Global.getAdminPath()+"/enterprise/tracelog/?repage";
	}
	
	
	

}