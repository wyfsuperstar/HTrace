/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.tracelog;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 溯源商品查询Service
 * @author gf
 * @version 2018-03-20
 */
@Service
@Transactional(readOnly = true)
public class EnterpriseTraceLogService extends CrudService<EnterpriseTraceLogDao, EnterpriseTraceLog> {

	public EnterpriseTraceLog get(String id) {
		return super.get(id);
	}

	public List<EnterpriseTraceLog> findList(EnterpriseTraceLog traceLog) {
		return super.findList(traceLog);
	}

	public Page<EnterpriseTraceLog> findPage(Page<EnterpriseTraceLog> page, EnterpriseTraceLog traceLog) {
		return super.findPage(page, traceLog);
	}

	@Transactional(readOnly = false)
	public void save(EnterpriseTraceLog traceLog) {
		super.save(traceLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(EnterpriseTraceLog traceLog) {
		super.delete(traceLog);
	}
	
	
	
	
}