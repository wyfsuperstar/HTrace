/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.enterprise.tracelog;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;

/**
 * 溯源商品查询DAO接口
 * @author gf
 * @version 2018-03-20
 */
@MyBatisDao
public interface EnterpriseTraceLogDao extends CrudDao<EnterpriseTraceLog> {

	
}