package hcht.web.goods;

import com.jeeplus.common.service.CrudService;
import hcht.manage.tracelog.VhGoodsTrace;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 商品订单Service
 * Created by gf on 2018/3/23.
 */
@Service
@Transactional(readOnly = true)
public class GoodsOrderService extends CrudService<GoodsOrderDao, GoodsOrder> {

    public List<VhCbe> findCbes(){
        return dao.findCbes();
    }

}
