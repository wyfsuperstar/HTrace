package hcht.web.goods;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 企业信息
 * Created by gf on 2018/3/29.
 */
public class VhCbe extends DataEntity<VhCbe> {

    private static final long serialVersionUID = 1L;
    private String cbeCode;     //企业代码
    private String cbeName;     //企业名称

    public String getCbeCode() {
        return cbeCode;
    }

    public void setCbeCode(String cbeCode) {
        this.cbeCode = cbeCode;
    }

    public String getCbeName() {
        return cbeName;
    }

    public void setCbeName(String cbeName) {
        this.cbeName = cbeName;
    }
}
