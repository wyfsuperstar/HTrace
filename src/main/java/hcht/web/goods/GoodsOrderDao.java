package hcht.web.goods;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import hcht.manage.tracelog.VhGoodsTrace;

import java.util.List;

/**
 * 商品订单Dao
 * Created by gf on 2018/3/23.
 */
@MyBatisDao
public interface GoodsOrderDao extends CrudDao<GoodsOrder> {

    List<VhCbe> findCbes();
}
