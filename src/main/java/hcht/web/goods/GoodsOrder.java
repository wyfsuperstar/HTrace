package hcht.web.goods;

import com.jeeplus.common.persistence.DataEntity;
import hcht.manage.tracelog.VhGoodsTrace;

import java.beans.Transient;
import java.util.Date;

/**
 * 商品订单信息
 * Created by gf on 2018/3/23.
 */
public class GoodsOrder extends DataEntity<GoodsOrder> {

    private static final long serialVersionUID = 1L;
    private String idCard;      //证件号
    private String consigneeTel;        //联系电话
    private String goodsName;       //商品名称
    private Date orderDate;     //申报时间
    private VhGoodsTrace vhGoodsTrace;      //订单商品
    private String brand;       //产品品牌
    private String masterBase;      //主要成分
    private String originCountry;       //原产国
    private String orderSerialNo;       //订单流水号ORDER_SERIAL_NO

    //区分首页
    private String fromPage = "index";      //页面来源
    //首页查询用字段
    private String cbeCode;     //企业代码
    private String orderNo;     //订单号


    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getConsigneeTel() {
        return consigneeTel;
    }

    public void setConsigneeTel(String consigneeTel) {
        this.consigneeTel = consigneeTel;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public VhGoodsTrace getVhGoodsTrace() {
        return vhGoodsTrace;
    }

    public void setVhGoodsTrace(VhGoodsTrace vhGoodsTrace) {
        this.vhGoodsTrace = vhGoodsTrace;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMasterBase() {
        return masterBase;
    }

    public void setMasterBase(String masterBase) {
        this.masterBase = masterBase;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getOrderSerialNo() {
        return orderSerialNo;
    }

    public void setOrderSerialNo(String orderSerialNo) {
        this.orderSerialNo = orderSerialNo;
    }

    @Transient
    public String getCbeCode() {
        return cbeCode;
    }

    public void setCbeCode(String cbeCode) {
        this.cbeCode = cbeCode;
    }

    @Transient
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getFromPage() {
        return fromPage;
    }

    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }
}
