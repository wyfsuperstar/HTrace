/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.web.feedback;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;


import java.util.List;

/**
 * 跨境电商反馈信息查询DAO接口
 * @author why
 * @version 2018-03-21
 */
@MyBatisDao
public interface WebFeedbackDao extends CrudDao<WebFeedback> {

    public WebVhGoodDetails getGoodInfo(String good_id);
    public List<WebVhGoodDetails> getGoodList(String good_id);

    public List<WebFeedbackPic> getPicInfo(String feedback_id);

    public int saveInfo(WebFeedback feedback);

    public int savePicInfo(WebFeedbackPic feedback);

    public List<WebFeedback> hasInfo(WebFeedback feedback);

}