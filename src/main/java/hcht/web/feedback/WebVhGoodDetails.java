/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.web.feedback;

import com.jeeplus.common.persistence.DataEntity;

/**
 * 跨境电商商品详情信息查询Entity
 * @author why
 * @version 2018-03-21
 */
public class WebVhGoodDetails extends DataEntity<WebVhGoodDetails> {

	private static final long serialVersionUID = 1L;
	private String orderGoodId;     //商品ID
	private String goodsName;       //商品名称
	private String originCountry;       //原产国
	private String spec;        //规格型号
	private String useWay;      //用途
	private String masterBase;      //主要成分
	private String brand;       //产品品牌
	private String proEnt;      //生产企业 生产商
	private String cbeName;     //企业名称(销售企业)
	private String cbeCode;     //企业代码
	private String declDate;      //申请日期
	private String declNo;      //申报单号
	private String consignorCname;      //发货人名称
	private String despCountryName;     //启运国 发货国家
	private String despPortName;        //启运口岸
	private String entryPortName;       //入境口岸 到岸港口
	private String createTime;      //入库时间 到岸时间 入境时间
	private String goodsAddress;        //仓库
	private String relsDate;      //放行日期
	private String transEnt;        //运输企业 物流企业
	private String goodsUrl;        //产品图片地址
	private String idCard;		// 证件号
	private String consigneeTel;		// 电话号码

	public String getOrderGoodId() {
		return orderGoodId;
	}

	public void setOrderGoodId(String orderGoodId) {
		this.orderGoodId = orderGoodId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getUseWay() {
		return useWay;
	}

	public void setUseWay(String useWay) {
		this.useWay = useWay;
	}

	public String getMasterBase() {
		return masterBase;
	}

	public void setMasterBase(String masterBase) {
		this.masterBase = masterBase;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProEnt() {
		return proEnt;
	}

	public void setProEnt(String proEnt) {
		this.proEnt = proEnt;
	}

	public String getCbeName() {
		return cbeName;
	}

	public void setCbeName(String cbeName) {
		this.cbeName = cbeName;
	}

	public String getCbeCode() { return cbeCode; }

	public void setCbeCode(String cbeCode) { this.cbeCode = cbeCode; }

	public String getDeclDate() {
		return declDate;
	}

	public void setDeclDate(String declDate) { this.declDate = declDate; }

	public String getDeclNo() {
		return declNo;
	}

	public void setDeclNo(String declNo) {
		this.declNo = declNo;
	}

	public String getConsignorCname() {
		return consignorCname;
	}

	public void setConsignorCname(String consignorCname) {
		this.consignorCname = consignorCname;
	}

	public String getDespCountryName() {
		return despCountryName;
	}

	public void setDespCountryName(String despCountryName) {
		this.despCountryName = despCountryName;
	}

	public String getDespPortName() {
		return despPortName;
	}

	public void setDespPortName(String despPortName) {
		this.despPortName = despPortName;
	}

	public String getEntryPortName() {
		return entryPortName;
	}

	public void setEntryPortName(String entryPortName) {
		this.entryPortName = entryPortName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getGoodsAddress() {
		return goodsAddress;
	}

	public void setGoodsAddress(String goodsAddress) {
		this.goodsAddress = goodsAddress;
	}

	public String getRelsDate() {
		return relsDate;
	}

	public void setRelsDate(String relsDate) {
		this.relsDate = relsDate;
	}

	public String getTransEnt() {
		return transEnt;
	}

	public void setTransEnt(String transEnt) {
		this.transEnt = transEnt;
	}

	public String getGoodsUrl() {
		return goodsUrl;
	}

	public void setGoodsUrl(String goodsUrl) {
		this.goodsUrl = goodsUrl;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getConsigneeTel() {
		return consigneeTel;
	}

	public void setConsigneeTel(String consigneeTel) {
		this.consigneeTel = consigneeTel;
	}

}