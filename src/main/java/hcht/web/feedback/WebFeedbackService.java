/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.web.feedback;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;

/**
 * 跨境电商反馈信息查询Service
 * @author why
 * @version 2018-03-21
 */
@Service
@Transactional(readOnly = true)
public class WebFeedbackService extends CrudService<WebFeedbackDao, WebFeedback> {

	public WebFeedback get(String id) {
		return super.get(id);
	}
	
	public List<WebFeedback> findList(WebFeedback feedback) {
		return super.findList(feedback);
	}
	
	public Page<WebFeedback> findPage(Page<WebFeedback> page, WebFeedback feedback) {
		return super.findPage(page, feedback);
	}
	
	@Transactional(readOnly = false)
	public void save(WebFeedback feedback) {
		super.save(feedback);
	}
	
	@Transactional(readOnly = false)
	public void delete(WebFeedback feedback) {
		super.delete(feedback);
	}

	public WebVhGoodDetails getGoodInfo(String good_id) {
		return dao.getGoodInfo(good_id);
	}

	public Page<WebVhGoodDetails> findPageGoodInfo(Page<WebVhGoodDetails> page, WebVhGoodDetails feedback) {
		feedback.setPage(page);
		page.setList(dao.getGoodList(feedback.getOrderGoodId()));
		return page;
	}

	public int saveInfo(WebFeedback feedback) {
		return dao.saveInfo(feedback);
	}

	public int savePicInfo(WebFeedbackPic feedback) {
		return dao.savePicInfo(feedback);
	}

	public boolean hasInfo(WebFeedback feedback) {
		List<WebFeedback> entity = dao.hasInfo(feedback);
		if (entity == null || entity.size() == 0){
			return false;
		}
		return true;
	}

}