/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package hcht.web.feedback;

import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.jeeplus.common.json.AjaxJson;
import hcht.manage.feedback.Feedback;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;

/**
 * 跨境电商反馈信息查询Controller
 *
 * @author why
 * @version 2018-03-21
 */
@Controller
@RequestMapping(value = "/web")
public class WebFeedbackController extends BaseController {

    @Autowired
    private WebFeedbackService feedbackService;
    private static final List<String> UPLOAD_FILE_TYPE = Arrays.asList(".gif", ".jpg", ".jpeg", ".png", ".bmp");

    @ModelAttribute
    public WebVhGoodDetails get(String idCard, String consigneeTel, String orderGoodId) {
        WebVhGoodDetails entity = null;
//		if (StringUtils.isNotBlank(orderGoodId)){
//			entity = feedbackService.getGoodInfo(orderGoodId);
//		}
//		if (entity == null){
//			entity = new WebVhGoodDetails();
//		}
        return entity;
    }

//	/**
//	 * 增加跨境电商反馈信息查询表单页面
//	 */
//	@RequiresPermissions(value={"manage:feedback:view","feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
//	@RequestMapping(value = "feedback")
//	public String feedback(WebVhGoodDetails feedback, Model model) {
//		model.addAttribute("feedback", feedback);
//		return "hcht/web/feedback";
//	}

    /**
     * 跨境电商反馈信息查询列表页面
     */
//	@RequiresPermissions("manage:feedback:list")
    @RequestMapping(value = {"feedback", ""})
    public String feedback(String idCard, String consigneeTel, String orderGoodId, HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes attr) {
        if (StringUtils.isBlank(orderGoodId)) {
            attr.addFlashAttribute("dataException", "网络异常，请稍后重试");
            return "redirect:/web/index";
        }
        WebVhGoodDetails entity = new WebVhGoodDetails();
//		entity.setIdCard(idCard);
//		entity.setConsigneeTel(consigneeTel);
        entity.setOrderGoodId(orderGoodId);
        Page<WebVhGoodDetails> page = feedbackService.findPageGoodInfo(new Page<WebVhGoodDetails>(request, response), entity);
        if(page.getList() == null || page.getList().size() == 0){
            attr.addFlashAttribute("dataException", "没有查询到相关数据");
            return "redirect:/web/index";
        }
        WebVhGoodDetails entity2 = page.getList().get(0);
        entity2.setIdCard(idCard);
        entity2.setConsigneeTel(consigneeTel);
        model.addAttribute("page", page);
        return "hcht/web/feedback";
    }

    /**
     * 保存跨境电商反馈信息查询
     */
    @RequestMapping(value = "feedbackSave")
    public String feedbackSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String title = request.getParameter("title");
        String text = request.getParameter("text");
        String idCard = request.getParameter("idCard");
        String consigneeTel = request.getParameter("consigneeTel");
        String orderGoodId = request.getParameter("orderGoodId");

        WebFeedback webFeedback = new WebFeedback();
        webFeedback.setIdCard(idCard);
        webFeedback.setConsigneeTel(consigneeTel);
        webFeedback.setOrderGoodId(orderGoodId);
        webFeedback.setTitle(title);
        webFeedback.setDesc(text);
        webFeedback.setSourceType("04");
        webFeedback.preInsert();
        feedbackService.saveInfo(webFeedback);//保存

        String files = request.getParameter("files");
        if (files != null && files.length() > 0) {
            files = files.substring(0, files.length() - 1);
            String[] imgs = files.split("\\|");

            String fid = webFeedback.getId();

            for (int i = 0; i < imgs.length; i++) {
                WebFeedbackPic webFeedbackPic = new WebFeedbackPic();
                webFeedbackPic.setFeedbackId(fid);
                webFeedbackPic.setPicUrl(imgs[i]);
                webFeedbackPic.preInsert();
                feedbackService.savePicInfo(webFeedbackPic);//保存
            }
        }

        return renderString(response, Boolean.toString(true));
    }

    @ResponseBody
    @RequestMapping(value = "feedback/upload")
    public AjaxJson feedbackUpload(WebFeedback feedback, HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) {
        AjaxJson j = new AjaxJson();
        String message = "上传成功";
        //获得文件类型（可以判断如果不是图片，禁止上传）
        String contentType = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));

        if (StringUtils.isBlank(feedback.getIdCard()) || StringUtils.isBlank(feedback.getConsigneeTel()) || StringUtils.isBlank(feedback.getOrderGoodId())) {
            j.setSuccess(false);
            message = "网络异常，请刷新页面后重试。";
        } else if(!UPLOAD_FILE_TYPE.contains(contentType.toLowerCase())){
            j.setSuccess(false);
            message = "只能上传" + UPLOAD_FILE_TYPE.toString() + "格式的图片。";
        } else if (feedbackService.hasInfo(feedback) == false) {
            j.setSuccess(false);
            message = "网络异常，请刷新页面后重试。";
        } else {
            //定义上传图片保存路径
            String uploadPath = "/upload/images/";
            //服务器路径
            String realPath = request.getSession().getServletContext().getRealPath(uploadPath);
            //判断保存目录是否存在，不存在则创建
            File saveDocument = new File(realPath);
            if (!saveDocument.exists()) saveDocument.mkdirs();
            if (!file.isEmpty()) {
                //生成uuid作为文件名称
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");

                //获得文件后缀名称
                String imageName = uuid + contentType;
                try {
                    file.transferTo(new File(realPath + File.separator + imageName));
                    j.put("imgUrl", request.getScheme() + "://" + request.getServerName() + (request.getServerPort() != 443 && request.getServerPort() != 80 ? ":" + String.valueOf(request.getServerPort()) : "") + uploadPath + imageName);
                } catch (IOException e) {
                    j.setSuccess(false);
                    j.put("error", e.getMessage());
                    message = "网络异常，请刷新页面后重试。";
                }
            }
        }
        j.setMsg(message);

        return j;
    }


}