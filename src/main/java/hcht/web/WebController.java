package hcht.web;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.web.BaseController;
import hcht.manage.orderlog.OrderLog;
import hcht.manage.orderlog.OrderLogService;
import hcht.manage.readlog.ReadLog;
import hcht.manage.readlog.ReadLogService;
import hcht.manage.tracelog.TraceLog;
import hcht.manage.tracelog.TraceLogService;
import hcht.web.goods.GoodsOrderService;
import hcht.web.goods.GoodsOrder;
import hcht.web.goods.VhCbe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "/web")
public class WebController extends BaseController {

    @Autowired
    private GoodsOrderService goodsOrderService;
    @Autowired
    private ReadLogService readLogService;
    @Autowired
    private TraceLogService traceLogService;
    @Autowired
    private OrderLogService orderLogService;
    private static final String SOURCE_TYPE = "04";

    //    @ResponseBody
    @RequestMapping(value = "index")
    public String index(GoodsOrder goodsOrder, Model model, @ModelAttribute("dataException") String dataException) {
        model.addAttribute("goodsOrder", goodsOrder);
        if(StringUtils.isNotBlank(dataException)){
            addMessage(model, dataException);
        }
        return "hcht/web/index";
    }


    @RequestMapping(value = "goodlist")
    public String goodlist(GoodsOrder goodsOrder, HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes attr) {
        if(goodsOrder == null){
            attr.addFlashAttribute("dataException", "网络异常，请稍后重试");
            return "redirect:index";
        }
        if("index".equals(goodsOrder.getFromPage())) {//判断页面来源是手机号和证件号查询页
            goodsOrder.setOrderSerialNo("");
            goodsOrder.setCbeCode("");
            goodsOrder.setOrderNo("");
            if (StringUtils.isBlank(goodsOrder.getIdCard()) || StringUtils.isBlank(goodsOrder.getConsigneeTel())) {
                attr.addFlashAttribute("dataException", "没有填写证件号码或手机号码");
                return "redirect:index";
            }
        }else {
            goodsOrder.setIdCard("");
            goodsOrder.setConsigneeTel("");
            if (StringUtils.isBlank(goodsOrder.getCbeCode()) || StringUtils.isBlank(goodsOrder.getOrderNo())) {
                goodsOrder.setOrderSerialNo("");
                attr.addFlashAttribute("dataException", "没有选择企业或没有填写订单号");
                return "redirect:orders";
            }else{
                goodsOrder.setOrderSerialNo(goodsOrder.getCbeCode() + goodsOrder.getOrderNo());
            }
        }

        Page<GoodsOrder> page = goodsOrderService.findPage(new Page<GoodsOrder>(request, response), goodsOrder);
        if (page.getList() != null && page.getList().size() > 0) {
            model.addAttribute("page", page);
            if("index".equals(goodsOrder.getFromPage())) {//判断页面来源是是依据手机号和证件号查询页
                ReadLog readLog = new ReadLog();
                readLog.setIdCard(goodsOrder.getIdCard());
                readLog.setConsigneeTel(goodsOrder.getConsigneeTel());
                readLog.setSourceType(SOURCE_TYPE);
                readLogService.save(readLog);
            }else{//保存order log信息
                OrderLog orderLog = new OrderLog();
                orderLog.setOrderSerialNo(goodsOrder.getOrderSerialNo());
                orderLog.setSourceType(SOURCE_TYPE);
                orderLogService.save(orderLog);
            }
            return "hcht/web/goodlist";
        } else {
            attr.addFlashAttribute("dataException", "没有查询到相关数据");
            return "redirect:index";
        }
    }

    @RequestMapping(value = "traceinfo")
    public String traceinfo(Model model, String id, RedirectAttributes attr) {
        GoodsOrder goodsOrder;
        if (StringUtils.isNotBlank(id)) {
            goodsOrder = goodsOrderService.get(id);
        }else{
            attr.addFlashAttribute("dataException", "网络异常，请稍后重试");
            return "redirect:index";
        }
        if (goodsOrder == null) {
            attr.addFlashAttribute("dataException", "没有查询到相关数据");
            return "redirect:index";
        }
        TraceLog traceLog = new TraceLog();
        traceLog.setIdCard(goodsOrder.getIdCard());
        traceLog.setConsigneeTel(goodsOrder.getConsigneeTel());
        traceLog.setVhGoodsTrace(goodsOrder.getVhGoodsTrace());
        traceLog.setSourceType(SOURCE_TYPE);
        traceLogService.save(traceLog);
        model.addAttribute("goodsOrder", goodsOrder);
        return "hcht/web/traceinfo";
    }

    @RequestMapping(value = "orders")
    public String orders(GoodsOrder goodsOrder, Model model, @ModelAttribute("dataException") String dataException) {
        //企业的列表从V_H_CBE这里取数据
        model.addAttribute("goodsOrder", goodsOrder);
        List<VhCbe> list = goodsOrderService.findCbes();
        model.addAttribute("cbes", list);
        if(StringUtils.isNotBlank(dataException)){
            addMessage(model, dataException);
        }
        return "hcht/web/orders";
    }


}
