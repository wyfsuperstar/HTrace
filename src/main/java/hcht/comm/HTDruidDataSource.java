package hcht.comm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.druid.pool.DruidDataSource;

public class HTDruidDataSource extends DruidDataSource {
    private static final long serialVersionUID = 5356438661505332271L;
    private final static Logger logger = LoggerFactory.getLogger(HTDruidDataSource.class);
    public HTDruidDataSource() {
        super();
    }
    @Override
    public void setPassword(String password){
        try{
            this.password = decode(password);
        }catch(Exception e){
            logger.error(e.getMessage());
        }
    }
//    @Override
//    public void setUsername(String username) {
//        try{
//            this.username = decode(username);
//        }catch(Exception e){
//            logger.error(e.getMessage());
//        }
//    }


    public static String encodeToString(String password){
        //编码
        String str=null;
        try {
            BASE64Encoder encoder = new  BASE64Encoder();
            str = encoder.encode(password.getBytes("utf-8"));
            System.out.println("编码结果为："+str);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }

        return str;
    }
    public static String decode(String password){
        //解码
        String str=null;
        BASE64Decoder decoder = new  BASE64Decoder();
        //String str1 = decoder.
        try {
            byte[] str1=decoder.decodeBuffer(password);
            str = new String(str1,"utf-8");
//            str1=decoder.decodeBuffer(str);
//            str = new String(str1,"utf-8");
//            str1=decoder.decodeBuffer(str);
//            str = new String(str1,"utf-8");
            System.out.println("解码结果为："+str);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

}
