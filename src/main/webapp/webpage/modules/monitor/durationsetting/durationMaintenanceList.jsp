<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
    <title>时长监控条件维护</title>
    <meta name="decorator" content="default"/>
    <!-- 引入 ECharts 文件 -->
   <%-- <script src="${ctxStatic}/echarts-latest/echarts.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function() {
           /* laydate({
                elem: '#beginInDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });
            laydate({
                elem: '#endInDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });*/
        });
    </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>时长监控条件维护 </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">选项1</a>
                    </li>
                    <li><a href="#">选项2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
            <sys:message content="${message}"/>

            <!--查询条件-->
            <div class="row">
                <div class="col-sm-12">
                    <form:form id="searchForm" modelAttribute="searchDuration" action="${ctx}/monitor/durationsetting/list" method="post" class="form-inline">
                        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                        <table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
                        <div class="form-group">
                            <span>时长项目：</span>
                            <form:select id="durationType" path="durationType" class="form-control m-b">
                                <form:option value="">&nbsp;&nbsp;&nbsp;全部&nbsp;&nbsp;&nbsp;</form:option>
                                <form:options items="${fns:getDictList('fm_warntype')}" itemLabel="label" itemValue="value" htmlEscape="false"></form:options>
                            </form:select>
                            <span>出入境：</span>
                            <form:select path="outOrInto">
                                <form:option value="" label="出入境" htmlEscape="false"/>
                                <form:options items="${fns:getDictList('fm_eoiflag')}" itemLabel="label" itemValue="value" htmlEscape="false"></form:options>
                            </form:select>
                            <span>检验检疫类别：</span>
                            <sys:treeselect id="inspTypeQuery" name="inspTypeQuery" value="${duration.inspTypeQuery}" labelName="inspTypeQueryName" labelValue="${duration.inspTypeQueryName}"
                                            title="检验检疫类别" url="/monitor/durationsetting/pTypeTreeData"
                                            cssClass=" form-control input-sm" notAllowSelectParent="true"/>
                        </div>
                    </form:form>
                    <br/>
                </div>
            </div>

            <!-- 工具栏 -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-left">
                        <table:addRow url="${ctx}/monitor/durationsetting/form" title="时长监控信息"></table:addRow><!-- 增加按钮 -->
                        <table:editRow url="${ctx}/monitor/durationsetting/form" title="时长监控信息" id="contentTable"></table:editRow><!-- 编辑按钮 -->
                        <table:delRow url="${ctx}/monitor/durationsetting/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->
                       <%-- <table:importExcel url="${ctx}"></table:importExcel><!-- 导入按钮 -->
                        <table:exportExcel url="${ctx}"></table:exportExcel><!-- 导出按钮 -->--%>
                            <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
                    </div>
                    <div class="pull-right">
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
                    </div>
                </div>
            </div>

            <!-- 表格 -->
            <table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
                <thead>
                <tr>
                    <th><input  type="checkbox"  class="i-checks"/></th>
                    <th  class="sort-column WARN_DURA_TYPE">时长项目</th>
                    <th  class="sort-column INSP_TYPE">检验检疫类别</th>
                    <th  class="sort-column WARN_DAYS">预警时长</th>
                    <%--<th  class="sort-column LAB_WARN_DAYS">过实验室时间</th>--%>
                    <th  class="sort-column EXP_IMP_FLAG">出入境</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="duration" items="${page.list}">
                    <tr>
                        <td><input id="${duration.id}" type="checkbox" class="i-checks"/></td>
                        <td>${fns:getDictLabel(duration.durationType,"fm_warntype","" ) }</td>
                        <td><c:choose>
                            <c:when test="${duration.inspType eq '9999'}">
                                默认
                            </c:when>
                            <c:otherwise>
                                ${duration.inspTypeName}
                            </c:otherwise>
                        </c:choose>
                        </td>
                        <td>${duration.warnDuration}</td>
                        <%--<td>${duration.labWarnDays}</td>--%>
                        <td>${fns:getDictLabel( duration.outOrInto,'fm_eoiflag' ,'' )}</td>
                        <td>
                            <shiro:hasPermission name="durationsetting:durationSetting:view">
                                <a href="#" onclick="openDialogView('查看时长监控条件信息', '${ctx}/monitor/durationsetting/form?id=${duration.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="durationsetting:durationSetting:edit">
                                <a href="#" onclick="openDialog('修改时长监控条件信息', '${ctx}/monitor/durationsetting/form?id=${duration.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="durationsetting:durationSetting:del">
                                <c:if test="${duration.inspType ne '9999'}">
                                    <a href="${ctx}/monitor/durationsetting/delete?id=${duration.id}" onclick="return confirmx('确认要删除该时长监控条件吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
                                </c:if>
                            </shiro:hasPermission>
                        </td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
            <table:page page="${page}"></table:page>
            <br/>
            <br/>
        </div>
    </div>
</div>
</body>
</html>