<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
    <title></title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        var validateForm;
        var val=0;
        function doSubmit() {//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
            if (validateForm.form())
            {
                ajax();
                if(val != 0) {
                    alert("该出入境类型的时长已经存在!请重新输入。");
                    return false;
                }
                else {
                    $("#inputForm").submit();
                    return true;
                }
            }
            return false;
        }

        function ajax() {
            var name=$("#durationType").val();
            var outOrInto=$("#outOrInto").val();
            var id = $("#id").val();
            $.ajax({
                url:"${ctx}/monitor/durationsetting/valiDuration",
                type:"post",
                async:false,
                data:{durationType:name,outOrInto:outOrInto,id:id},
                success:function (data) {
                    val = data;
                }
            });
        }

        $(document).ready(function() {
            validateForm = $("#inputForm").validate({
                submitHandler: function (form) {
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function (error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
    <%--去除input  type=number  输入框后的符号--%>
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button{
            -webkit-appearance: none !important;
            margin: 0;
        }
    </style>
</head>
<body class="hideScroll">
<form:form id="inputForm" modelAttribute="duration" action="${ctx}/monitor/durationsetting/save" method="post" class="form-horizontal">
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active">
                <input type="hidden" id="id" name="id" value="${duration.id}"/>
                <label class="pull-right"><font color="red">*</font>预警时长项目：</label>
            </td>
            <td class="width-35">
                <form:select id="durationType" path="durationType" class="form-control">
                    <form:options items="${fns:getDictList('fm_warntype')}" itemLabel="label" itemValue="value" htmlEscape="false"></form:options>
                </form:select>
            </td>
            <td class="width-15 active"><label class="pull-right"><font color="red">*</font>出入境</label></td>
            <td class="width-35">
                <form:select id="outOrInto" path="outOrInto">
                    <form:options items="${fns:getDictList('fm_eoiflag')}" itemLabel="label" itemValue="value" htmlEscape="false"></form:options>
                </form:select>
            </td>
        </tr>
        <tr>
            <td class="width-15 active"><label class="pull-right"><font color="red">*</font>检验检疫类别：</label></td>
            <td class="width-35">
               <%-- <select id="inspType" name="inspType" class="form-control m-b">
                    <option value="1">检验检疫类别1</option>
                    <option value="1">检验检疫类别2</option>
                    <option value="1">检验检疫类别3</option>
                    <option value="1">检验检疫类别4</option>
                </select>--%>
                   <sys:treeselect id="inspType" name="inspType" value="${duration.inspType}" labelName="inspTypeName" labelValue="${duration.inspType eq '9999'? '默认':duration.inspTypeName}"
                                   title="检验检疫类别" url="/monitor/durationsetting/pTypeTreeData"
                                   cssClass=" form-control input-sm required" notAllowSelectParent="true"  disabled="${duration.inspType eq '9999'? 'disabled':''}"/>
            </td>
            <td class="width-15 active"><label class="pull-right"><font color="red">*</font>预警时长：</label></td>
            <td class="width-35">
                <form:input path="warnDuration" htmlEscape="false" maxlength="10" class="form-control m-b required number"/>
            </td>
        </tr>
<%--        <tr>

            <td class="width-15 active"><label class="pull-right">过实验室时间：</label></td>
            <td class="width-35">
                <input id="labWarnDays" name="labWarnDays" type="number" min="0" max="99.99999999999" value="${duration.labWarnDays}"  class="form-control m-b">
            </td>
        </tr>--%>
        <tr>
            <td class="width-15 active"><label class="pull-right">备注</label></td>
            <td colspan="3"/>
                <input id="remarks" name="remarks" type="text" value="${duration.remarks}" class="form-control m-b"/>
            </td>

        </tr>
        </tbody>
    </table>
</form:form>
</body>
</html>