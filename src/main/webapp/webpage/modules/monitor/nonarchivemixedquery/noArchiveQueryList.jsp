<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
    <title>未归档组合查询</title>
    <meta name="decorator" content="default">
    <script>
        $(document).ready(function () {
            laydate({
                elem: '#beginDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });
            laydate({
                elem: '#endDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });
        });
    </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>未归档组合查询</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">选项1</a>
                    </li>
                    <li><a href="#">选项2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <sys:message content="${message}"/>
            <!--查询条件-->
            <div class="row">
                <div class="col-sm-12">
                    <form:form id="searchForm" modelAttribute="noArchive" action="${ctx}/modules/nonarchivemixedquery/noarchivequery/list" method="post" class="form-inline">
                        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                        <table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
                        <div class="form-group">
                            <span>报检机构：</span>
                            <form:select path="orgCode" cssStyle="width: 220px">
                                <c:forEach var="testORG" items="${ORG}">
                                    <form:option value="${testORG.testORGcode}" label="${testORG.testORG}" htmlEscape="false"/>
                                </c:forEach>
                            </form:select>
                            <div class="form-group">
                            <span>施检机构：</span>
                           <form:select path="testORGcode" cssStyle="width: 220px">
                               <c:forEach var="testORG" items="${ORG}">
                                   <form:option value="${testORG.testORGcode}" label="${testORG.testORG}" htmlEscape="false"/>
                               </c:forEach>
                           </form:select>
                            <span>出入境：</span>
                            <form:select path="outOrInto">
                                <form:option value="" label="出入境" htmlEscape="false"/>
                                <form:options items="${fns:getDictList('fm_eoiflag')}" itemValue="value" itemLabel="label" htmlEscape="false"/>
                            </form:select>
                            <span>报检日期：</span>
                            <input id="beginDate" name="beginDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
                                   value="<fmt:formatDate value="${noArchive.beginDate}" pattern="yyyy-MM-dd"/>"/>
                            <span>&nbsp;至&nbsp;</span>
                            <input id="endDate" name="endDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
                                   value="<fmt:formatDate value="${noArchive.endDate}" pattern="yyyy-MM-dd"/>"/>
                        </div>
                    </form:form>
                    <br/>
                </div>
            </div>

            <!-- 工具栏 -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-left">
                        <shiro:hasPermission name="noarchivequery:add">
                            <table:addRow url="${ctx}/sys/user/form" title="用户" width="800px" height="625px" target="officeContent"></table:addRow><!-- 增加按钮 -->
                        </shiro:hasPermission>
                        <shiro:hasPermission name="noarchivequery:edit">
                            <table:editRow url="${ctx}/form" id="contentTable"  title="用户" width="800px" height="680px" target="officeContent"></table:editRow><!-- 编辑按钮 -->
                        </shiro:hasPermission>
                        <shiro:hasPermission name="noarchivequery:detelAll">
                            <table:delRow url="${ctx}/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->
                        </shiro:hasPermission>
                        <shiro:hasPermission name="noarchivequery:import">
                            <table:importExcel url="${ctx}/import"></table:importExcel><!-- 导入按钮 -->
                        </shiro:hasPermission>
                        <shiro:hasPermission name="noarchivequery:export">
                            <table:exportExcel url="${ctx}/modules/nonarchivemixedquery/noarchivequery/export"></table:exportExcel><!-- 导出按钮 -->
                        </shiro:hasPermission><%-- <a download="固定报表-按处室.xls" href="#" onclick="return ExcellentExport.excel(this, 'contentTable1', '固定报表');"
                           class="btn btn-primary btn-rounded btn-outline btn-sm "><i class="fa fa-file-excel-o"></i>导出</a>--%>


                        <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
                    </div>
                    <div class="pull-right">
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
                    </div>
                </div>
            </div>

            <!-- 表格 -->
            <table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
                <thead>
                <tr>
                    <th  class="sort-column a.DECL_NO">报检号</th>
                    <th  class="sort-column a.ORG_CODE">报检机构</th>
                    <th  class="sort-column a.INSP_ORG_CODE">施检机构</th>
                    <th  class="sort-column a.EXP_IMP_FLAG">出入境</th>
                    <th  class="sort-column a.DECL_DATE">报检日期</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="noArchive" items="${page.list}">
                    <tr>
                        <td>${noArchive.declNo}</td>
                        <td>${noArchive.orgName}</td>
                        <td>${noArchive.testORG}</td>
                        <td>${fns:getDictLabel(noArchive.outOrInto,"fm_eoiflag","")}</td>
                        <td><fmt:formatDate value="${noArchive.decDate}" pattern="yyyy-MM-dd"/></td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
            <table:page page="${page}"></table:page>
            <br/>
            <br/>
        </div>
    </div>
</div>
</body>
</html>
