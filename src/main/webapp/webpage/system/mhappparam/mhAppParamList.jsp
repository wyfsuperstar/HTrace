<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>参数设置管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>参数设置列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="mhAppParam" action="${ctx}/mhappparam/mhAppParam/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>param_id：</span>
				<form:input path="paramId" htmlEscape="false" maxlength="36"  class=" form-control input-sm"/>
			<span>type_id：</span>
				<form:input path="typeId" htmlEscape="false" maxlength="32"  class=" form-control input-sm"/>
			<span>param_code：</span>
				<form:input path="paramCode" htmlEscape="false" maxlength="32"  class=" form-control input-sm"/>
			<span>param_name：</span>
				<form:input path="paramName" htmlEscape="false" maxlength="200"  class=" form-control input-sm"/>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<%--<shiro:hasPermission name="mhappparam:mhAppParam:add">--%>
				<%--<table:addRow url="${ctx}/mhappparam/mhAppParam/form" title="参数设置"></table:addRow><!-- 增加按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhappparam:mhAppParam:edit">--%>
			    <%--<table:editRow url="${ctx}/mhappparam/mhAppParam/form" title="参数设置" id="contentTable"></table:editRow><!-- 编辑按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhappparam:mhAppParam:del">--%>
				<%--<table:delRow url="${ctx}/mhappparam/mhAppParam/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhappparam:mhAppParam:import">--%>
				<%--<table:importExcel url="${ctx}/mhappparam/mhAppParam/import"></table:importExcel><!-- 导入按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhappparam:mhAppParam:export">--%>
	       		<%--<table:exportExcel url="${ctx}/mhappparam/mhAppParam/export"></table:exportExcel><!-- 导出按钮 -->--%>
	       	<%--</shiro:hasPermission>--%>
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column param_id">param_id</th>
				<th  class="sort-column type_id">type_id</th>
				<th  class="sort-column param_code">param_code</th>
				<th  class="sort-column param_name">param_name</th>
				<th  class="sort-column param_series">param_series</th>
				<th  class="sort-column param_enable">param_enable</th>
				<th  class="sort-column code_type">code_type</th>
				<th  class="sort-column param_remark">param_remark</th>
				<th  class="sort-column create_date">create_date</th>
				<th  class="sort-column creater">creater</th>
				<th  class="sort-column modify_date">modify_date</th>
				<th  class="sort-column modifier">modifier</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="mhAppParam">
			<tr>
				<td> <input type="checkbox" id="${mhAppParam.paramId}" class="i-checks"></td>
				<td><a  href="#" onclick="openDialogView('查看参数设置', '${ctx}/mhappparam/mhAppParam/form?id=${mhAppParam.paramId}','800px', '500px')">
					${mhAppParam.paramId}
				</a></td>
				<td>
					${mhAppParam.typeId}
				</td>
				<td>
					${mhAppParam.paramCode}
				</td>
				<td>
					${mhAppParam.paramName}
				</td>
				<td>
					${mhAppParam.paramSeries}
				</td>
				<td>
					${mhAppParam.paramEnable}
				</td>
				<td>
					${mhAppParam.codeType}
				</td>
				<td>
					${mhAppParam.paramRemark}
				</td>
				<td>
					<fmt:formatDate value="${mhAppParam.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${mhAppParam.creater}
				</td>
				<td>
					<fmt:formatDate value="${mhAppParam.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${mhAppParam.modifier}
				</td>
				<td>
					<shiro:hasPermission name="mhappparam:mhAppParam:view">
						<a href="#" onclick="openDialogView('查看参数设置', '${ctx}/mhappparam/mhAppParam/form?id=${mhAppParam.paramId}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
					</shiro:hasPermission>
					<%--<shiro:hasPermission name="mhappparam:mhAppParam:edit">--%>
    					<%--<a href="#" onclick="openDialog('修改参数设置', '${ctx}/mhappparam/mhAppParam/form?id=${mhAppParam.paramId}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>--%>
    				<%--</shiro:hasPermission>--%>
    				<%--<shiro:hasPermission name="mhappparam:mhAppParam:del">--%>
						<%--<a href="${ctx}/mhappparam/mhAppParam/delete?id=${mhAppParam.paramId}" onclick="return confirmx('确认要删除该参数设置吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>--%>
					<%--</shiro:hasPermission>--%>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>