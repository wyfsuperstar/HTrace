<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>检验检疫报关表头管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#declDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="mhTbImportApplyRecordHead" action="${ctx}/mhtbimportapplyrecordhead/mhTbImportApplyRecordHead/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">境外进区流水号：</label></td>
					<td class="width-35">
						<form:input path="iiSerialNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报号：</label></td>
					<td class="width-35">
						<form:input path="declNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">备注：</label></td>
					<td class="width-35">
						<form:input path="remark" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">单申报人名称：</label></td>
					<td class="width-35">
						<form:input path="declPerson" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申请单状态代码：1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过：</label></td>
					<td class="width-35">
						<form:input path="status" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">组织机构代码：</label></td>
					<td class="width-35">
						<form:input path="orgNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">操作状态0正常   1已删除：</label></td>
					<td class="width-35">
						<form:input path="activeInd" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报时间：</label></td>
					<td class="width-35">
						<input id="declDate" name="declDate" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${mhTbImportApplyRecordHead.declDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">抽样单号：</label></td>
					<td class="width-35">
						<form:input path="sampleNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报类型代码：</label></td>
					<td class="width-35">
						<form:input path="declTypeCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">贸易方式：</label></td>
					<td class="width-35">
						<form:input path="tradeModeCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报单位代码：</label></td>
					<td class="width-35">
						<form:input path="entCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">电商企业代码：</label></td>
					<td class="width-35">
						<form:input path="cbecode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">施检机构代码：</label></td>
					<td class="width-35">
						<form:input path="checkOrgCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">目的机构代码：</label></td>
					<td class="width-35">
						<form:input path="orgCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人证件类型：</label></td>
					<td class="width-35">
						<form:input path="consigneeIdtype" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人证件类型：</label></td>
					<td class="width-35">
						<form:input path="consignorIdtype" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">运输工具代码：</label></td>
					<td class="width-35">
						<form:input path="transTypeCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">贸易国别/地区代码：</label></td>
					<td class="width-35">
						<form:input path="tradeCountryCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">启运国代码：</label></td>
					<td class="width-35">
						<form:input path="despCountryCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">启运口岸代码：</label></td>
					<td class="width-35">
						<form:input path="despPortCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">入境口岸代码：</label></td>
					<td class="width-35">
						<form:input path="entryPortCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">集货/备货标识 C备货 B集货：</label></td>
					<td class="width-35">
						<form:input path="stockFlag" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人编号：</label></td>
					<td class="width-35">
						<form:input path="consigneeNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人：</label></td>
					<td class="width-35">
						<form:input path="consigneeCname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人英文名称：</label></td>
					<td class="width-35">
						<form:input path="consigneeEname" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人地址：</label></td>
					<td class="width-35">
						<form:input path="consigneeAddress" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人电话：</label></td>
					<td class="width-35">
						<form:input path="consigneeTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人证件号码：</label></td>
					<td class="width-35">
						<form:input path="consigneeIdnum" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人编号：</label></td>
					<td class="width-35">
						<form:input path="consginorNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人：</label></td>
					<td class="width-35">
						<form:input path="consignorCname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人英文名称：</label></td>
					<td class="width-35">
						<form:input path="consginorEname" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人地址：</label></td>
					<td class="width-35">
						<form:input path="consignorAddress" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人电话：</label></td>
					<td class="width-35">
						<form:input path="consignorTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人证件号码：</label></td>
					<td class="width-35">
						<form:input path="consignorIdnum" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">是否有废旧物品：</label></td>
					<td class="width-35">
						<form:input path="wasteFlag" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">是否带有植物性包装及铺垫材料：</label></td>
					<td class="width-35">
						<form:input path="packFlag" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">运输号码：</label></td>
					<td class="width-35">
						<form:input path="transTypeNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">合同号：</label></td>
					<td class="width-35">
						<form:input path="contractNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">提/运单号：</label></td>
					<td class="width-35">
						<form:input path="carrierNoteNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">随附单据代码串,由相应单据代码串联而得：</label></td>
					<td class="width-35">
						<form:input path="sheetTypeCodes" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">货物存放地点：</label></td>
					<td class="width-35">
						<form:input path="goodsAddress" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">报检单号：</label></td>
					<td class="width-35">
						<form:input path="lawNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">检验检疫状态代码：1待检疫 2合格 3不合格：</label></td>
					<td class="width-35">
						<form:input path="insStatus" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">id：</label></td>
					<td class="width-35" ><form:input path="id" htmlEscape="false"    class="form-control "/></td>
					<td class="width-15 active"></td>
					<td class="width-35">
					</td>
				</tr>

		 	</tbody>
		</table>
	</form:form>
</body>
</html>