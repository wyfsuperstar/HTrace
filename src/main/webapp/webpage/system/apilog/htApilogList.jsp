<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>api日志管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
	        laydate({
	            elem: '#beginReadDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
	        laydate({
	            elem: '#endReadDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
					
		
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>api日志列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="htApilog" action="${ctx}/apilog/htApilog/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>企业备案号：</span>
				<form:input path="attribute" htmlEscape="false" maxlength="32"  class=" form-control input-sm"/>
			<span>SSID：</span>
				<form:input path="ssid" htmlEscape="false" maxlength="32"  class=" form-control input-sm"/>
			<span>访问时间：</span>
				<input id="beginReadDate" name="beginReadDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
					value="<fmt:formatDate value="${htApilog.beginReadDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/> - 
				<input id="endReadDate" name="endReadDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
					value="<fmt:formatDate value="${htApilog.endReadDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column apiType">接口类型</th>
				<th  class="sort-column attribute">企业备案号</th>
				<th  class="sort-column ssid">SSID</th>
				<th  class="sort-column read_Date">访问时间</th>
				<th  class="sort-column ip">IP</th>
				<th  class="sort-column success">成功标志</th>
				<th  class="sort-column error_Code">错误代码</th>
				<th  class="sort-column error_Msg">错误信息</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="htApilog">
			<tr>
				<td><a  href="#" onclick="openDialogView('查看api日志', '${ctx}/apilog/htApilog/form?id=${htApilog.id}','800px', '500px')">
					${htApilog.apiType}
				</a></td>
				<td>
					${htApilog.attribute}
				</td>
				<td>
					${htApilog.ssid}
				</td>
				<td>
					<fmt:formatDate value="${htApilog.readDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${htApilog.ip}
				</td>
				<td>
					${htApilog.success}
				</td>
				<td>
					${htApilog.errorCode}
				</td>
				<td>
					${htApilog.errorMsg}
				</td>
				<td>
						<a href="#" onclick="openDialogView('查看api日志', '${ctx}/apilog/htApilog/form?id=${htApilog.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>

				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>