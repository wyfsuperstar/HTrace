<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>订单表头管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#storageTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="mhOrderHead" action="${ctx}/mhorderhead/mhOrderHead/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">订单流水号 ：</label></td>
					<td class="width-35">
						<form:input path="orderSerialNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">订单编号 ：</label></td>
					<td class="width-35">
						<form:input path="orderNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">订单批次号 ：</label></td>
					<td class="width-35">
						<form:input path="batchNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电商平台编号    ：</label></td>
					<td class="width-35">
						<form:input path="ecpCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">进/出口编号     I进口 E出口：</label></td>
					<td class="width-35">
						<form:input path="ieType" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电商企业编号    ：</label></td>
					<td class="width-35">
						<form:input path="cbeCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">业务类型 1保税备货 2保税集货  3邮件 4快件：</label></td>
					<td class="width-35">
						<form:input path="bizType" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理申报企业编号：</label></td>
					<td class="width-35">
						<form:input path="agentCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人名称 ：</label></td>
					<td class="width-35">
						<form:input path="consigneeName" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人电话 ：</label></td>
					<td class="width-35">
						<form:input path="consigneeTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人地址 ：</label></td>
					<td class="width-35">
						<form:input path="consigneeAddress" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人所在国编号：</label></td>
					<td class="width-35">
						<form:input path="consigneeCountryCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人 ：</label></td>
					<td class="width-35">
						<form:input path="consignorCname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人地址 ：</label></td>
					<td class="width-35">
						<form:input path="consignorAddress" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人电话 ：</label></td>
					<td class="width-35">
						<form:input path="consignorTel" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人所在国编号：</label></td>
					<td class="width-35">
						<form:input path="consignorCountryCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">证件类型编号 1身份证 2军官证 3护照 4其他：</label></td>
					<td class="width-35">
						<form:input path="idType" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">证件号码 ：</label></td>
					<td class="width-35">
						<form:input path="idCard" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">订单商品货款    ：</label></td>
					<td class="width-35">
						<form:input path="goodsValue" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">订单商品运费 ：</label></td>
					<td class="width-35">
						<form:input path="freight" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">币制编号 ：</label></td>
					<td class="width-35">
						<form:input path="currency" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">贸易国别编号 ：</label></td>
					<td class="width-35">
						<form:input path="tradeCountryCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">备注 ：</label></td>
					<td class="width-35">
						<form:input path="remark" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">状态：</label></td>
					<td class="width-35">
						<form:input path="status" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">区分直邮（Z）和保税(B)：</label></td>
					<td class="width-35">
						<form:input path="type" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">入库时间：</label></td>
					<td class="width-35">
						<form:input path="createTime" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报时间：</label></td>
					<td class="width-35">
						<form:input path="decltime" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电商平台名称：</label></td>
					<td class="width-35">
						<form:input path="ecpName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">电商企业名称：</label></td>
					<td class="width-35">
						<form:input path="cbeName" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">是否单独报检   1 单独报检：</label></td>
					<td class="width-35">
						<form:input path="flag" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">报文入库时间：</label></td>
					<td class="width-35">
						<input id="storageTime" name="storageTime" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							   value="<fmt:formatDate value="${mhOrderHead.storageTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
					</td>
					<td class="width-15 active"><label class="pull-right">ciq：</label></td>
					<td class="width-35">
						<form:input path="ciq" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">id：</label></td>
					<td class="width-35" ><form:input path="id" htmlEscape="false"    class="form-control "/></td>
					<td class="width-15 active"></td>
					<td class="width-35">
					</td>
				</tr>


		 	</tbody>
		</table>
	</form:form>
</body>
</html>