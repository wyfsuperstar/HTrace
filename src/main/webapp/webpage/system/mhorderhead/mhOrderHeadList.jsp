<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>订单表头管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
	        laydate({
	            elem: '#beginStorageTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
	        laydate({
	            elem: '#endStorageTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
					
		
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>订单表头列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="mhOrderHead" action="${ctx}/mhorderhead/mhOrderHead/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>订单流水号 ：</span>
				<form:input path="orderSerialNo" htmlEscape="false" maxlength="50"  class=" form-control input-sm"/>
			<span>订单编号 ：</span>
				<form:input path="orderNo" htmlEscape="false" maxlength="60"  class=" form-control input-sm"/>
			<span>电商企业编号    ：</span>
				<form:input path="cbeCode" htmlEscape="false" maxlength="30"  class=" form-control input-sm"/>
			<span>证件号码 ：</span>
				<form:input path="idCard" htmlEscape="false" maxlength="30"  class=" form-control input-sm"/>
			<span>报文入库时间：</span>
				<input id="beginStorageTime" name="beginStorageTime" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
					value="<fmt:formatDate value="${mhOrderHead.beginStorageTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/> - 
				<input id="endStorageTime" name="endStorageTime" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
					value="<fmt:formatDate value="${mhOrderHead.endStorageTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:add">--%>
				<%--<table:addRow url="${ctx}/mhorderhead/mhOrderHead/form" title="订单表头"></table:addRow><!-- 增加按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:edit">--%>
			    <%--<table:editRow url="${ctx}/mhorderhead/mhOrderHead/form" title="订单表头" id="contentTable"></table:editRow><!-- 编辑按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:del">--%>
				<%--<table:delRow url="${ctx}/mhorderhead/mhOrderHead/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:import">--%>
				<%--<table:importExcel url="${ctx}/mhorderhead/mhOrderHead/import"></table:importExcel><!-- 导入按钮 -->--%>
			<%--</shiro:hasPermission>--%>
			<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:export">--%>
	       		<%--<table:exportExcel url="${ctx}/mhorderhead/mhOrderHead/export"></table:exportExcel><!-- 导出按钮 -->--%>
	       	<%--</shiro:hasPermission>--%>
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column order_Serial_No">订单流水号 </th>
				<th  class="sort-column order_No">订单编号 </th>
				<th  class="sort-column cbe_Code">电商企业编号    </th>
				<th  class="sort-column id_Card">证件号码 </th>
				<th  class="sort-column id">主键</th>
				<th  class="sort-column storage_Time">报文入库时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="mhOrderHead">
			<tr>
				<td> <input type="checkbox" id="${mhOrderHead.id}" class="i-checks"></td>
				<td><a  href="#" onclick="openDialogView('查看订单表头', '${ctx}/mhorderhead/mhOrderHead/form?id=${mhOrderHead.id}','800px', '500px')">
					${mhOrderHead.orderSerialNo}
				</a></td>
				<td>
					${mhOrderHead.orderNo}
				</td>
				<td>
					${mhOrderHead.cbeCode}
				</td>
				<td>
					${mhOrderHead.idCard}
				</td>
				<td>
					${mhOrderHead.id}
				</td>
				<td>
					<fmt:formatDate value="${mhOrderHead.storageTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<shiro:hasPermission name="mhorderhead:mhOrderHead:view">
						<a href="#" onclick="openDialogView('查看订单表头', '${ctx}/mhorderhead/mhOrderHead/form?id=${mhOrderHead.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
					</shiro:hasPermission>
					<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:edit">--%>
    					<%--<a href="#" onclick="openDialog('修改订单表头', '${ctx}/mhorderhead/mhOrderHead/form?id=${mhOrderHead.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>--%>
    				<%--</shiro:hasPermission>--%>
    				<%--<shiro:hasPermission name="mhorderhead:mhOrderHead:del">--%>
						<%--<a href="${ctx}/mhorderhead/mhOrderHead/delete?id=${mhOrderHead.id}" onclick="return confirmx('确认要删除该订单表头吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>--%>
					<%--</shiro:hasPermission>--%>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>