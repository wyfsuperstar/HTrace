<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>清单表头管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#storageTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="mhManifestbillHead" action="${ctx}/mhmanifestbillhead/mhManifestbillHead/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">出区进口流水号：</label></td>
					<td class="width-35">
						<form:input path="ioSerialNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报号：</label></td>
					<td class="width-35">
						<form:input path="declNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报人名称：</label></td>
					<td class="width-35">
						<form:input path="declPerson" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报时间：</label></td>
					<td class="width-35">
						<form:input path="declDate" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报人联系方式：</label></td>
					<td class="width-35">
						<form:input path="declTel" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">汇总申报单号：</label></td>
					<td class="width-35">
						<form:input path="mainDeclNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">并单标志：</label></td>
					<td class="width-35">
						<form:input path="combineFlag" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">订单总数：</label></td>
					<td class="width-35">
						<form:input path="orderNum" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">出区申报单数量：</label></td>
					<td class="width-35">
						<form:input path="declCount" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电商订单数量 ：</label></td>
					<td class="width-35">
						<form:input path="orderCount" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">运单数量 ：</label></td>
					<td class="width-35">
						<form:input path="billCount" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">支付单数量：</label></td>
					<td class="width-35">
						<form:input path="paymentCount" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">运单号：</label></td>
					<td class="width-35">
						<form:input path="logisticsno" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">订单号：</label></td>
					<td class="width-35">
						<form:input path="orderNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">支付交易号：</label></td>
					<td class="width-35">
						<form:input path="paymentNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报类型代码：FI-境外进区、SI-出区进口：</label></td>
					<td class="width-35">
						<form:input path="declTypeCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报单位代码：</label></td>
					<td class="width-35">
						<form:input path="entCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电商企业代码：</label></td>
					<td class="width-35">
						<form:input path="cbecode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">物流仓储企业编号：</label></td>
					<td class="width-35">
						<form:input path="logisticsCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">施检机构代码：</label></td>
					<td class="width-35">
						<form:input path="checkOrgCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人证件类型编号(默认身份证)：</label></td>
					<td class="width-35">
						<form:input path="idType" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">总货值：</label></td>
					<td class="width-35">
						<form:input path="totalValues" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">集货/备货标识：</label></td>
					<td class="width-35">
						<form:input path="stockFlag" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人编号：</label></td>
					<td class="width-35">
						<form:input path="consigneeNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人：</label></td>
					<td class="width-35">
						<form:input path="consigneeCname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人英文名称：</label></td>
					<td class="width-35">
						<form:input path="consigneeEname" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">收货人电话：</label></td>
					<td class="width-35">
						<form:input path="consigneeTel" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">收货人地址：</label></td>
					<td class="width-35">
						<form:input path="consigneeAddress" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人编号：</label></td>
					<td class="width-35">
						<form:input path="consginorNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人：</label></td>
					<td class="width-35">
						<form:input path="consignorCname" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人英文名称：</label></td>
					<td class="width-35">
						<form:input path="consginorEname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人电话：</label></td>
					<td class="width-35">
						<form:input path="consignorTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">发货人地址：</label></td>
					<td class="width-35">
						<form:input path="consignorAddress" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">发货人证件号码：</label></td>
					<td class="width-35">
						<form:input path="idCard" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">备注：</label></td>
					<td class="width-35">
						<form:input path="remark" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">单据回执状态 状态代码:1 电子审单通过2 人工审单通过3 退单4 放行5 部分放行6 截留7 销毁8 退运9 电子审单未通过 ：</label></td>
					<td class="width-35">
						<form:input path="billsMesCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">检疫回执状态 状态代码，1待检疫 2合格 3不合格：</label></td>
					<td class="width-35">
						<form:input path="quarantineMesCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">查验回执状态 状态代码，0未查验 1待查验 2合格 3不合格：</label></td>
					<td class="width-35">
						<form:input path="proveMesCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">商品状态回执：</label></td>
					<td class="width-35">
						<form:input path="goodsStatus" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">推送状态（未推送：1  已推送：2 ）：</label></td>
					<td class="width-35">
						<form:input path="declarestatus" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">是否生成撤单申请  1 已生成：</label></td>
					<td class="width-35">
						<form:input path="flag" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">是否是单独报检    1为单独报检：</label></td>
					<td class="width-35">
						<form:input path="status" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">电商企业名称：</label></td>
					<td class="width-35">
						<form:input path="cbename" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报企业名称：</label></td>
					<td class="width-35">
						<form:input path="entName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">报文入库时间：</label></td>
					<td class="width-35">
						<input id="storageTime" name="storageTime" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${mhManifestbillHead.storageTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
					</td>
					<td class="width-15 active"><label class="pull-right">默认值为0，读取后改为1：</label></td>
					<td class="width-35">
						<form:input path="mark" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">原始流水号：</label></td>
					<td class="width-35">
						<form:input path="ioSerialNoBk" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">id：</label></td>
		   			<td class="width-35" ><form:input path="id" htmlEscape="false"    class="form-control "/></td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>