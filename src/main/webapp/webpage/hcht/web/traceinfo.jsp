<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp" %>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>天津跨境溯源</title>
    <%@ include file="/webpage/include/head.jsp" %>
    <link href="${ctxStatic}/web/style/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        function doSubmit() {
            document.getElementById("inputForm").submit();
        }
    </script>
</head>

<body>
<div class="wraper">
    <div class="logo-wrapper">
        <div class="logo"><img src="${ctxStatic}/web/images/logo.jpg"/></div>
    </div>
    <div class="inner-wrapper mt">
        <div class="main">
            <sys:message content="${message}" hideType="1"/>
            <form:form id="inputForm" modelAttribute="goodsOrder" action="web/feedback" target="_blank" method="post">
            <input id="idCard" name="idCard" type="hidden" value="${goodsOrder.idCard}"/>
            <input id="consigneeTel" name="consigneeTel" type="hidden" value="${goodsOrder.consigneeTel}"/>
            <input id="orderGoodId" name="orderGoodId" type="hidden" value="${goodsOrder.vhGoodsTrace.orderGoodId}"/>
            <div class="main-head">购买商品<span></span></div>
            <div class="news-item">
                <div class="info">
                <h2><a href="#">商品信息</a></h2>
                <div class="summary" style="height:30px; padding-left:100px;">
                    <c:if test="${goodsOrder.goodsName != null and goodsOrder.goodsName != ''}">
                        <div style="width:10%; float:left;"> 商品名称:</div>
                        <div style="width:30%; float:left;"> ${goodsOrder.goodsName} </div>
                    </c:if>
                    <div style="width:10%; float:left;"></div>
                    <div style="width:30%; float:left;"></div>
                </div>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.spec != null and goodsOrder.vhGoodsTrace.spec != ''}">
                            <div style="width:10%; float:left;"> 规格型号:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.spec}</div>
                        </c:if>
                    </div>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.brand != null and goodsOrder.vhGoodsTrace.brand != ''}">
                            <div style="width:10%; float:left;"> 品牌:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.brand}</div>
                        </c:if>

                    </div>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>
        <div class="news-item">
            <div class="info">
                <h2><a href="#">企业信息</a></h2>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <div style="width:10%; float:left;"> 企业名称:</div>
                        <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.cbeName}</div>
                    </div>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>

        <div class="news-item">
            <div class="info">
                <h2><a href="#">产地</a></h2>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.originCountry != null and goodsOrder.vhGoodsTrace.originCountry != ''}">
                            <div style="width:10%; float:left;"> 原产国:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.originCountry}</div>
                            <div style="width:10%; float:left;"></div>
                            <div style="width:30%; float:left;"></div>
                        </c:if>
                    </div>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>

                <c:if test="${!fn:contains(goodsOrder.vhGoodsTrace.despCountryName,'中国')}">
        <div class="news-item">
            <div class="info">
                <h2><a href="#">发货地</a></h2>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.despCountryName != null and goodsOrder.vhGoodsTrace.despCountryName != ''}">
                            <div style="width:10%; float:left;"> 发货国家:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.despCountryName}</div>
                            <div style="width:10%; float:left;"></div>
                            <div style="width:30%; float:left;"></div>
                        </c:if>
                    </div>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>
                </c:if>




        <div class="news-item">
            <div class="info">
                <h2><a href="#">跨境电商平台</a></h2>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.url != null and goodsOrder.vhGoodsTrace.url != ''}">
                            <div style="width:10%; float:left;"> 网址:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.url}</div>
                            <div style="width:10%; float:left;"></div>
                            <div style="width:30%; float:left;"></div>
                        </c:if>
                    </div>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.sitename != null and goodsOrder.vhGoodsTrace.sitename != ''}">
                            <div style="width:10%; float:left;"> 网站名:</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.sitename}</div>
                            <div style="width:10%; float:left;"></div>
                            <div style="width:30%; float:left;"></div>
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${goodsOrder.vhGoodsTrace != null}">
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <c:if test="${goodsOrder.vhGoodsTrace.importtypename != null and goodsOrder.vhGoodsTrace.importtypename != ''}">
                            <div style="width:10%; float:left;"> 运递进境方式 :</div>
                            <div style="width:30%; float:left;">${goodsOrder.vhGoodsTrace.importtypename}</div>
                        </c:if>
                    </div>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>

        <%--<div class="main-head about" style="text-align: center;cursor:pointer; height: 40px;"--%>
             <%--onclick="doSubmit()">对以上信息有疑问--%>
        <%--</div>--%>
        </form:form>
    </div>

    <div class="clear"></div>
</div>
<div class="footer mt">
    @2018<br/>
</div>
</div>
</body>
</html>
