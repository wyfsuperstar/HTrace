<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp" %>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>天津跨境溯源</title>
    <%@ include file="/webpage/include/head.jsp" %>
    <link href="${ctxStatic}/web/style/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div class="wraper">

    <div class="logo-wrapper">
        <div class="logo"><img src="${ctxStatic}/web/images/logo.jpg"/></div>
    </div>
    <form:form id="searchForm" modelAttribute="goodsOrder" action="/web/goodlist" method="post" class="form-inline">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
        <form:hidden path="idCard"/>
        <form:hidden path="consigneeTel"/>
    </form:form>

    <div class="inner-wrapper mt">

        <div class="main">
            <sys:message content="${message}"/>
            <div class="main-head">购买商品<span></span></div>
            <c:forEach items="${page.list}" var="goodsOrder">
                <div class="news-item">
                    <div class="info">
                        <h2><a href="traceinfo?id=${goodsOrder.vhGoodsTrace.orderGoodId}"
                               target="_blank">${goodsOrder.goodsName}</a></h2>
                        <div class="summary" style="height:50px; padding-left:100px;">
                            <div style="width:40%; float:left; height:40px;">
                                订单号:${goodsOrder.orderNo}
                            </div>
                            <div style="width:60%; float:left; height:40px;">
                                网站名:${goodsOrder.vhGoodsTrace.cbeName}
                            </div>
                            <%--<div style="width:20%; float:left;">--%>
                                <%--品牌:${goodsOrder.brand}--%>
                            <%--</div>--%>
                            <%--<div style="width:20%; float:left;">--%>
                                <%--成分:${goodsOrder.masterBase}--%>
                            <%--</div>--%>
                            <%--<div style="width:20%; float:left;">--%>
                                <%--原产国:${goodsOrder.originCountry}--%>
                            <%--</div>--%>


                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </c:forEach>


        </div>
        <div class="clear"></div>
        <!-- 分页代码 -->
        <table:page page="${page}"></table:page>
        <br/>
        <br/>
    </div>

    <div class="footer mt">
        @2018<br/>
    </div>
</div>
</body>
</html>
