<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp" %>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>天津跨境溯源</title>
    <%@ include file="/webpage/include/head.jsp"%>
    <link href="${ctxStatic}/web/style/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var validateForm;
        function doSubmit() {//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
            if (validateForm.form()) {
                $("#searchForm").submit();
            }

            return false;
        }
        $(document).ready(function () {
            validateForm = $("#searchForm").validate({
                submitHandler: function (form) {
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                messages: {
                    cbeCode: {required: "*"},
                    orderNo: {required: "*"},
                },
                errorContainer: "#messageBox",
                errorPlacement: function (error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>

<body>
<div class="wraper">

    <div class="logo-wrapper">
        <div class="logo"><img src="${ctxStatic}/web/images/logo.jpg"/></div>

    </div>


    <div class="inner-wrapper mt query">
        <div class="query-form">
            <sys:message content="${message}" hideType="1"/>
            <div class="pull-right form">
                <div class="tips">请选择企业和输入订单号</div>
                <form:form id="searchForm" modelAttribute="goodsOrder" action="web/goodlist" method="post">
                    <input type="hidden" name="fromPage" value="orders"/>
                    <form:select path="cbeCode" class="input required">
                        <form:options items="${cbes}" itemLabel="cbeName" itemValue="cbeCode" htmlEscape="false"/>
                    </form:select>
                    <form:input path="orderNo" htmlEscape="false" maxlength="50" class="input required" placeholder="请输入订单号"/>
                    <input class="submit" type="button" name="" value="" onclick="doSubmit()"/>
                </form:form>
            </div>
        </div>
        <!---img src="images/query-bg.jpg" height="767" width="1024" /--->
        <div class="clear"></div>
    </div>

    <div class="footer mt">
        @2018<br/>
    </div>
</div>
</body>
</html>
