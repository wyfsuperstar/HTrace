<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp" %>
<!doctype html>
<html>
<head>
    <%@ include file="/webpage/include/head.jsp" %>
    <meta charset="UTF-8">
    <title>天津跨境溯源</title>
    <link href="${ctxStatic}/web/style/style.css" rel="stylesheet" type="text/css"/>

    <link href="${ctxStatic}/jqUpload/css/upload.css" rel="stylesheet" type="text/css"/>
    <script src="${ctxStatic}/jqUpload/upload.js" type="text/javascript"></script>

    <link href="${ctxStatic}/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="${ctxStatic}/sweetalert/sweetalert.min.js"></script>

    <style>
        .error{
            color:red;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    // form.submit();

                    var imgFiles = "";
                    //遍历image-box元素下的全部子集
                    $(".image-box  img").each(function () {
                        // alert($(this).attr("src"));
                        imgFiles += $(this).attr("src") + "|";
                    });
                    $("#files").val(imgFiles);

                    $.ajax({
                        url : "/web/feedbackSave",
                        type : 'POST',
                        data : {
                            "title": $("#title").val(),
                            "text": $("#text").val(),
                            "idCard": $("#idCard").val(),
                            "consigneeTel": $("#consigneeTel").val(),
                            "orderGoodId": $("#orderGoodId").val(),
                            "files": $("#files").val()
                        },
                        success : function(responseStr) {
                            closeTip();
                            swal({
                                title: "",
                                text: "保存成功",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                window.close();
                            });
                        },
                        error : function(responseStr) {
                            alert("error");
                        }
                    });
                },
                rules: {
                    title: {
                        required: true
                    },
                    text: "required"
                },
                messages: {
                    title: {
                        required: '请输入反馈标题'
                    },
                    text: '请输入反馈内容'
                },
                errorElement: "span",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                },
                //用来创建错误提示信息标签
                success: function (label) {

                }
            });

            $("#upload-input").ajaxImageUpload({
                url: '/web/feedback/upload', //上传的服务器地址
                data: {
                    "idCard": "${page.list[0].idCard}",
                    "consigneeTel": "${page.list[0].consigneeTel}",
                    "orderGoodId": "${page.list[0].orderGoodId}"
                },
                maxNum: 3, //允许上传图片数量
                zoom: true, //允许上传图片点击放大
                allowType: ["gif", "jpeg", "jpg", "bmp", 'png'], //允许上传图片的类型
                maxSize: 5, //允许上传图片的最大尺寸，单位M
                success: function (data) {
                    //data.body.imgUrl
                },
                error: function (e) {
                    top.layer.alert("上传失败，请稍后重试。");
                }
            });
        });

    </script>
</head>

<body>
<div class="wraper">
    <div class="logo-wrapper">
        <div class="logo"><img src="${ctxStatic}/web/images/logo.jpg"/></div>
    </div>

    <div class="inner-wrapper mt">
        <div class="main">
            <div class="main-head">信息反馈<span></span></div>
            <div class="news-item">
                <div class="info">
                    <h2><a href="#">商品信息</a></h2>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <div style="width:10%; float:left;"> 商品名称:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].goodsName} </div>
                        <div style="width:10%; float:left;"></div>
                        <div style="width:30%; float:left;"></div>
                    </div>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <div style="width:10%; float:left;"> 主要成分:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].masterBase} </div>
                        <div style="width:10%; float:left;"> 规格型号:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].spec} </div>
                    </div>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <div style="width:10%; float:left;"> 原产国:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].originCountry} </div>
                        <div style="width:10%; float:left;"> 生产企业:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].proEnt} </div>
                    </div>
                    <div class="summary" style="height:30px; padding-left:100px;">
                        <div style="width:10%; float:left;"> 品牌:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].brand} </div>
                        <div style="width:10%; float:left;"> 用途:</div>
                        <div style="width:30%; float:left;"> ${page.list[0].useWay} </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <form id="inputForm" name="inputForm" action="web/feedbackSave" method="post">
                <input type="hidden" id="idCard" name="idCard" value="${page.list[0].idCard}">
                <input type="hidden" id="consigneeTel" name="consigneeTel" value="${page.list[0].consigneeTel}">
                <input type="hidden" id="orderGoodId" name="orderGoodId" value="${page.list[0].orderGoodId}">
                <input type="hidden" id="files" name="files" value="">
                <div class="news-item">
                    <div class="info">
                        <h2><a href="#">信息反馈</a></h2>
                        <div class="summary" style="padding-left:100px;">
                            <div class="post">
                                <table width="100%">
                                    <tr>
                                        <td width="10%" align="right">反馈标题：</td>
                                        <td width="90%">
                                            <%--<input type="text" id="title" name="title" class="input" style="width: 250px;">--%>
                                            <table>
                                                <tr>
                                                    <td><input type="text" id="title" name="title" class="input" style="width: 250px;"></td>
                                                    <td name="tip"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" align="right">图片上传：</td>
                                        <td width="90%">

                                            <div class="upload-box">
                                                <p class="upload-tip">反馈图片：最多可以上传3张图片</p>
                                                <div class="image-box up-clear">
                                                    <section class="upload-section">
                                                        <div class="upload-btn"></div>
                                                        <input type="file" name="file" id="upload-input" value=""
                                                               accept="image/jpg,image/jpeg,image/png,image/bmp"
                                                               multiple="multiple"/>
                                                    </section>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" align="right">反馈内容：</td>
                                        <td width="90%">
                                            <%--<textarea id="text" name="text" class="textarea" style="width: 250px;"></textarea>--%>
                                            <table>
                                                <tr>
                                                    <td><textarea id="text" name="text" class="textarea"  style="width: 250px;"></textarea></td>
                                                    <td name="tip"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" align="right"></td>
                                        <td width="90%">
                                            <input type="submit" value="提交" class="main-head about"  style="width:130px;text-align: center;cursor:pointer; height: 40px;">
                                            <%--<div class="main-head about" style="width:100px;text-align: center;cursor:pointer; height: 40px;">提交</div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </form>

        </div>
        <div class="clear"></div>
    </div>

    <div class="footer mt">
        @2018<br/>
    </div>
</div>
</body>
</html>
