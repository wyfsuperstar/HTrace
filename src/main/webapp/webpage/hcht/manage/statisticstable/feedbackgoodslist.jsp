<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>反馈商品统计</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			document.getElementById("date").onclick = function (event) {
				WdatePicker({
					dateFmt: 'yyyy-MM',
					skin: 'default'
				})
			};

		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>反馈商品统计 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="statisticsTable" action="${ctx}/manage/statisticstable/feedbackgoodslist" method="post" class="form-inline">
		<div class="form-group">
			<span>溯源日期：</span>
			<input id="date" name="date" type="text" maxlength="20"
				   class="laydate-icon form-control layer-date input-sm" readonly="readonly"
				   value="<fmt:formatDate value="${statisticsTable.date}" pattern="yyyy-MM"/>"/>

		</div>
		<div class="form-group">
			<span>统计类型：</span>
			<form:select path="statisticsType" class="form-control " style="width:200px;">
				<form:options items="${fns:getDictList('statisticsType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
		</div>
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:exportExcel url="${ctx}/manage/statisticstable/exportfeedbackgoodslist"></table:exportExcel><!-- 导出按钮 -->
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th>项目</th>
				<th>本期值</th>
				<th>上期值</th>
				<th>环比%</th>
				<th>同期值</th>
				<th>同比%</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${list}" var="statisticsTable">
			<tr>
				<td>
					${statisticsTable.name}
				</td>
				<td>
					${statisticsTable.thisvalue}
				</td>
				<td>
					${statisticsTable.lastvalue}
				</td>
				<td>
					${statisticsTable.lastpre}
				</td>
				<td>
					${statisticsTable.lastyearvalue}
				</td>
				<td>
					${statisticsTable.lastyearpre}
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>