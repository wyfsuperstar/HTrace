<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>反馈信息处理意见</title>
	<meta name="decorator" content="default"/>
	<style>
		.yuanBtn
		{
			border-radius: 50%;
			width: 10px;
			height: 15px;
			background: #888888;
		}

		.yuanBtnSelect
		{
			border-radius: 50%;
			width: 10px;
			height: 15px;
			background: #ff0000;
		}
	</style>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});

        function btnClick(pic, btn){
            for(var i=0; i<inputForm.buttons.length; i++){
                inputForm.buttons[i].className = "yuanBtn";
                if(inputForm.buttons[i] == btn) {
                    inputForm.buttons[i].className = "yuanBtnSelect";
                }
            }

            $("#picImg").attr("src", pic);
            $("#picHref").attr("href", pic);
        }
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" name="inputForm" modelAttribute="feedback" action="${ctx}/feedback/feedback/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">处理方式：</label></td>
					<td class="width-35">
						<form:input path="idCard" htmlEscape="false"    class="form-control "/>
					</td>

				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">处理描述：</label></td>
					<td class="width-35">
						<form:input path="gpsx" htmlEscape="false"    class="form-control "/>
					</td>

				</tr>

		 	</tbody>
		</table>
	</form:form>
</body>
</html>