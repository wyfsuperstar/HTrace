<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>反馈信息统计</title>
	<meta name="decorator" content="default"/>
	<%@ include file="/webpage/include/echarts.jsp"%>
	<script type="text/javascript">
		$(document).ready(function () {
			laydate({
				elem: '#beginCreateDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
				event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			});
			laydate({
				elem: '#endCreateDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
				event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			});


		});
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>反馈信息统计 </h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-wrench"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#">选项1</a>
					</li>
					<li><a href="#">选项2</a>
					</li>
				</ul>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>

		<div class="ibox-content">
			<sys:message content="${message}"/>

			<!--查询条件-->
			<div class="row">
				<div class="col-sm-12">
					<form:form id="searchForm" modelAttribute="feedback" action="${ctx}/manage/feedback/statistics1" method="post" class="form-inline">
						<div class="form-group">
							<span>反馈日期：</span>
							<input id="beginCreateDate" name="beginCreateDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="<fmt:formatDate value="${feedback.beginCreateDate}" pattern="yyyy-MM-dd"/>"/> -
							<input id="endCreateDate" name="endCreateDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="<fmt:formatDate value="${feedback.endCreateDate}" pattern="yyyy-MM-dd"/>"/>
						</div>

					</form:form>
				</div>
			</div>

			<!-- 工具栏 -->
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
					</div>
				</div>
			</div>
			<br/>

			<div id="line_normal"  class="main000"></div>
			<echarts:bar
				id="line_normal"
				title="反馈信息统计"
				xAxisData="${xAxisData}"
				yAxisData="${yAxisData}"
				xAxisName="企业名称"
				yAxisName="反馈次数"
				changeXy="true"
				/>

		</div>
	</div>
</div>
</body>
</html>