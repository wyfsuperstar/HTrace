<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>溯源商品统计</title>
	<meta name="decorator" content="default"/>
	<%--<%@ include file="/webpage/include/echarts.jsp"%>--%>
	<style type="text/css">
		.main000 {
			height: 360px;
			/*width: 778px !important;*/
			overflow: hidden;
			padding : 10px;
			margin-bottom: 10px;
			border: 1px solid #e3e3e3;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
			-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
		}
	</style>
	<script src="${ctxStatic}/echarts-4.1.0/echarts.min.js"></script>
	<script src="${ctxStatic}/echarts-4.1.0/extension/bmap.min.js"></script>
	<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=4E121DD04dc04ddf4092d0cbbc8c54b2"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			laydate({
				elem: '#beginDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
				event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			});
			laydate({
				elem: '#endDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
				event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			});

			let datas = ${dataList};
			let dataArr = [];
            let centerPoint = [117.1591, 39.1102];
			if(datas !== null){
			    for(i in datas){
			        if(i === 0){
                        centerPoint[0] = datas[i].gpsx;
                        centerPoint[1] = datas[i].gpsy;
                    }
                    dataArr.push({"name":datas[i].itemName,"value":[datas[i].gpsx,datas[i].gpsy,datas[i].itemNum]});
                }
            }

            let myChart = echarts.init(document.getElementById('div_statistic'));

            // 指定图表的配置项和数据
            let option = {
                title : {
                    text: '反馈地点',
                    left: 'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: function(a){
                        return a.seriesName + " <br/>" + a.name + " : " + a.value[2];

                    }
                },
                bmap: {
                    center: centerPoint,
                    zoom: 7,
                    roam: true
                },
                series : [
                    {
                        name: '反馈数量',
                        type: 'scatter',
                        coordinateSystem: 'bmap',
                        data: dataArr,
                        symbolSize: function (val) {
                            return val[2] < 10 ? 10 : val[2] > 30 ? 30 : val[2];
                        },
                        label: {
                            normal: {
                                formatter: '{b}',
                                position: 'right',
                                show: true
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: '#CC6600'
                            }
                        }
                    }
                ]
            };


            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);

		});
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>反馈地点统计 </h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-wrench"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#">选项1</a>
					</li>
					<li><a href="#">选项2</a>
					</li>
				</ul>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>

		<div class="ibox-content">
			<sys:message content="${message}"/>

			<!--查询条件-->
			<div class="row">
				<div class="col-sm-12">
					<form:form id="searchForm" modelAttribute="feedbackStatistic" action="${ctx}/manage/feedback/locationStatistic" method="post" class="form-inline">
						<div class="form-group">
							<span>反馈日期：</span>
							<input id="beginDate" name="beginDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="<fmt:formatDate value="${feedbackStatistic.beginDate}" pattern="yyyy-MM-dd"/>"/> -
							<input id="endDate" name="endDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="<fmt:formatDate value="${feedbackStatistic.endDate}" pattern="yyyy-MM-dd"/>"/>
						</div>

					</form:form>
				</div>
			</div>

			<!-- 工具栏 -->
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
					</div>
				</div>
			</div>
			<br/>

			<div id="div_statistic"  class="main000"></div>

			<br/>

		</div>
	</div>
</div>
</body>
</html>