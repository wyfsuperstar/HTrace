<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>反馈信息详情查询</title>
	<meta name="decorator" content="default"/>
	<style>
		.yuanBtn
		{
			border-radius: 50%;
			width: 10px;
			height: 15px;
			background: #888888;
		}

		.yuanBtnSelect
		{
			border-radius: 50%;
			width: 10px;
			height: 15px;
			background: #ff0000;
		}
	</style>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});

        function btnClick(pic, btn){
            for(var i=0; i<inputForm.buttons.length; i++){
                inputForm.buttons[i].className = "yuanBtn";
                if(inputForm.buttons[i] == btn) {
                    inputForm.buttons[i].className = "yuanBtnSelect";
                }
            }

            $("#picImg").attr("src", pic);
            $("#picHref").attr("href", pic);
        }
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" name="inputForm" modelAttribute="feedback" action="${ctx}/manage/feedback/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">证件号：</label></td>
					<td class="width-35">
						<form:input path="idCard" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">电话号码：</label></td>
					<td class="width-35">
						<form:input path="consigneeTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">GPS-X信息：</label></td>
					<td class="width-35">
						<form:input path="gpsx" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">GPS-Y信息：</label></td>
					<td class="width-35">
						<form:input path="gpsy" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<%--<td class="width-15 active"><label class="pull-right">订单商品ID：</label></td>--%>
					<%--<td class="width-35">--%>
						<%--<form:input path="orderGoodId" htmlEscape="false"    class="form-control "/>--%>
					<%--</td>--%>
					<td class="width-15 active"><label class="pull-right">商品名称：</label></td>
					<td class="width-35">
						<form:input path="vhGoodDetails.goodsName" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业名称：</label></td>
					<td class="width-35">
						<form:input path="vhGoodDetails.cbeName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">反馈标题：</label></td>
					<td class="width-35">
						<form:input path="title" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">反馈描述：</label></td>
					<td class="width-35">
						<form:textarea path="desc" htmlEscape="false" rows="4"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">反馈时间：</label></td>
					<td class="width-35">
						<input value="<fmt:formatDate value="${feedback.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>" pattern="yyyy-MM-dd HH:mm:ss" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">来源类型：</label></td>
					<td class="width-35">
						<form:select path="sourceType" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('source_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">处置措施：</label></td>
					<td class="width-35">
						<form:select path="czcs" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('handle_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					</td>
					<td class="width-15 active"><label class="pull-right">处置意见：</label></td>
					<td class="width-35">
						<form:textarea path="czyj" htmlEscape="false" rows="4" maxlength="500"  class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">反馈图片：</label></td>
					<td class="width-35" align="center">
						<c:choose>
							<c:when test="${feedback.feedbackpicList.size() > 1}">
								<a id="picHref" href="${feedback.feedbackpicList[0].picUrl}" target="_blank"><img id="picImg" src="${feedback.feedbackpicList[0].picUrl}" width="100px" height="100px" alt=""/></a>
								<br>
								<div align="center">
									<c:forEach items="${feedback.feedbackpicList}" var="feedbackPic">
										<c:choose>
											<c:when test="${feedbackPic.picUrl == feedback.feedbackpicList[0].picUrl}">
												<button type="button" name="buttons" class="yuanBtnSelect" onclick='btnClick("${feedbackPic.picUrl}", this)' />
											</c:when>
											<c:otherwise>
												<button type="button" name="buttons" class="yuanBtn" onclick='btnClick("${feedbackPic.picUrl}", this)' />
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</div>
							</c:when>
							<c:when test="${feedback.feedbackpicList.size() == 1}">
								<a href="${feedback.feedbackpicList[0].picUrl}" target="_blank"><img src="${feedback.feedbackpicList[0].picUrl}" width="100px" height="100px" alt=""/></a>
							</c:when>
							<c:otherwise>
								<img src="/static/images/webuploader.png" width="100px" height="100px" alt=""/>
								<br>
								未上传图片
								<%--<img src="" width="100px" height="100px" alt="未上传图片"/>--%>
							</c:otherwise>
						</c:choose>
					</td>
					<td class="width-15 active"></td>
					<td class="width-35" ></td>
				</tr>

		   </tbody>
		</table>
	</form:form>
</body>
</html>