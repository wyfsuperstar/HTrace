<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>溯源商品统计</title>
	<meta name="decorator" content="default"/>
	<%@ include file="/webpage/include/echarts.jsp"%>
	<script type="text/javascript">
        $(document).ready(function () {
            laydate({
                elem: '#beginDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });
            laydate({
                elem: '#endDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });

            let datas = ${dataList};
            let nameArr = [];
            let dataArr = [];
            if(datas !== null){
                for(i in datas){
                    nameArr.push(datas[i].itemName);
                    dataArr.push(datas[i].itemNum);
                }
            }

            require([ 'echarts', 'echarts/chart/bar'], function(ec) {
                let myChart = ec.init(document.getElementById('div_statistic'));

                // 指定图表的配置项和数据
                let option = {
                    xAxis: {
                        type: 'category',
                        data: nameArr
                    },
                    yAxis: {
                        type: 'value'
                    },
                    title : {
                        text: '溯源商品类别',
                        x:'center'
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {
                                show: true,
                                type: "png",
                            },
                            dataZoom: {show: true},
                            restore: {show: true}
                        },
						show: true
                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c}"
                    },
                    series : [
                        {
                            name: '商品类别',
                            type: 'bar',
                            data: dataArr,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                                ,normal: {label : {show: true}}
                            }
                        }
                    ]
                };


                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
            });

        });
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>溯源商品类别统计 </h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-wrench"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#">选项1</a>
					</li>
					<li><a href="#">选项2</a>
					</li>
				</ul>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>

		<div class="ibox-content">
			<sys:message content="${message}"/>

			<!--查询条件-->
			<div class="row">
				<div class="col-sm-12">
					<form:form id="searchForm" modelAttribute="orderStatistic" action="${ctx}/manage/goodsOrder/goodsCategoryStatistic" method="post" class="form-inline">
						<div class="form-group">
							<span>订单日期：</span>
							<input id="beginDate" name="beginDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="${orderStatistic.beginDate}"/> -
							<input id="endDate" name="endDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
								   value="${orderStatistic.endDate}"/>
						</div>

					</form:form>
				</div>
			</div>

			<!-- 工具栏 -->
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
						<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
					</div>
				</div>
			</div>
			<br/>

			<div id="div_statistic"  class="main000"></div>

			<br/>

		</div>
	</div>
</div>
</body>
</html>