<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>修改企业备案信息成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="mhTbCompanyInspe" action="${ctx}/manage/companyinspe/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">注册地址：</label></td>
					<td class="width-35">
						<form:input path="registerAddress" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">单位联系人联系传真：</label></td>
					<td class="width-35">
						<form:input path="fax" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报日期：</label></td>
					<td class="width-35">
						<form:input path="applyTime" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">营业执照号：</label></td>
					<td class="width-35">
						<form:input path="busLcNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">营业执照签发单位：</label></td>
					<td class="width-35">
						<form:input path="busLcSignUnit" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">法定代表人：</label></td>
					<td class="width-35">
						<form:input path="lawMan" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">单位联系人：</label></td>
					<td class="width-35">
						<form:input path="contactMan" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">单位联系人手机：</label></td>
					<td class="width-35">
						<form:input path="contactManMobile" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">单位联系人邮箱：</label></td>
					<td class="width-35">
						<form:input path="contactManEmail" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">申报人：</label></td>
					<td class="width-35">
						<form:input path="declPerson" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业备案网址信息：</label></td>
					<td class="width-35">
						<form:input path="url" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">网站名：</label></td>
					<td class="width-35">
						<form:input path="siteName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">经营商品：</label></td>
					<td class="width-35">
						<form:input path="busScope" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业备案流水号：</label></td>
					<td class="width-35">
						<form:input path="entSerialNo" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业中文名：</label></td>
					<td class="width-35">
						<form:input path="entCname" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业英文名：</label></td>
					<td class="width-35">
						<form:input path="entEname" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业类型：平台（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypePl" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业类型：电商（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeCbe" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业类型：物流（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeLst" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业类型：代理公司（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeAge" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业类型：仓储（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeSto" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业类型：支付企业（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypePay" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业类型：监管场所（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeRgl" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业类型：其他（Y是/N否）：</label></td>
					<td class="width-35">
						<form:input path="entTypeOt" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">代理企业编号：</label></td>
					<td class="width-35">
						<form:input path="agentEntNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理企业名称：</label></td>
					<td class="width-35">
						<form:input path="agentEntName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">代理企业联系人：</label></td>
					<td class="width-35">
						<form:input path="agentEntMan" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理企业联系电话：</label></td>
					<td class="width-35">
						<form:input path="agentEntTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">代理企业法人代表：</label></td>
					<td class="width-35">
						<form:input path="agentEntLawMan" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理企业法人电话：</label></td>
					<td class="width-35">
						<form:input path="agentEntLawManTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">代理企业组织机构代码：</label></td>
					<td class="width-35">
						<form:input path="agentEntOrgNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理企业邮箱：</label></td>
					<td class="width-35">
						<form:input path="agentEntEmail" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">代理企业国别：</label></td>
					<td class="width-35">
						<form:input path="agentEntCountry" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理企业地址：</label></td>
					<td class="width-35">
						<form:input path="agentEntAddress" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">申报状态   1一级审核未通过  2二级审核通过  3已提交 03待审核  4二级审核未通过  5一级审核通过  6暂存    9电子审单未通过 ：</label></td>
					<td class="width-35">
						<form:input path="status" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">锁定状态    N未锁定 Y已锁定：</label></td>
					<td class="width-35">
						<form:input path="lockflag" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">企业ID：</label></td>
					<td class="width-35">
						<form:input path="refCompanyId" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">企业备案号：</label></td>
					<td class="width-35">
						<form:input path="attribute" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">统一社会信用代码/组织机构代码：</label></td>
					<td class="width-35">
						<form:input path="orgNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">单位办公地址：</label></td>
					<td class="width-35">
						<form:input path="officeAddress" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">员工数：</label></td>
					<td class="width-35">
						<form:input path="empCount" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">售后服务人数：</label></td>
					<td class="width-35">
						<form:input path="srvEmployeeCount" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">投资来源（国别/地区）：</label></td>
					<td class="width-35">
						<form:input path="investSource" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">法人代表联系电话：</label></td>
					<td class="width-35">
						<form:input path="lawManTel" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">应急联络负责人：</label></td>
					<td class="width-35">
						<form:input path="emergencyMan" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">应急联络负责人手机：</label></td>
					<td class="width-35">
						<form:input path="emergencyManMobile" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">应急联络负责人电话：</label></td>
					<td class="width-35">
						<form:input path="emergencyManTel" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">质量安全管理员：</label></td>
					<td class="width-35">
						<form:input path="qaSafeMan" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">质量安全管理员手机：</label></td>
					<td class="width-35">
						<form:input path="qaSafeManMobile" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">质量安全管理员邮箱：</label></td>
					<td class="width-35">
						<form:input path="qaSafeManEmail" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">海关企业备案号：</label></td>
					<td class="width-35">
						<form:input path="hgAttribute" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>