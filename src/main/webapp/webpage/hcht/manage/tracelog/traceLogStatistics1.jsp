<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp" %>
<html>
<head>
    <title>溯源商品统计</title>
    <meta name="decorator" content="default"/>
    <%@ include file="/webpage/include/echarts.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById("beginCreateDate").onclick = function (event) {
                var endMonth = $dp.$('endCreateDate');
                WdatePicker({
                    onpicked: function () {
                        endMonth.click();
                    },
                    maxDate: '#F{$dp.$D(\'endCreateDate\')}',
                    dateFmt: 'yyyy-MM',
                    skin: 'default'
                });
            };

            document.getElementById("endCreateDate").onclick = function (event) {
                WdatePicker({
                    minDate: '#F{$dp.$D(\'beginCreateDate\')}',
                    dateFmt: 'yyyy-MM',
                    skin: 'default'
                })
            };


        });
    </script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>溯源商品统计 </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">选项1</a>
                    </li>
                    <li><a href="#">选项2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
            <sys:message content="${message}"/>

            <!--查询条件-->
            <div class="row">
                <div class="col-sm-12">
                    <form:form id="searchForm" modelAttribute="traceLog" action="${ctx}/manage/tracelog/statistics1" method="post" class="form-inline">
                        <div class="form-group">
                            <div class="form-group">
                                <span>溯源日期：</span>
                                <input id="beginCreateDate" name="beginCreateDate" type="text" maxlength="20"
                                       class="laydate-icon form-control layer-date input-sm" readonly="readonly"
                                       value="<fmt:formatDate value="${traceLog.beginCreateDate}" pattern="yyyy-MM"/>"/> -
                                <input id="endCreateDate" name="endCreateDate" type="text" maxlength="20"
                                       class="laydate-icon form-control layer-date input-sm" readonly="readonly"
                                       value="<fmt:formatDate value="${traceLog.endCreateDate}" pattern="yyyy-MM"/>"/>
                            </div>
                            <%--<div class="form-group">--%>
                                <%--<span>商品用途：</span>--%>
                                <%--<form:select path="useWay" class="form-control " style="width:200px;">--%>
                                    <%--<form:option value="" label=""/>--%>
                                    <%--<form:options items="${fns:getDictList('use_way')}" itemLabel="label" itemValue="value" htmlEscape="false"/>--%>
                                <%--</form:select>--%>

                            <%--</div>--%>
                        </div>
                    </form:form>
                    <br />
                </div>
            </div>

            <!-- 工具栏 -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
                    </div>
                </div>
            </div>
            <br/>

            <!-- 图表 -->
            <div id="line_normal" class="main000"></div>
            <echarts:line
                    id="line_normal"
                    title="溯源商品统计"
                    xAxisData="${xAxisData}"
                    yAxisData="${yAxisData}"
                    xAxisName="年月"
                    yAxisName="商品次"/>

        </div>
    </div>
</div>
</body>
</html>

